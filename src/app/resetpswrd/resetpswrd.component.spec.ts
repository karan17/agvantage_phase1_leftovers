import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResetpswrdComponent } from './resetpswrd.component';

describe('ResetpswrdComponent', () => {
  let component: ResetpswrdComponent;
  let fixture: ComponentFixture<ResetpswrdComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResetpswrdComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResetpswrdComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
