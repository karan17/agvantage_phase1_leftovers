import {Component, OnInit, ViewChild} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import {Router} from '@angular/router';
import {serviceUrl} from "../app.module";

declare var $: any;

@Component({
	selector: 'app-resetpswrd',
	templateUrl: './resetpswrd.component.html',
	styleUrls: ['./resetpswrd.component.scss']
})
export class ResetpswrdComponent implements OnInit {
	reasons: any;
	cnfrmpswrd: any;
	newpswrd: any;
	url: string;
	headers: Headers;
	options: RequestOptions;
	USER: any;
	showOutcome: string;
	outcomeMsg: boolean = false;


	constructor(private http: Http, public spinnerService: Ng4LoadingSpinnerService, private router: Router) {
		this.USER = {};
		this.reasons = [
			{img: "fa fa-briefcase", why: "Comprehensive Farm Marketing Plans"},
			{img: "fa fa-support", why: "Marketing Support"},
			{img: "fa fa-bell", why: "Automation, Price Alerts & Reminders"},
			{img: "fa fa-line-chart", why: "Always know their position"},
			{img: "fa fa-bar-chart", why: "Records, History, & Reports - in one place "},
			{img: "fa fa-lock", why: "Highly Secure "}];
	}

	@ViewChild('resetform') public resetform;

	showMsg() {
		this.headers = new Headers({
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
			'Accept': 'application/json'
		});
		this.options = new RequestOptions({headers: this.headers});
		this.url = serviceUrl + "AccountGatewayResetPassword/AccountGatewayPasswordResetPS/api/v1/resetPassword";
		this.spinnerService.show();
		this.http.post(this.url, $.param(this.USER), this.options).subscribe((data) => {
			console.log(data.json());
			this.spinnerService.hide();
			this.showOutcome = data.json().outcome;
			this.outcomeMsg = true;
			this.USER = {UserName: "", OldPassword: "", NewPassword: ""};
			console.log(this.resetform);
			this.resetform.reset();

		})

	}

	gotoLogin() {
		this.router.navigate(['login']);
	}

	ngOnInit() {
	}

}
