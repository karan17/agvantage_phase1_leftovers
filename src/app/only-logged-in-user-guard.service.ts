import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { UserRoleService } from './user-role.service';

@Injectable()
export class OnlyLoggedInUserGuardService implements CanActivate {

  
  constructor(public auth: UserRoleService, public router: Router) {}

  canActivate(): boolean {
    if (!this.auth.isLoggedIn()) {
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
