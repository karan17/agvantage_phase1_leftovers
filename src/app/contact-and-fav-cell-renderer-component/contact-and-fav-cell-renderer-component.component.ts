import { Component, OnInit } from '@angular/core';
import {ICellRendererAngularComp} from "ag-grid-angular";

@Component({
  selector: 'app-contact-and-fav-cell-renderer-component',
  templateUrl: './contact-and-fav-cell-renderer-component.component.html',
  styleUrls: ['./contact-and-fav-cell-renderer-component.component.scss']
})
export class ContactAndFavCellRendererComponentComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public params: any;

    agInit(params: any): void {
        this.params = params;
    }

    public invokeParentMethod() {
        console.log(this.params.node);
        this.params.context.componentParent.methodFromParent(this.params.node);
    }

    refresh(): boolean {
        return false;
    }

}
