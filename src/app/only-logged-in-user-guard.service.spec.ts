import { TestBed, inject } from '@angular/core/testing';

import { OnlyLoggedInUserGuardService } from './only-logged-in-user-guard.service';

describe('OnlyLoggedInUserGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [OnlyLoggedInUserGuardService]
    });
  });

  it('should be created', inject([OnlyLoggedInUserGuardService], (service: OnlyLoggedInUserGuardService) => {
    expect(service).toBeTruthy();
  }));
});
