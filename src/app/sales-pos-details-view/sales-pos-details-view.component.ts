import { Component, OnInit } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import {serviceUrl} from "../app.module";
import { Http, Headers, RequestOptions } from '@angular/http';
import { UserRoleService } from '../user-role.service';

@Component({
  selector: 'app-sales-pos-details-view',
  templateUrl: './sales-pos-details-view.component.html',
  styleUrls: ['./sales-pos-details-view.component.scss']
})
export class SalesPosDetailsViewComponent implements OnInit {

  gridOptions: GridOptions;
  gridApi;
  gridColumnApi;
  getRowNodeId;
  defaultColDef;
  columnDefs;
  rowData;
  currentUser;

  constructor(private http: Http, private role: UserRoleService) {
    this.currentUser = this.role.getUser();
    this.gridOptions = <GridOptions>{
      onGridReady: () => {
        // this.gridOptions.api.sizeColumnsToFit();

      }
    };
    this.defaultColDef = { editable: false };
    this.getRowNodeId = function (data) {
      return data.id;
    };
    this.columnDefs = [

      { headerName: "StorageName", field: "StorageName", pinned: 'left' },
      { headerName: "Commodity", field: "Commodity", pinned: 'left' },
      { headerName: "CropYear", field: "CropYear", pinned: 'left' },
      { headerName: "StartingQuantity", field: "StartingQuantity", pinned: 'left' },
      { headerName: "SoldQuantity", field: "SoldQuantity", pinned: 'left' },
      { headerName: "SaleValuePerUnit", field: "SaleValuePerUnit", pinned: 'left' },
      { headerName: "BreakevenPerUnit", field: "BreakevenPerUnit", pinned: 'left' },
      { headerName: "FreightFarmToInventoryLocation", field: "FreightFarmToInventoryLocation", pinned: 'left' },
      { headerName: "FreightInventoryToSaleLocation", field: "FreightInventoryToSaleLocation", pinned: 'left' },
      { headerName: "CarryFumigationCost", field: "CarryFumigationCost", pinned: 'left' ,hide:true },
      { headerName: "ExFarmPrice", field: "ExFarmPrice", pinned: 'left',hide:true },
      { headerName: "M2MRevenue", field: "M2MRevenue", pinned: 'left',hide:true },
      { headerName: "M2MProfitLoss", field: "M2MProfitLoss", pinned: 'left',hide:true},
      { headerName: "M2MProfit", field: "M2MProfit", pinned: 'left',hide:true },
      { headerName: "M2MROI", field: "M2MROI", pinned: 'left' ,hide:true },
      { headerName: "TargetROI", field: "TargetROI", pinned: 'left',hide:true },
      { headerName: "ROI", field: "ROI", pinned: 'left',hide:true},
      { headerName: "Contract No", field: "ContractNo", pinned: 'left',hide:true}
    ];
    this.rowData = [];
  }

  ngOnInit() {
    this.getSalesPosDetails();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  selectAllRows() {
    this.gridOptions.api.selectAll();
  }


  getSalesPosDetails() {
    // https://129.154.71.56:9074/SalesAndM2MPosition/SalesPositionPS/api/v1/GetSalesPositionDetailView?U_ID=279
    let url = serviceUrl+'SalesAndM2MPosition/SalesPositionPS/api/v1/GetSalesPositionDetailView?U_ID=' + this.currentUser.USER.U_ID;
    this.http.get(url).subscribe((data) => {
      if (data.json() != null) {
        this.rowData = data.json()?data.json().SalesPosition:[];
      } else {
        this.rowData = [];
      }
    })
  }

}
