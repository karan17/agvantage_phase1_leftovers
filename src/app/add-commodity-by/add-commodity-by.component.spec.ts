import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCommodityByComponent } from './add-commodity-by.component';

describe('AddCommodityByComponent', () => {
  let component: AddCommodityByComponent;
  let fixture: ComponentFixture<AddCommodityByComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCommodityByComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCommodityByComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
