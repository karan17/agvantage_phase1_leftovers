import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfitabilityMatrixGridComponent } from './profitability-matrix-grid.component';

describe('ProfitabilityMatrixGridComponent', () => {
  let component: ProfitabilityMatrixGridComponent;
  let fixture: ComponentFixture<ProfitabilityMatrixGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfitabilityMatrixGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfitabilityMatrixGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
