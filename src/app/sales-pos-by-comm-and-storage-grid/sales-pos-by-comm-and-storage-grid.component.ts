import { Component, OnInit } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import {serviceUrl} from "../app.module";
import { Http, Headers, RequestOptions } from '@angular/http';
import { UserRoleService } from '../user-role.service';

@Component({
  selector: 'app-sales-pos-by-comm-and-storage-grid',
  templateUrl: './sales-pos-by-comm-and-storage-grid.component.html',
  styleUrls: ['./sales-pos-by-comm-and-storage-grid.component.scss']
})
export class SalesPosByCommAndStorageGridComponent implements OnInit {
  gridOptions: GridOptions;
  gridColumnApi;
  gridApi;

  getRowNodeId;
  defaultColDef;
  columnDefs;
  rowData;
  currentUser;
  constructor(private http: Http, private role: UserRoleService) {
    this.currentUser = this.role.getUser();
    this.gridOptions = <GridOptions>{
      onGridReady: () => {
        // this.gridOptions.columnApi.autoSizeAllColumns();
        // this.gridOptions.api.sizeColumnsToFit();

      }
    };
    this.defaultColDef = { editable: false };
    this.getRowNodeId = function (data) {
      return data.id;
    };

    this.columnDefs = [
      { headerName: "StorageName", field: "StorageName", pinned: 'left'},
      { headerName: "SaleType", field: "SaleType", pinned: 'left' },
      { headerName: "Commodity", field: "Commodity", pinned: 'left' },

      { headerName: "CropYear", field: "CropYear", pinned: 'left' },
      { headerName: "StartingQuantity", field: "StartingQuantity", pinned: 'left' },
      { headerName: "SoldQuantity", field: "SoldQuantity", pinned: 'left'},
      { headerName: "UnsoldQuantity", field: "UnsoldQuantity", pinned: 'left' },
      { headerName: "PPDestination", field: "PPDestination", pinned: 'left'},
      { headerName: "SaleValuePerUnit", field: "SaleValuePerUnit", pinned: 'left' },
      { headerName: "FreightNTPLocationDifferential", field: "FreightNTPLocationDifferential", pinned: 'left' ,hide:true},
      { headerName: "FreightFarmToInventoryLocation", field: "FreightFarmToInventoryLocation", pinned: 'left',hide:true },
      { headerName: "FreightInventoryToSaleLocation", field: "FreightInventoryToSaleLocation", pinned: 'left' ,hide:true},
      { headerName: "CarryFumigationCost", field: "CarryFumigationCost", pinned: 'left' ,hide:true},
      { headerName: "ExFarmPrice", field: "ExFarmPrice", pinned: 'left' ,hide:true},
      { headerName: "M2MRevenue", field: "M2MRevenue", pinned: 'left',hide:true },
      { headerName: "M2MProfitLoss", field: "M2MProfitLoss", pinned: 'left' ,hide:true},
      { headerName: "M2MProfit", field: "M2MProfit", pinned: 'left',hide:true },
      { headerName: "M2MROI", field: "M2MROI", pinned: 'left',hide:true },
      { headerName: "TargetROI", field: "TargetROI", pinned: 'left',hide:true },
      { headerName: "WeightedAvgGrowingProfitPerUnit", field: "WeightedAvgGrowingProfitPerUnit", pinned: 'left',hide:true },
      { headerName: "WeightedAverageRevenue", field: "WeightedAverageRevenue", pinned: 'left',hide:true },
      { headerName: "AvgGrowingCost", field: "AvgGrowingCost", pinned: 'left',hide:true },
      { headerName: "WeightedAvgGrowingProfitPerHA", field: "WeightedAvgGrowingProfitPerHA", pinned: 'left',hide:true },
      { headerName: "TotalAssetsEmployedCostPerCrop", field: "TotalAssetsEmployedCostPerCrop", pinned: 'left',hide:true },
      { headerName: "TotalAssetsEmployedToGrow", field: "TotalAssetsEmployedToGrow", pinned: 'left',hide:true },
      { headerName: "AverageYield", field: "AverageYield", pinned: 'left',hide:true },
      { headerName: "SoldPerc", field: "SoldPerc", pinned: 'left',hide:true },
      { headerName: "UnsoldPerc", field: "UnsoldPerc", pinned: 'left',hide:true},
      { headerName: "U_ID", field: "U_ID", pinned: 'left' ,hide:true},
      { headerName: "ROI", field: "DateCompleted", pinned: 'left',hide:true }
    ];
    this.rowData = [];
  }

  ngOnInit() {
    this.getSalesPosByCommodityAndStorage();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  selectAllRows() {
    this.gridOptions.api.selectAll();
  }


  getSalesPosByCommodityAndStorage() {

    let url = serviceUrl+'SalesAndM2MPosition/SalesPositionPS/api/v1/GetSalesPositionByCommodityAndStorage?U_ID=' + this.currentUser.USER.U_ID;
    this.http.get(url).subscribe((data) => {
      if (data.json() != null) {
        this.rowData = data.json()?data.json().SalesPosition:[];
      } else {
        this.rowData = [];
      }
    })
  }

}
