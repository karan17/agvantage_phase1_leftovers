import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import { MessageService } from '../message-service.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { forEach } from '@angular/router/src/utils/collection';
import { serviceUrl } from "../app.module";
declare var google;
declare var $: any;
@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
    brokerOffices: any = { broker_office: [] };
    bulkHandlers: any;
    titles: Array<String>;
    businesstypes: Array<String>;
    title: string;
    check: boolean;
    USER: any;
	U_NGR: any;
    mnum1: any;
    mnumt1: any;
    address: any;
    place: any;
    reasons: Array<Object>;
    showbox: boolean;
    pswrdInvalid: boolean;
    minpswrdlength: boolean;
    confirmcheck: boolean;
    postal_code: any;
    country: any;
    state: any;
    city: any;
    airport: any;
    url: string;
    NGRStatus: string;
    userStatusRes: any;
    signupRes: any;
    checking: boolean;
    headers: Headers;
    options1: RequestOptions;
    hideValid: boolean;
    farmingRegions: Array<Object>;
    farmRegions: any;
    zipcode; farmregion; titleList;

    @ViewChild('noBDModal') noBDModal;
    @ViewChild('userform') public userform;
    public options = { types: ['address'], componentRestrictions: { country: 'AUS' } };
    constructor(private router: Router, private http: Http, private spinnerService: Ng4LoadingSpinnerService, private _ngZone: NgZone, private msg: MessageService) {
        this.USER = { U_TYPE: "", U_A_COUNTRY: "Australia" };
        this.userStatusRes = {};
        this.signupRes = {};
        this.checking = false;
        this.check = false;

        this.showbox = false;
        this.pswrdInvalid = false;
        this.titleList = [{ title: 'Mr' }, { title: 'Mrs' }, { title: 'Other' }]
		this.reasons = [
			{img: "fa fa-briefcase", why: "Comprehensive Farm Marketing Plans"},
			{img: "fa fa-support", why: "Marketing Support"},
			{img: "fa fa-bell", why: "Automation, Price Alerts & Reminders"},
			{img: "fa fa-line-chart", why: "Always know their position"},
			{img: "fa fa-bar-chart", why: "Records, History, & Reports - in one place "},
			{img: "fa fa-lock", why: "Highly Secure "}];
        // Region Name	State	Coordinates
        // Emerald - Central QLD	QLD-XF	"-23.519173,148.16586699999993"
        // Mt Mclaren - Central QLD	QLD-XF	"-22.3468907,147.65479679999999"
        // Southern QLD	QLD-XF	"-27.5999916,152.97132090000002"
        // Northern NSW	NSW	"-19.4914108,132.55096030000004"
        this.farmingRegions = [
            { id: 1, regionName: 'Emerald - Central QLD', state: 'QLD-XF', coordinates: '-23.519173,148.16586699999993' },
            { id: 2, regionName: 'Mt Mclaren - Central QLD', state: 'QLD-XF', coordinates: '-22.3468907,147.65479679999999' },
            { id: 3, regionName: 'Southern QLD', state: 'QLD-XF', coordinates: '-27.5999916,152.97132090000002' },
            { id: 4, regionName: 'Northern NSW', state: 'NSW', coordinates: '-19.4914108,132.55096030000004' }
        ];
        this.getRegion();
    }

    getRegion() {
        let url = serviceUrl + 'AgvGetRegionSB/AGVGetRegionPS/api/v1/getRegion?getRegion=true';
        // console.log();
        this.http.get(url).subscribe(data => {
            // Read the result field from the JSON response.
            console.log(data.json().getRegion);
            this.farmRegions = data.json().getRegion;
            console.log('--- farm region', this.farmRegions);
        });
    }

    selectedRegion() {
        console.log('...selected region', this.USER.U_Farm_Region);
    }

    U_FINDREGN; ob;
    setSelectedRegion() {
        // console.log('...selected region popup', this.USER.U_FINDREGN);
        // this.USER.U_Farm_Region = this.U_FINDREGN;
        //get lat long
        // this.farmingRegions.forEach(obj,(){

        // })
        // console.log(obj);
        // }
        for (let i = 0; i < this.farmRegions.length; i++) {
            // console.log('--- farm ',this.farmRegions[i]);
            if (this.U_FINDREGN == this.farmRegions[i].RegionID) {
                console.log('--- farm', this.farmRegions[i]);
                this.ob = this.farmRegions[i]
            }
        }

        // let ob = this.farmRegions;
        console.log('...selected region popup', this.ob);
        this.USER.U_Farm_Region = this.ob.RegionName;
        console.log('...selected region popup', this.USER.U_Farm_Region);
        let coords = this.ob.RegionCoordinates.split(",");
        let currentPosition = new google.maps.LatLng(coords[0], coords[1]);//lat,long
        var markers = [];
        // Clear out the old markers.
        markers.forEach(function (marker) {
            marker.setMap(null);
        });
        markers = [];
        var originIcon = 'https://chart.googleapis.com/chart?' +
            'chst=d_map_pin_letter&chld=O|FFFF00|000000';
        var icon = {
            url: originIcon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
        };

        var map = new google.maps.Map(document.getElementById('map'), {
            center: currentPosition,
            zoom: 13,
            mapTypeId: 'roadmap'
        });

        // Create a marker for each place.
        markers.push(new google.maps.Marker({
            map: map,
            icon: icon,
            position: currentPosition
        }));
    }

    validatePswrd() {
        if (this.USER.pswrd === this.USER.confirmpswrd) {
            this.confirmcheck = false;
        } else {
            this.confirmcheck = true;
        }
    }

    alphanumeric(inputtxt) {
        if (inputtxt.length < 6) {
            this.minpswrdlength = true;
        } else {
            this.minpswrdlength = false;
        }
        if (inputtxt.search(/[a-z]/i) < 0 || inputtxt.search(/[0-9]/) < 0) {
            this.pswrdInvalid = true;
        } else {
            this.pswrdInvalid = false;
        }
    }
    getAddress(place: Object) {
        console.log("Address", place);
        this.place = place;

    }
    setCity(event) {

        this._ngZone.run(() => {

            this.USER.U_A_CITY = event;
            setTimeout(() => {
                this.getBulkHandler();
            })

        })

    }
    keyPress(event: any) {
        const pattern = /[0-9\+\-\ ]/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
        if (this.USER.U_MOBILE && (this.USER.U_MOBILE.length == 4 || this.USER.U_MOBILE.length == 8)) {
            this.USER.U_MOBILE = this.USER.U_MOBILE + '-';
        }
        if (this.USER.U_TELEPHONE && (this.USER.U_TELEPHONE.length == 2 || this.USER.U_TELEPHONE.length == 7)) {
            this.USER.U_TELEPHONE = this.USER.U_TELEPHONE + '-';
        }

    }
    keyPress1(event: any) {
        const pattern = /[0-9\+\-\ ]/;

        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }

    }

    onBlurMethod() {
        alert('on blur...');
    }

    gotologin() {
        this.router.navigate(['login']);
    }
    watchtype() {

        if (this.USER.U_TYPE === "SELLERS") {
            this.showbox = true;
        } else {
            this.showbox = false;
        }
    }
    ngOnInit() {
        $('.selectpicker').selectpicker({
            style: 'btn-default',
            width: "100%",
            size: 4
        });
        $('.selectpicker1').selectpicker({
            style: 'btn-default',
            width: "100%",
            size: 4
        });
        this.getBrokerOffice();
        console.log(this.userform);
    }

    abnErrorMsg; showAlert = false; showNGRTextBox; showNGRDDL; ngrDDL;
    ngrlist = [];
    abnLookUp() {
        this.spinnerService.show();
        // if(this.USER.U_ABN!=undefined || this.USER.U_ABN!=''){
        this.url = serviceUrl + "ABNLookupSB/ABNLookUpPS/api/v1/abnLookUp?ABN=" + this.USER.U_ABN;
        this.http.get(this.url).subscribe(data => {
            // Read the result field from the JSON response.

            this.spinnerService.hide();
            if (data.json().errorCode == "ABN-NOTEXISTS") {
                this.abnErrorMsg = "ABN number is incorrect , please contact agvantege system administrator";
                this.showAlert = true;
            } else if (data.json().errorCode == "ABNR-001") {
                this.abnErrorMsg = "ABN Service is Incorrect";
                this.showAlert = true;
            }
            if (data.json().CompanyName) {
                this.showAlert = false;
                var abnres = data.json();
                this.USER.U_TRADING_NAME = abnres.CompanyName;
                console.log('--- ABN data.json() ', data.json());
                if (!abnres.NGRUserList) {
                    this.showNGRTextBox = true;
                } else {
                    this.showNGRDDL = true;
                    this.ngrlist = abnres.NGRUserList;
                    console.log('arr:', this.ngrlist);
                }
            }
        });

    }

    selectedNGR(value) {
        this.USER.U_NGR = value;

        console.log("selected ngr", this.USER.NGRDDL);
        for (let i = 0; i < this.ngrlist.length; i++) {
            if (this.ngrlist[i].NGR == this.USER.U_NGR) {
                // console.log(this.ngrlist[i].NGRUserList.Users.User);
                this.USER.U_TITLE = this.ngrlist[i].Title;
                this.USER.U_FNAME = this.ngrlist[i].Fname;
                this.USER.U_LNAME = this.ngrlist[i].Lname;
                this.USER.U_USERNAME = this.ngrlist[i].Username;
                this.USER.U_MOBILE = this.ngrlist[i].Mobile;
                this.USER.U_TELEPHONE = this.ngrlist[i].Telephone;
                this.USER.U_A_STREET = this.ngrlist[i].A_Street;
                this.USER.U_A_CITY = this.ngrlist[i].A_City;
                this.USER.U_A_STATE = this.ngrlist[i].A_State;
                this.USER.U_A_COUNTRY = this.ngrlist[i].A_Country;
                this.USER.U_A_POSTCODE = this.ngrlist[i].A_Postcode;
            }
        }
    }

    checkUserStatus() {
        console.log(this.userform);
        let emailValid = this.userform.form.controls.email.status;
        this.checking = true;
        if (this.USER.U_USERNAME != undefined && this.USER.U_USERNAME.length > 0 && emailValid === "VALID") {
            this.checking = true;
            this.url = serviceUrl + "AccountGatewayCheckUserStatus/AccountGatewayUserStatusPS/checkUserStatus?USERNAME=" + this.USER.U_USERNAME;
            this.http.get(this.url).subscribe(data => {
                // Read the result field from the JSON response.
                this.checking = false;
                this.hideValid = true;
                console.log(data.json());
                this.userStatusRes = data.json();
            },(err)=>{
                alert("Server error occured");
                this.checking = false;
            });
        } else if (emailValid === "INVALID") {
            this.checking = false;
            this.hideValid = false;
        }
    }
    checkNgr() {
        this.spinnerService.show();
        this.url = serviceUrl + "AccountGatewayCheckNGRStatusSB/ProxyService/AccountGatewayNGRPS/api/v1/checkNGRStatus?NGR=" + this.USER.U_NGR;
        this.http.get(this.url).subscribe(data => {
            // Read the result field from the JSON response.
            this.spinnerService.hide();
            console.log(data.json());
            var ngres = data.json();
            this.NGRStatus = ngres.Status;
            if (this.NGRStatus == "NOT EXISTS") {
                this.USER.U_TITLE = '',
                    this.USER.U_FNAME = '',
                    this.USER.U_LNAME = '',
                    this.USER.U_USERNAME = '',
                    this.USER.U_MOBILE = '',
                    this.USER.U_TELEPHONE = '',
                    this.USER.U_A_STREET = '',
                    this.USER.U_A_CITY = '',
                    this.USER.U_A_STATE = '',
                    this.USER.U_A_COUNTRY = '',
                    this.USER.U_A_POSTCODE = ''
            }
        });

    }
    private _markFormPristine(form: FormGroup | NgForm): void {
        Object.keys(form.controls).forEach(control => {
            form.controls[control].markAsDirty();
        });
    }

    status: { isopen: boolean } = { isopen: false };

    toggleDropdown($event: MouseEvent): void {
        $event.preventDefault();
        $event.stopPropagation();
        this.status.isopen = !this.status.isopen;
    }

    change(value: boolean): void {
        this.status.isopen = value;
    }

    createUser() {
        console.log(this.userform);
        if (this.userform.valid) {
            console.log("Form Submitted!");

            console.log(this.userform);
            this.headers = new Headers({
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json'
            });
            this.options1 = new RequestOptions({ headers: this.headers });
            let body = `USER=${this.USER}`;
            this.url = serviceUrl + "AccountGatewayInitSignup/AccountGatewayInitSignup/doInitSignUp";
            // this.url = "http://129.154.71.56:9073/AccountGatewayInitSignup/AccountGatewayInitSignup/doInitSignUp";
            console.log(this.USER);
            delete this.USER.tradingname;
            let USER = { USER: {} };
            USER.USER = this.USER;
            this.spinnerService.show();
            this.http.post(this.url, this.USER, this.options1).subscribe(data => {
                // Read the result field from the JSON response.
                console.log(data.json());
                this.signupRes = data.json();
                if (this.signupRes.errorCode) {
                    // this.msg.setMessage('Incorrect signup details');
                    this.spinnerService.hide();
                    // this.noBDModal.show();
                    this.showAlert = true;
                    this.abnErrorMsg = 'Incorrect Registration Details';
                }
                else {
                    this.msg.setMessage(this.signupRes.Result.Outcome);
                    this.spinnerService.hide();
                    this._ngZone.run(() => this.router.navigate(['login']));
                }
            }, err => {
                this.signupRes = err.json();
                console.log(this.signupRes);
                // this.noBDModal.show();
                this.spinnerService.hide();
                this.showAlert = true;
                this.abnErrorMsg = 'Incorrect Registration Details';
            });
        } else {
            if (this.userform.invalid) {
                this._markFormPristine(this.userform);
            }

        }
    }
    getBulkHandler() {
        let url = `${serviceUrl}BulkHandlerSB/BulkHandlersPS/api/v1/GetBulkHandlers?State=${this.USER.U_A_STATE}`;
        this.http.get(decodeURI(url)).subscribe((data) => {
            this.bulkHandlers = data.json().BulkHandlers ? data.json().BulkHandlers.BulkHandler : [];

        },
            (err) => {
                this.bulkHandlers = [];
            })


    }
    getBrokerOffice() {
        let url = serviceUrl + "AgvantageGetBrokerOfficeSB/AgvantageGetBrokerOfficeRestPS/api/v1/GetBrokerOffice?broker_name=Agvantage Commodities Pty Ltd";
        this.http.get(url).subscribe((data) => {
            this.brokerOffices = data.json() ? data.json().broker_offices : [];
        }),
            (err) => {
                this.brokerOffices = [];
            }
    }

    login() {
        this._ngZone.run(() => this.router.navigate(['login']));
    }
}
