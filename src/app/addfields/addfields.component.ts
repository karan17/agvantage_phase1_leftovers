import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import { MessageService } from '../message-service.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { FormGroup, FormBuilder, Validators, FormControl, NgForm } from '@angular/forms';
import { forEach } from '@angular/router/src/utils/collection';
import { serviceUrl } from "../app.module";
declare var google;
declare var $: any;
@Component({
    selector: 'add-newfields',
    templateUrl: './addfields.component.html',
    styleUrls: ['./addfields.component.scss']
})
export class AddNewFieldsComponent implements OnInit {

    crop_year: any;
	StorageType_name:any;
	Commodity_name:any;
	Chemical_Name:any;
	Country_name:any;
	headers: Headers;
	options: RequestOptions;
	urlAddCrop: string;
	signupRes: any;
	yearObj: any;
	StorageType_namerObj:any;
	Commodity_nameObj:any;
	Chemical_NameObj:any;
	Country_nameObj:any;

	ngOnInit() {
    }

	constructor(private router: Router, private http: Http, private spinnerService: Ng4LoadingSpinnerService, private _ngZone: NgZone, private msg: MessageService) {}

	addCropYear() {

            console.log("Form Submitted!");


            this.headers = new Headers({
                'Content-Type': 'application/json; charset=UTF-8',
                'Accept': 'application/json'
            });
            this.options = new RequestOptions({ headers: this.headers });
			this.yearObj = {Crop_year: this.crop_year.trim()};
            this.urlAddCrop = serviceUrl + "AgvantageAdminAddCropYearSB/AgvantageAdminAddCropYearRestPS/api/v1/AddCropYear";
            // this.url = "http://129.154.71.56:9073/AccountGatewayInitSignup/AccountGatewayInitSignup/doInitSignUp";

		console.log("this.yearObj",this.yearObj);

		//  this.http.post(this.urlAddCrop, this.yearObj, this.options);

            this.http.post(this.urlAddCrop, this.yearObj, this.options).subscribe(data => {
                // Read the result field from the JSON response.
				console.log("this.crop_year",this.crop_year);
                console.log(data.json());
                this.signupRes = data.json();
                if (this.signupRes.errorCode) {
                    // this.msg.setMessage('Incorrect signup details');
                    this.spinnerService.hide();
                    // this.noBDModal.show();

                }
                else {
                  //  this.msg.setMessage(this.signupRes.Result.Outcome);
                    this.spinnerService.hide();
                //    this._ngZone.run(() => this.router.navigate(['login']));
                }
            }, err => {
                this.signupRes = err.json();
               // console.log(this.signupRes);
				this.crop_year="";
                // this.noBDModal.show();
                this.spinnerService.hide();

            });





    }

	addStorageType() {

		console.log("Form Submitted!");


		this.headers = new Headers({
			'Content-Type': 'application/json; charset=UTF-8',
			'Accept': 'application/json'
		});
		this.options = new RequestOptions({ headers: this.headers });
		this.StorageType_namerObj = {StorageType_name: this.StorageType_name.trim()};
		this.urlAddCrop = serviceUrl + "AgvantageAdminAddStorageType/AgvantageAdminAddStorageTypeRestPS/api/v1/AddStorageType";
		// this.url = "http://129.154.71.56:9073/AccountGatewayInitSignup/AccountGatewayInitSignup/doInitSignUp";

		console.log("this.yearObj",this.yearObj);

		//  this.http.post(this.urlAddCrop, this.yearObj, this.options);

		this.http.post(this.urlAddCrop, this.StorageType_namerObj, this.options).subscribe(data => {
			// Read the result field from the JSON response.
			console.log("this.crop_year",this.crop_year);
			console.log(data.json());
			this.signupRes = data.json();
			if (this.signupRes.errorCode) {
				// this.msg.setMessage('Incorrect signup details');
				this.spinnerService.hide();
				// this.noBDModal.show();

			}
			else {
				//  this.msg.setMessage(this.signupRes.Result.Outcome);
				this.spinnerService.hide();
				//    this._ngZone.run(() => this.router.navigate(['login']));
			}
		}, err => {
			this.signupRes = err.json();
			// console.log(this.signupRes);
			this.crop_year="";
			// this.noBDModal.show();
			this.spinnerService.hide();

		});





	}

	addCommodityType() {

		console.log("Form Submitted!");


		this.headers = new Headers({
			'Content-Type': 'application/json; charset=UTF-8',
			'Accept': 'application/json'
		});
		this.options = new RequestOptions({ headers: this.headers });
		this.Commodity_nameObj = {Commodity_name: this.Commodity_name.trim()};
		this.urlAddCrop = serviceUrl + "AgvantageAdminAddCommoditySB/AgvantageAdminAddCommodityRestPS/api/v1/AddCommodity";
		// this.url = "http://129.154.71.56:9073/AccountGatewayInitSignup/AccountGatewayInitSignup/doInitSignUp";

		console.log("this.yearObj",this.yearObj);

		//  this.http.post(this.urlAddCrop, this.yearObj, this.options);

		this.http.post(this.urlAddCrop, this.Commodity_nameObj, this.options).subscribe(data => {
			// Read the result field from the JSON response.
			console.log("this.crop_year",this.crop_year);
			console.log(data.json());
			this.signupRes = data.json();
			if (this.signupRes.errorCode) {
				// this.msg.setMessage('Incorrect signup details');
				this.spinnerService.hide();
				// this.noBDModal.show();

			}
			else {
				//  this.msg.setMessage(this.signupRes.Result.Outcome);
				this.spinnerService.hide();
				//    this._ngZone.run(() => this.router.navigate(['login']));
			}
		}, err => {
			this.signupRes = err.json();
			// console.log(this.signupRes);
			this.crop_year="";
			// this.noBDModal.show();
			this.spinnerService.hide();

		});





	}

	addChemicalName() {

		console.log("Form Submitted!");


		this.headers = new Headers({
			'Content-Type': 'application/json; charset=UTF-8',
			'Accept': 'application/json'
		});
		this.options = new RequestOptions({ headers: this.headers });
		this.Chemical_NameObj = {Chemical_Name: this.Chemical_Name.trim()};
		this.urlAddCrop = serviceUrl + "AgvantageAdminAddChemicalNamesSB/AgvantageAddChemicalNamesRestPS/api/v1/AddChemicalNames";
		// this.url = "http://129.154.71.56:9073/AccountGatewayInitSignup/AccountGatewayInitSignup/doInitSignUp";

		console.log("this.yearObj",this.yearObj);

		//  this.http.post(this.urlAddCrop, this.yearObj, this.options);

		this.http.post(this.urlAddCrop, this.Chemical_NameObj, this.options).subscribe(data => {
			// Read the result field from the JSON response.
			console.log("this.crop_year",this.crop_year);
			console.log(data.json());
			this.signupRes = data.json();
			if (this.signupRes.errorCode) {
				// this.msg.setMessage('Incorrect signup details');
				this.spinnerService.hide();
				// this.noBDModal.show();

			}
			else {
				//  this.msg.setMessage(this.signupRes.Result.Outcome);
				this.spinnerService.hide();
				//    this._ngZone.run(() => this.router.navigate(['login']));
			}
		}, err => {
			this.signupRes = err.json();
			// console.log(this.signupRes);
			this.crop_year="";
			// this.noBDModal.show();
			this.spinnerService.hide();

		});





	}

	addCountrName() {

		console.log("Form Submitted!");


		this.headers = new Headers({
			'Content-Type': 'application/json; charset=UTF-8',
			'Accept': 'application/json'
		});
		this.options = new RequestOptions({ headers: this.headers });
		this.Country_nameObj = {Country_name: this.Country_name.trim()};
		this.urlAddCrop = serviceUrl + "AgvantageAdminAddCountrySB/AgvantageAddCountryRestPS/api/v1/AddCountry";
		// this.url = "http://129.154.71.56:9073/AccountGatewayInitSignup/AccountGatewayInitSignup/doInitSignUp";

		console.log("this.yearObj",this.yearObj);

		//  this.http.post(this.urlAddCrop, this.yearObj, this.options);

		this.http.post(this.urlAddCrop, this.Country_nameObj, this.options).subscribe(data => {
			// Read the result field from the JSON response.
			console.log("this.crop_year",this.crop_year);
			console.log(data.json());
			this.signupRes = data.json();
			if (this.signupRes.errorCode) {
				// this.msg.setMessage('Incorrect signup details');
				this.spinnerService.hide();
				// this.noBDModal.show();

			}
			else {
				//  this.msg.setMessage(this.signupRes.Result.Outcome);
				this.spinnerService.hide();
				//    this._ngZone.run(() => this.router.navigate(['login']));
			}
		}, err => {
			this.signupRes = err.json();
			// console.log(this.signupRes);
			this.crop_year="";
			// this.noBDModal.show();
			this.spinnerService.hide();

		});





	}



    addNewFields() {
        this._ngZone.run(() => this.router.navigate(['addNewFields']));
    }
}
