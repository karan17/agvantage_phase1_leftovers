import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactsFavNameDetailsCellRendererComponent } from './contacts-fav-name-details-cell-renderer.component';

describe('ContactsFavNameDetailsCellRendererComponent', () => {
  let component: ContactsFavNameDetailsCellRendererComponent;
  let fixture: ComponentFixture<ContactsFavNameDetailsCellRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactsFavNameDetailsCellRendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactsFavNameDetailsCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
