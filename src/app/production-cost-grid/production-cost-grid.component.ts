import { Component, ViewChild, OnInit, ChangeDetectorRef } from "@angular/core";
import { GridOptions } from "ag-grid/main";
@Component({
  selector: 'app-production-cost-grid',
  templateUrl: './production-cost-grid.component.html',
  styleUrls: ['./production-cost-grid.component.scss']
})
export class ProductionCostGridComponent implements OnInit {
  gridOptions: GridOptions;
  gridApi;
  gridColumnApi;

  columnDefs;

  rowData;
  defaultColDef;
  getRowNodeId;
  gridOptions1: GridOptions;
  gridApi1;
  gridColumnApi1;

  columnDefs1;

  rowData1;
  defaultColDef1;
  getRowNodeId1;
  ngOnInit() {

  }

  constructor(private cdRef: ChangeDetectorRef) {

    // we pass an empty gridOptions in, so we can grab the api out

    this.gridOptions = <GridOptions>{
      onGridReady: () => {
        this.gridOptions.api.sizeColumnsToFit();

      }
    };
    this.gridOptions1 = <GridOptions>{
      onGridReady: () => {
        this.gridOptions1.api.sizeColumnsToFit();

      }
    };
    this.columnDefs = [
      {
        headerName: "Area and Yield by Commodity", groupId: "area", headerClass: "text-center",
        children: [{ headerName: "", field: "attrs", width: 300 },
        { headerName: "Sample", field: "sample" },
        { headerName: "Wheat", field: "wheat" },
        { headerName: "Durum", field: "durum" },
        { headerName: "Feed Barley", field: "feed" },
        { headerName: "Malt Barley", field: "malt" },
        { headerName: "Canola", field: "canola" },
        { headerName: "Chickpeas", field: "chickpeas" },
        { headerName: "Faba Beans", field: "faba" }]
      }
    ];
    this.columnDefs1 = [
      {
        headerName: "Total Costs by Commodity", groupId: "costs", headerClass: "text-center",
        children: [{ headerName: "", field: "attrs", width: 300 },
        { headerName: "Sample", field: "sample" },
        { headerName: "Wheat", field: "wheat" },
        { headerName: "Durum", field: "durum", },
        { headerName: "Feed Barley", field: "feed" },
        { headerName: "Malt Barley", field: "malt" },
        { headerName: "Canola", field: "canola" },
        { headerName: "Chickpeas", field: "chickpeas" },
        { headerName: "Faba Beans", field: "faba" }]
      }
    ];
    this.rowData1 = [{
      id: "gg",
      attrs: "Repair and Maintainence",
      sample: "$10.00",
      wheat: "$20.00",
      durum: "$20.00",
      feed: "$20.00",
      malt: "$20.00",
      canola: "$20.00",
      chickpeas: "$20.00",
      faba: "$20.00"
    },
    {
      id: "gg",
      attrs: "Interest(Operating)",
      sample: "$4.00",
      wheat: "$4.00",
      durum: "$4.00",
      feed: "$4.00",
      malt: "$4.00",
      canola: "$4.00",
      chickpeas: "$4.00",
      faba: "$4.00"
    },
    {
      id: "gg",
      attrs: "Interest(Other)",
      sample: "$2.00",
      wheat: "$0.0",
      durum: "$0.0",
      feed: "$0.0",
      malt: "$0.0",
      canola: "$0.0",
      chickpeas: "$0.0",
      faba: "$0.0"
    },
    {
      id: "gg",
      attrs: "Rent/Mortgage/Opportunity",
      sample: "$300.00",
      wheat: "$175.00",
      durum: "$175.00",
      feed: "$175.00",
      malt: "$175.00",
      canola: "$175.00",
      chickpeas: "$175.00",
      faba: "$175.00"
    },
    {
      id: "gg",
      attrs: "Seed",
      sample: "$130.00",
      wheat: "$20.00",
      durum: "$30.00",
      feed: "$30.00",
      malt: "$30.00",
      canola: "$30.00",
      chickpeas: "$30.00",
      faba: "$30.00"
    },
    {
      id: "gg",
      attrs: "Fertilizer",
      sample: "$130.00",
      wheat: "$20.00",
      durum: "$30.00",
      feed: "$30.00",
      malt: "$30.00",
      canola: "$30.00",
      chickpeas: "$30.00",
      faba: "$30.00"
    },
    {
      id: "gg",
      attrs: "Chemicals",
      sample: "$130.00",
      wheat: "$20.00",
      durum: "$30.00",
      feed: "$30.00",
      malt: "$30.00",
      canola: "$30.00",
      chickpeas: "$30.00",
      faba: "$30.00"
    },
    {
      id: "gg",
      attrs: "Machine Hire Contracting",
      sample: "$130.00",
      wheat: "$20.00",
      durum: "$30.00",
      feed: "$30.00",
      malt: "$30.00",
      canola: "$30.00",
      chickpeas: "$30.00",
      faba: "$30.00"
    },
    {
      id: "gg",
      attrs: "Supplies/Sundries",
      sample: "$130.00",
      wheat: "$20.00",
      durum: "$30.00",
      feed: "$30.00",
      malt: "$30.00",
      canola: "$30.00",
      chickpeas: "$30.00",
      faba: "$30.00"
    },
    {
      id: "gg",
      attrs: "Gas,fuel,oil",
      sample: "$130.00",
      wheat: "$20.00",
      durum: "$30.00",
      feed: "$30.00",
      malt: "$30.00",
      canola: "$30.00",
      chickpeas: "$30.00",
      faba: "$30.00"
    },
    {
      id: "gg",
      attrs: "Taxes",
      sample: "$130.00",
      wheat: "$20.00",
      durum: "$30.00",
      feed: "$30.00",
      malt: "$30.00",
      canola: "$30.00",
      chickpeas: "$30.00",
      faba: "$30.00"
    },
    {
      id: "gg",
      attrs: "Insurance",
      sample: "$130.00",
      wheat: "$20.00",
      durum: "$30.00",
      feed: "$30.00",
      malt: "$30.00",
      canola: "$30.00",
      chickpeas: "$30.00",
      faba: "$30.00"
    },
    {
      id: "gg",
      attrs: "Utilities",
      sample: "$130.00",
      wheat: "$20.00",
      durum: "$30.00",
      feed: "$30.00",
      malt: "$30.00",
      canola: "$30.00",
      chickpeas: "$30.00",
      faba: "$30.00"
    },
    {
      id: "gg",
      attrs: "Marketing",
      sample: "$130.00",
      wheat: "$20.00",
      durum: "$30.00",
      feed: "$30.00",
      malt: "$30.00",
      canola: "$30.00",
      chickpeas: "$30.00",
      faba: "$30.00"
    },
    {
      id: "gg",
      attrs: "Freight",
      sample: "$130.00",
      wheat: "$20.00",
      durum: "$30.00",
      feed: "$30.00",
      malt: "$30.00",
      canola: "$30.00",
      chickpeas: "$30.00",
      faba: "$30.00"
    }];
    this.rowData = [
      {
        id: "aa",
        attrs: "Area(acres)",
        sample: "",
        wheat: "12,355.00",
        durum: "4942.00",
        feed: "2471.00",
        malt: "0.00",
        canola: "1235.00",
        chickpeas: "1235.00",
        faba: "1235.00"
      },
      {
        id: "bb",
        attrs: "Yield(acres)",
        sample: "1.21",
        wheat: "1.31",
        durum: "0.21",
        feed: "0.73",
        malt: "1.23",
        canola: "2.33",
        chickpeas: "1.24",
        faba: "1.21"
      },
      {
        id: "cc",
        attrs: "Area(ha.)",
        sample: "1,000.00",
        wheat: "5,000.00",
        durum: "2,000.00",
        feed: "4,0000.00",
        malt: "2,000.00",
        canola: "500.00",
        chickpeas: "500.00",
        faba: "500.00"
      },
      {
        id: "dd",
        attrs: "yield(ha.)",
        sample: "185.00",
        wheat: 35000,
        durum: "",
        feed: "",
        malt: "",
        canola: "",
        chickpeas: "",
        faba: ""
      },
      {
        id: "ee",
        attrs: "Removed Amount (your share farmer %) ",
        sample: "",
        wheat: 35000,
        durum: "",
        feed: "",
        malt: "",
        canola: "",
        chickpeas: "",
        faba: ""
      },
      {
        id: "ff",
        attrs: "Retained Speed",
        sample: "2%",
        wheat: "2%",
        durum: "2%",
        feed: "0%",
        malt: "0%",
        canola: "0%",
        chickpeas: "4%",
        faba: "4%"
      },
      {
        id: "gg",
        attrs: "Storage Available-on farm(mt)",
        sample: "",
        wheat: 35000,
        durum: "",
        feed: "",
        malt: "",
        canola: "",
        chickpeas: "",
        faba: ""
      },
      {
        id: "gg",
        attrs: "Total Production)",
        sample: "1,85,000.00",
        wheat: 35000,
        durum: "",
        feed: "",
        malt: "",
        canola: "",
        chickpeas: "",
        faba: ""
      },
      {
        id: "gg",
        attrs: "Old Crop left to market(tonnes/bales)",
        sample: "$25.00",
        wheat: "$30.00",
        durum: "$30.00",
        feed: "$30.00",
        malt: "$30.00",
        canola: "$30.00",
        chickpeas: "$30.00",
        faba: "$30.00"
      }
    ];
    this.defaultColDef = { editable: false };
    this.defaultColDef1 = { editable: false };
    this.getRowNodeId = function (data) {
      return data.id;
    };
  }



  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  onGridReady1(params) {
    this.gridApi1 = params.api;
    this.gridColumnApi1 = params.columnApi;
  }
  selectAllRows() {
    this.gridOptions.api.selectAll();
  }
}

