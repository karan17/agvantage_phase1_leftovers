import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccountDetailsGridComponent } from './account-details-grid.component';

describe('AccountDetailsGridComponent', () => {
  let component: AccountDetailsGridComponent;
  let fixture: ComponentFixture<AccountDetailsGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccountDetailsGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccountDetailsGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
