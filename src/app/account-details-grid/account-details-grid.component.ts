import { Component, OnInit, ViewChild } from '@angular/core';
import { UserRoleService } from '../user-role.service'
import { Http, Headers, RequestOptions } from '@angular/http';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router } from '@angular/router';
import { GridOptions } from "ag-grid/main";
import { CommodityUpdateCellRendererComponent } from '../commodity-update-cell-renderer/commodity-update-cell-renderer.component';
import { TooltipcellrendererComponent } from '../tooltipcellrenderer/tooltipcellrenderer.component';
import {serviceUrl} from "../app.module";

@Component({
  selector: 'app-account-details-grid',
  templateUrl: './account-details-grid.component.html',
  styleUrls: ['./account-details-grid.component.scss']
})
export class AccountDetailsGridComponent implements OnInit {
  headers: Headers;
  url: string;
  options: RequestOptions;
  Users: any;
  ContractDetails;
  showApproved: Array<boolean>;
  showRejected: Array<boolean>;
  columnDefs;
  gridOptions: GridOptions;
  gridApi;
  gridColumnApi;
  params;
  currentUser;
  rowSelection: any;
  frameworkComponents;
  submitContract;
    rowData;
  @ViewChild('modalContract') public modalContract;
  @ViewChild('modalInvGrid') public modalInvGrid;
  @ViewChild('addContractForm') public addContractForm;

  constructor(private router: Router, private role: UserRoleService, private http: Http, private spinnerService: Ng4LoadingSpinnerService) {
    this.showApproved = [];
    this.showRejected = [];
    this.ContractDetails = {};
    this.currentUser = this.role.getUser();
    this.gridOptions = <GridOptions>{
      onGridReady: (params) => {
        this.params = params;
        this.gridApi = params.api;
        this.gridOptions.api.sizeColumnsToFit();
      }
    };

    this.columnDefs = [

      { headerName: "Actions", checkboxSelection: true, headerCheckboxSelection: true, field: "actions", width: 200, cellRenderer: 'commodityUpdateCellRendererComponent', cellEditor: 'agRichSelect', editable: true },

      { headerName: "Field From", field: "field_from", headerClass: "text-center" },
      { headerName: "Date Harvested", field: "date_harvested" },
      { headerName: "Commodity Name", field: "comm_name" },
      { headerName: "Sold Inventory", field: "soldInventory" },
      { headerName: "Unsold Quantity", field: "unsoldQuantity" },
      { headerName: "Grade Name", field: "grade_name" },
      { headerName: "Storage Name", field: "storage_name" },
      { headerName: "Storage Location", field: "storage_location", tooltipField: "storage_location" },
      { headerName: "Target Price", field: "target_price" },
      { headerName: "Qualities", field: "qualities", width: 200, tooltipField: "qualities" },
      { headerName: "Chemicals", field: "chemicals", width: 200, tooltipField: "chemicals" }
    ];

    this.rowSelection = "single";
    this.frameworkComponents = {
      commodityUpdateCellRendererComponent: CommodityUpdateCellRendererComponent,
      tooltipCellRenderer: TooltipcellrendererComponent
    };
  }

  ngOnInit() {
    if (this.role.getRole() === "superadmin") {

      this.fetchTaskForAdmin();
    }

  }

  gotoHome() {
    this.router.navigate(['dashboard']);
  }

  contract;
  onSelectionChanged(event) {
    var summaryArr = event.api.getSelectedNodes();    
    this.contract = {
      u_id: this.view_id,
      inventory_id: event.api.getSelectedNodes()[0].data.storage_add_cid
    }
    this.openViewModal(this.contract);
  }

  highlightrow(id: number) {
    let index = 0;
    this.gridOptions.api.forEachNode((node) => {
      index++;
      if (node.data.storage_add_cid == id) {
        // Master open detail.  You could also call node.setSelected( true ); for alternate design.
        node.setSelected(true, true);
        this.gridOptions.api.ensureIndexVisible(index - 1, 'bottom')
      }
    });

  }

  getGridData(id) {
    let url = serviceUrl+'AgvInvGetCommodity/AgvInvGetCommPS/api/v1/getInvComm?U_ID=' + id;
    this.http.get(url).subscribe((data) => {
      
      if (data.json() != null) {
        let inventoryData = data.json().comStore;
        inventoryData.forEach((val) => {
          var chemicalsCSV = [];
          var qualitiesCSV = [];
          val.chemicalattributes.chemical.forEach((valin) => {
            chemicalsCSV.push(valin.chemical_name + '-' + valin.chemical_value + '|' + valin.date_applied);
          })
          val.qualityattributes.quality.forEach((valin) => {
            qualitiesCSV.push(valin.quality_name + '-' + valin.quality_value);
          })
          val.chemicals = chemicalsCSV.join(',');
          val.qualities = qualitiesCSV.join(',');

        });        
        this.gridOptions.api.setRowData(inventoryData);
        if (id != undefined) {
          this.highlightrow(id);
        }
      } else {
        this.gridOptions.api.setRowData([])
      }
    })
  }

  approve(user, status, i) {
    let userObj = {
      "USER": {
        "U_ID": user.uId,
        "U_TYPE": user.uType,
        "U_ACTION": status,
        "U_USERNAME": user.uUsername
      }
    }
    this.headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'Accept': 'application/json'
    });
    this.options = new RequestOptions({ headers: this.headers });
    this.url = serviceUrl+"AccountGateWaySignup/AccountGatewaySignup/doSignUp";
    this.spinnerService.show();
    this.http.post(this.url, $.param(userObj.USER), this.options).subscribe((data) => {

      console.log(data.json());
      if (data.json().Result.Outcome == "Success") {
        this.showApproved[i] = true;
        this.showRejected[i] = false;
      } else if (data.json().Result.Outcome == "Your Application is Rejected") {
        this.showRejected[i] = true;
        this.showApproved[i] = false;
      }
      this.spinnerService.hide();
    })
  }

  fetchTaskForAdmin() {

    this.url = serviceUrl+"AccountGatewayFetchTasks/AccountGatewayFetchTasks/doFetchTasks";
    this.headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'Accept': 'application/json'
    });
    this.options = new RequestOptions({ headers: this.headers });
    this.http.post(this.url, $.param({ "getInactive": "true" }), this.options).subscribe((data) => {

      console.log(data.json());
      if (data.json().AgvUser.length >= 1) {
        this.Users = data.json().AgvUser;
      }
      
    })
  }

  openViewModal(contract) {
    console.log('contract', contract);
    this.modalContract.show();
  }

  closeViewModal() {
    this.modalContract.hide();
  }

  view_id;
  openInvGridModal(id) {
    this.view_id = id;
    this.modalInvGrid.show();
    this.getGridData(id);
  }

  closeInvGridModal() {
    this.modalInvGrid.hide();
  }

  
  submitContractDetails() {

    this.ContractDetails.u_id = this.contract.u_id;
    this.ContractDetails.inventory_id = this.contract.inventory_id;

    let url = serviceUrl+'ContractManagement/RegisterContractPS/api/v1/registerContract'
    this.headers = new Headers({
      'Content-Type': 'application/json; charset=UTF-8',
      'Accept': 'application/json'
    });
    this.options = new RequestOptions({ headers: this.headers });
    this.http.post(url, this.ContractDetails, this.options).subscribe((data) => {
      console.log('data', data);
      if(data.status == 200)
      this.submitContract = "Contract registered successfully";
      // if (data.json().AgvUser.length >= 1) {
      //   this.Users = data.json().AgvUser;
      // }
      this.closeViewModal();      
    })
  }

}
