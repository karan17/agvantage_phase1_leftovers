
import {Component, OnInit, ViewChild, NgZone, HostListener} from '@angular/core';
import {Router} from '@angular/router';
import {UserRoleService} from '../user-role.service';

@Component({
  selector: 'app-profitability-matrix',
  templateUrl: './profitability-matrix.component.html',
  styleUrls: ['./profitability-matrix.component.scss']
})
export class ProfitabilityMatrixComponent implements OnInit {
  currentUser;
  EntityName; NGR;
  displayMatrixBy;
  profitabilityMatrix;
  enableGrowingProfit;
  constructor(private _ngZone: NgZone, private router: Router, private role: UserRoleService) { 
    this.profitabilityMatrix={};
    this.currentUser = this.role.getUser();
    console.log("--- current user:", this.currentUser.USER);
    this.EntityName = this.currentUser.USER.U_FNAME;
    this.NGR = this.currentUser.USER.U_NGR;
  }

  ngOnInit() {
    // this.displayMatrixBy = [
    //   {id:1, name : "Growing Profit"},
    //   {id:2, name : "Growing BreakEven Price"},
    //   {id:3, name : "Yield and Price/Unit "}
    // ]

  }

  gotoHome() {
    this._ngZone.run(() => this.router.navigate(['dashboard']));
  }

  selectedOption(){
    // if(this.profitabilityMatrix.profmatby == "Growing Profit"){
    //   console.log("selected option", this.profitabilityMatrix.profmatby);
    //   this.enableGrowingProfit = false;
    // }
  }



}
