import {Component, OnInit, ChangeDetectorRef, ChangeDetectionStrategy, NgZone} from '@angular/core';
import {Router} from '@angular/router';
import {UserRoleService} from '../user-role.service';
import {MessageService} from '../message-service.service';
import {Http, Headers, RequestOptions} from '@angular/http';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import {serviceUrl} from "../app.module";

declare var $: any;

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
	reasons: any;
	username: any;
	password: any;
	url: string;
	headers: Headers;
	options: RequestOptions;
	loginRes: any;
	msgToShow: string;
	errmsg: any;

	constructor(private router: Router, private chRef: ChangeDetectorRef, private _ngZone: NgZone, private role: UserRoleService, private http: Http, public spinnerService: Ng4LoadingSpinnerService, private msg: MessageService) {
		this.reasons = [
			{img: "fa fa-briefcase", why: "Comprehensive Farm Marketing Plans"},
			{img: "fa fa-support", why: "Marketing Support"},
			{img: "fa fa-bell", why: "Automation, Price Alerts & Reminders"},
			{img: "fa fa-line-chart", why: "Always know their position"},
			{img: "fa fa-bar-chart", why: "Records, History, & Reports - in one place "},
			{img: "fa fa-lock", why: "Highly Secure "}];
		this.loginRes = {};
		this.msgToShow = "";
		this.errmsg = "";
		if (this.msg.getMessage().length > 0) {
			this.msgToShow = this.msg.getMessage();
		}
		setTimeout(() => {
			this.msgToShow = "";
		}, 10000);
	}

	ngOnInit() {
	}

	gotoDashboard() {

		this.headers = new Headers({
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
			'Accept': 'application/json'
		});
		this.options = new RequestOptions({headers: this.headers});
		this.url = serviceUrl + "AccountGatewayLoginService/AccountGatewayLoginPS/api/v1/doLogin";
		let userObj = {username: this.username.trim(), password: this.password};
		let body = `username=${this.username.trim()}&password=${this.password}`;
	//	console.log(body);
		this.spinnerService.show();
		this.http.post(this.url, body, this.options).subscribe(data => {
				// Read the result field from the JSON response.
				console.log(data.json());
				this.spinnerService.hide();
				this.msgToShow = "";
				this.loginRes = data.json();
				//this.loginRes.IsValid='true';
				if (this.loginRes.IsValid === 'true' && this.loginRes.UserInRole === "SUPERADMIN") {
					this.errmsg = "";
					this.role.setRole("SUPERADMIN");
					this.role.setUser(this.loginRes);
					this._ngZone.run(() => this.router.navigate(['dashboard']));
				} else if (this.loginRes.IsValid === 'true' && this.loginRes.UserInRole === "SELLERS") {
					this.errmsg = "";
					this.role.setRole("SELLERS");
					this.role.setUser(this.loginRes);
					this._ngZone.run(() => this.router.navigate(['dashboard']));
				}
				else if (this.loginRes.IsValid === 'true' && this.loginRes.UserInRole === "contact") {
					this.errmsg = "";
					this.role.setRole("contact");
					this.role.setUser(this.loginRes);
					this._ngZone.run(() => this.router.navigate(['dashboard']));
				}
				else if (this.loginRes.IsValid === 'true' && this.loginRes.UserInRole === "BUYERS") {
					this.errmsg = "";
					this.role.setRole("BUYERS");
					this.role.setUser(this.loginRes);
					this._ngZone.run(() => this.router.navigate(['dashboard']));
				} else if (this.loginRes.IsValid === 'false') {
					this.errmsg = "";
				}

				else {
					this.errmsg = this.loginRes.errorMessage + ". Error code is-" + this.loginRes.errorCode;
					if (this.loginRes.errorCode == "AGLB-001") {
						this.errmsg = "Incorrect username password"
					}
				}
			},
			(error) => {

				alert("an error occured");
				console.log(error);

				this.spinnerService.hide();
			});
		this.spinnerService.show();

	}

	gotoSignup() {
		this._ngZone.run(() => this.router.navigate(['signup']));

	}

	gotoFP() {
		this._ngZone.run(() => this.router.navigate(['forgetpswrd']));
	}
}
