import { Component, ViewChild, OnInit,ChangeDetectorRef } from "@angular/core";
import {GridOptions} from "ag-grid/main";

@Component({
  selector: 'app-profitability-matrix-yield',
  templateUrl: './profitability-matrix-yield.component.html',
  styleUrls: ['./profitability-matrix-yield.component.scss']
})
export class ProfitabilityMatrixYieldComponent implements OnInit {

 
  gridOptions: GridOptions;
     gridApi;
   gridColumnApi;

   columnDefs;

   rowData;
   defaultColDef;
   getRowNodeId;
 
     gridApi1;
   gridColumnApi1;

   rowData1;
   defaultColDef1;
   getRowNodeId1;
  constructor(private cdRef:ChangeDetectorRef) {
  
    this.gridOptions = <GridOptions>{
      onGridReady: () => {
          this.gridOptions.api.sizeColumnsToFit();
          
      }
  };

      this.columnDefs = [   {     
        children: [
          {headerName: 'Commodity', field: 'commodity', width: 150, suppressSizeToFit: true, enableRowGroup: true, rowGroupIndex: 0},
          {headerName: 'Price', field: 'price', width: 90, minwidth: 75, maxWidth: 100, enableRowGroup: true},
          {headerName: 'Yield', field: 'yield', width: 120, enableRowGroup: true},
          {headerName: 'Area(ha)', field: 'area', width: 90, enableRowGroup: true, pivotIndex: 0},
          {headerName: 'Units', field: 'units', width: 110, enableRowGroup: true},
          {
              headerName: 'Growing Break Even Per Unit',
              field: 'grbrkevenperunit',
              width: 60,
              enableValue: true,
              suppressMenu: true,
              // suppressSizeToFit: true,
              filter: 'agNumberColumnFilter',
              // aggFunc: 'sum'
          },
          {
              headerName: 'Profit/Unit',
              field: 'profitunit',
              width: 60,
              enableValue: true,
              suppressMenu: true,
              filter: 'agNumberColumnFilter',
              // aggFunc: 'sum'
          },
          {
              headerName: 'Profit/ha',
              field: 'profitha',
              width: 60,
              enableValue: true,
              suppressMenu: true,
              filter: 'agNumberColumnFilter',
              // aggFunc: 'sum'
          },
          {
              headerName: 'ROI',
              field: 'roi',
              width: 60,
              enableValue: true,
              suppressMenu: true,
              filter: 'agNumberColumnFilter',
              
              // aggFunc: 'sum'
          }
      ]    }   
    ];

    this.defaultColDef = { editable: false };
    this.getRowNodeId = function(data) {
      return data.id;
    };
  };

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }
  
    selectAllRows() {
        this.gridOptions.api.selectAll();
    }

  ngOnInit() {
  }
}
