import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfitabilityMatrixYieldComponent } from './profitability-matrix-yield.component';

describe('ProfitabilityMatrixYieldComponent', () => {
  let component: ProfitabilityMatrixYieldComponent;
  let fixture: ComponentFixture<ProfitabilityMatrixYieldComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfitabilityMatrixYieldComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfitabilityMatrixYieldComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
