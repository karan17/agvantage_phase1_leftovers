import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfitabilityMatrixGrowingProfitComponent } from './profitability-matrix-growing-profit.component';

describe('ProfitabilityMatrixGrowingProfitComponent', () => {
  let component: ProfitabilityMatrixGrowingProfitComponent;
  let fixture: ComponentFixture<ProfitabilityMatrixGrowingProfitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfitabilityMatrixGrowingProfitComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfitabilityMatrixGrowingProfitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
