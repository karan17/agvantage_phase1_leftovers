import { Component, ViewChild, OnInit,ChangeDetectorRef } from "@angular/core";
import {GridOptions} from "ag-grid/main";

@Component({
  selector: 'app-profitability-matrix-growing-profit',
  templateUrl: './profitability-matrix-growing-profit.component.html',
  styleUrls: ['./profitability-matrix-growing-profit.component.scss']
})
export class ProfitabilityMatrixGrowingProfitComponent implements OnInit {

  gridOptions: GridOptions;
     gridApi;
   gridColumnApi;

   columnDefs;

   rowData;
   defaultColDef;
   getRowNodeId;
 
     gridApi1;
   gridColumnApi1;

   rowData1;
   defaultColDef1;
   getRowNodeId1;
  constructor(private cdRef:ChangeDetectorRef) {
  
    this.gridOptions = <GridOptions>{
      onGridReady: () => {
          this.gridOptions.api.sizeColumnsToFit();
          
      }
  };

      this.columnDefs = [        
        {headerName: "-25.00%", field: "sample",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },
        {headerName: "-22.50%", field: "wheat",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },
        {headerName: "-20.00%", field: "durum",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },
        {headerName: "-17.50%", field: "feed",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },
        {headerName: "-15.00%", field: "malt",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },
        {headerName: "-12.50%", field: "canola",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },
        {headerName: "-10.00%", field: "chickpeas",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },
        {headerName: "-7.50%", field: "faba",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },
        {headerName: "-5.00%", field: "lupin",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },
        {headerName: "-2.50%", field: "cotton",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },
        {headerName: "Yeild", field: "cottonseed",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },
        {headerName: "2.50% ", field: "sorghum",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },
        {headerName: "5.00% ", field: "mungbeans",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },        
        {headerName: "7.50%", field: "faba",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },
        {headerName: "10.00%", field: "chickpeas",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },
        {headerName: "12.50%", field: "canola",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },
        {headerName: "15.00%", field: "malt",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },
        {headerName: "17.50%", field: "feed",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },
        {headerName: "20.00%", field: "durum",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },
        {headerName: "22.50%", field: "wheat",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        },
        {headerName: "25.00%", field: "sample",
        children:[{headerName: "2.35", field: "attrs",width:300}]
        }                
    ];

    this.defaultColDef = { editable: false };
    this.getRowNodeId = function(data) {
      return data.id;
    };
  };

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }
  
    selectAllRows() {
        this.gridOptions.api.selectAll();
    }

  ngOnInit() {
  }


}
