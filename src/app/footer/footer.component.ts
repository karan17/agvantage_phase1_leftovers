import { Component, OnInit, NgZone} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  constructor(private router: Router, private _ngZone: NgZone) { }

  ngOnInit() {
  }

  gotoAU() {
    this._ngZone.run(() => this.router.navigate(['AboutusComponent']));
  }

  gotoTeam() {
    this._ngZone.run(() => this.router.navigate(['TeamComponent']));
  }

  gotoProducts() {
    this._ngZone.run(() => this.router.navigate(['ProductsComponent']));
  }
  gotoOS() {
    this._ngZone.run(() => this.router.navigate(['OurServicesComponent']));
  }
  gotoCU() {
    this._ngZone.run(() => this.router.navigate(['ContactUsComponent']));
  }
  gotoFaq() {
    this._ngZone.run(() => this.router.navigate(['FaqComponent']));
  }
  gotoProd() {
    this._ngZone.run(() => this.router.navigate(['ProducerComponent']));
  }
}
