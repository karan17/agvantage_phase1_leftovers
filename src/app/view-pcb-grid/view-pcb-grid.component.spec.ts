import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPcbGridComponent } from './view-pcb-grid.component';

describe('ViewPcbGridComponent', () => {
  let component: ViewPcbGridComponent;
  let fixture: ComponentFixture<ViewPcbGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPcbGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPcbGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
