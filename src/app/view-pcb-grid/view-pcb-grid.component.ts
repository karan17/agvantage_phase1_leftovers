import { Component, OnInit, ChangeDetectorRef, AfterViewInit, AfterViewChecked, ViewChild } from '@angular/core';
import {Router} from '@angular/router';
import {Http, Headers, RequestOptions} from '@angular/http';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { UserRoleService } from '../user-role.service';
import {Broadcaster} from '../broadcaster';
import {serviceUrl} from "../app.module";
declare var $: any;

@Component({
  selector: 'app-view-pcb-grid',
  templateUrl: './view-pcb-grid.component.html',
  styleUrls: ['./view-pcb-grid.component.scss']
})

export class ViewPcbGridComponent implements OnInit, AfterViewInit, AfterViewChecked  {

    farmDetails: any;
    fieldDetails: any;
    userDetails: UserRoleService;
    disableAfterClick : boolean = false;
    url: string;
    headers: Headers;
    options: RequestOptions;
    commodities: any;
    AddProductionCosts:any ={};
    grades: any;
    selectedCommodity1: any;
    commodity: any;
    submitCommodity: string;
    grade: any;
    misc: any;
    roi: any;
    currentUser;
    farm:any={};
    newfarm:boolean=false;
    @ViewChild('addBrkevenfrcstForm') public addBrkevenfrcstForm;
    @ViewChild('addffForm') public addffForm;
    
    constructor(private cdRef: ChangeDetectorRef, private broadCaster: Broadcaster, private role: UserRoleService, private router: Router, private http: Http, public spinnerService: Ng4LoadingSpinnerService) {
        this.commodity = {};
        this.userDetails = this.role.getUser();
    }

    ngOnInit() {
        this.currentUser = this.role.getUser();
        this.getCommodities();
        this.getCropyearList();
        this.getFarmDetails();
        this.getFieldDetails(); 
        console.log('this.currentUser', this.currentUser);
    }
    ngAfterViewInit() {
        this.cdRef.detectChanges();

    }
    ngAfterViewChecked() {
        $('.selectpicker').selectpicker('refresh');
    }
    gotoHome() {
        this.router.navigate(['dashboard']);
    }


    openaddpcModal() {
        this.disableAfterClick = false;
        this.myModal.show();
    }

    closeBrkEvnModal() {
        this.myModal.hide();
    }

    openaddffModal() {
        this.addff.show();
    }

    closeaddffModal() {
        this.addff.hide();
    }

    getCommodities() {
        this.url = serviceUrl+"AgVantageGetCommoditiesSB/getCommodities/api/v1/getCommodity?getCommodities=true";
        this.http.get(this.url).subscribe((data) => {
            this.commodities = data.json().CommodityCollection.Commodity;
            //this.selectedCommodity1=this.commodities[0].C_NAME;
            console.log('response : ', this.commodities);
        })

    }
   

    callGrades() {
        this.getCommodityGrade(this.commodity.SCC_C_ID);
    }

    getFarmDetails(){
        this.url = serviceUrl+"/AgvantageGetFarmDetailsSB/AgvnatgeGetFarmDetailsRestPS/api/v1/GetFarmDetails?user_id="+this.currentUser.USER.U_ID;
        this.spinnerService.show();
        this.http.get(this.url).subscribe((data)=>{
        this.farmDetails=data.json()?data.json().FarmDetails:[];
        console.log(this.farmDetails);
        this.spinnerService.hide();
        },
        (err)=>{
            this.spinnerService.hide();
            this.farmDetails=[];
        });
    }

    getFieldDetails() {
        this.url = serviceUrl+"/AgvantageGetFieldDetailsSB/AgvantageGetFieldDetailsRestPS/api/v1/GetFieldDetails?user_id="+this.currentUser.USER.U_ID;
        this.http.get(this.url).subscribe((data)=>{
        this.fieldDetails=data.json()?data.json().FieldDetails:[];
        console.log(this.farmDetails);
        },
        (err)=>{
            this.fieldDetails=[];
        });
    }
    getFieldDetailsByFarmId(){
        let farmId;
        this.farmDetails.forEach(element => {
            if(element.farm_name === this.AddProductionCosts.farm_name){
                farmId = element.farm_id;
            }
        });
        this.url = serviceUrl+"/AgvantageGetFieldDetailsSB/AgvantageGetFieldDetailsRestPS/api/v1/GetFieldDetails?farm_id="+farmId;
        this.http.get(this.url).subscribe((data)=>{
        this.fieldDetails=data.json()?data.json().FieldDetails:[];
        },
        (err)=>{
            this.fieldDetails=[];
        });
    }

    getCommodityGrade(id) {
        // this.spinnerService.show();
        this.url = serviceUrl+"AgVantageGetCommodityGrade/AgVantageGetGradePS/api/v1/getGrade?C_ID=" + id;
        this.http.get(this.url).subscribe((data) => {
            // this.spinnerService.hide();
            this.grades = data.json().gradeCollection.grade;
        })
    }

    cropYearList;
    getCropyearList() {
        let url = serviceUrl+'AgvGetCropYear/GetCropYear/api/v1/getCropYear?getcropyer=true';
        this.http.get(url).subscribe((data) => {
            // console.log('crp year data:',data.json().CropYear);
            this.cropYearList = data.json().CropYear;
            // this.selectedCommodity1=this.commodities[0].C_NAME;
            // console.log(this.commodities);
        })
    }

    submitAddffForcast
    @ViewChild('addff') public addff;
    addFarmForm() {
        this.disableAfterClick = true;
        this.spinnerService.show();
        this.headers = new Headers({
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept': 'application/json'
        });
        this.options = new RequestOptions({ headers: this.headers });
        console.log('break even forecast', this.commodity);
        let params ={};
       
        if(this.farm.farm_id==undefined){
             params = {
                FarmDetails : {
                    user_id : this.currentUser.USER.U_ID,
                    farm_name : this.farm.farm_name,
                    farm_area : this.farm.farm_area,
                    FieldDetails : this.FieldDetails
    
                }
               
            };
        }else{
             params = {
                FarmDetails : {
                    user_id : this.currentUser.USER.U_ID,
                    farm_id : this.farm.farm_id,
                    FieldDetails : this.FieldDetails
    
                }
                
            };
        }
        console.log('Params', params);

        // this.commodity.SCC_U_ID=1;
        this.url = serviceUrl+"AgvantageAddFarmDetailsSB/AgvantageAddFarmDetailsRestPS/api/v1/AddFarmDetails"

        this.http.post(this.url, params, this.options).subscribe((data) => {
            console.log('resp', data.json());
            this.submitAddffForcast = data.json().Outcome;
            this.broadCaster.broadcast('Save Farm Event', 'Farm Saved Successfully.');
            this.addff.hide();
        })
    }


    submitBrkevenForcast
    @ViewChild('myModal') public myModal;
    addBrakevenForcast() {

        this.spinnerService.show();
        this.headers = new Headers({
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept': 'application/json'
        });
        this.options = new RequestOptions({ headers: this.headers });
       
        this.AddProductionCosts.user_id = this.currentUser.USER.U_ID;
        let params =this.AddProductionCosts;
        console.log('Params', params);

        // this.commodity.SCC_U_ID=1;
        this.url = serviceUrl+"AgvantageAddProductionCostsSB/AgvantageAddProductionCostsRestPS/api/v1/AddProductionCosts"

        this.http.post(this.url, params, this.options).subscribe((data) => {
            console.log('resp', data.json());
            this.submitBrkevenForcast = data.json().Outcome;
            this.broadCaster.broadcast('Save PCB Event', 'Saved Production Cost Successfully.');
            this.myModal.hide();
        })
    }

    FieldDetails = [];
    costsArray = [];

    createNewCustomFields() {
        let field = { field_name: '', field_area: '' };
        let newfield = Object.assign({}, field);
        // obj.disable = true;
        if (this.FieldDetails.length < 11) {
            newfield.field_name = '';
            newfield.field_area = '';
            this.FieldDetails.push(newfield);
            console.log('create new  fields...', this.FieldDetails);
        }
    }
    createCostsArray(){
        let field = { cost_name: '', cost_value: '' };
        let newfield = Object.assign({}, field);
        // obj.disable = true;
        if (this.FieldDetails.length < 11) {
            newfield.cost_name = '';
            newfield.cost_value = '';
            this.costsArray.push(newfield);
            console.log('create new  fields...', this.costsArray);
        }
        this.AddProductionCosts.Costs =  this.costsArray;

    }

    removeNewCustomFields(obj, index) {
        console.log('obj', obj, 'i:', index);
        this.FieldDetails.splice(index, 1);
    }

    

}
