import { Component, OnInit, ChangeDetectorRef, AfterViewInit, AfterViewChecked, ViewChild } from '@angular/core';
import {Router} from '@angular/router';
import {Http, Headers, RequestOptions} from '@angular/http';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { UserRoleService } from '../user-role.service';
import {Broadcaster} from '../broadcaster';
import {serviceUrl} from "../app.module";
declare var $: any;

@Component({
    selector: 'app-break-even-forcast',
    templateUrl: './break-even-forcast.component.html',
    styleUrls: ['./break-even-forcast.component.scss']
})
export class BreakEvenForcastComponent implements OnInit, AfterViewInit, AfterViewChecked {

    url: string;
    headers: Headers;
    options: RequestOptions;
    commodities: any;
    grades: any;
    selectedCommodity1: any;
    commodity: any;
    submitCommodity: string;
    grade: any;
    misc: any;
    roi: any;
    currentUser;
    @ViewChild('addBrkevenfrcstForm') public addBrkevenfrcstForm;

    constructor(private cdRef: ChangeDetectorRef, private broadCaster: Broadcaster, private role: UserRoleService, private router: Router, private http: Http, public spinnerService: Ng4LoadingSpinnerService) {
        this.commodity = {};
    }

    ngOnInit() {
        this.getCommodities();
        this.getCropyearList();
        this.currentUser = this.role.getUser();
        console.log('this.currentUser', this.currentUser);
    }
    ngAfterViewInit() {
        this.cdRef.detectChanges();

    }
    ngAfterViewChecked() {
        $('.selectpicker').selectpicker('refresh');
    }
    gotoHome() {
        this.router.navigate(['dashboard']);
    }


    openBrkEvnModal() {
        this.myModal.show();
    }

    closeBrkEvnModal() {
        this.myModal.hide();
    }

    getCommodities() {
        this.url = serviceUrl+"AgVantageGetCommoditiesSB/getCommodities/api/v1/getCommodity?getCommodities=true";
        this.http.get(this.url).subscribe((data) => {
            this.commodities = data.json().CommodityCollection.Commodity;
            //this.selectedCommodity1=this.commodities[0].C_NAME;
            console.log('response : ', this.commodities);
        })
    }


    callGrades() {
        this.getCommodityGrade(this.commodity.SCC_C_ID);
    }
    getCommodityGrade(id) {
        // this.spinnerService.show();
        this.url = serviceUrl+"AgVantageGetCommodityGrade/AgVantageGetGradePS/api/v1/getGrade?C_ID=" + id;
        this.http.get(this.url).subscribe((data) => {
            // this.spinnerService.hide();
            this.grades = data.json().gradeCollection.grade;
        })
    }

    cropYearList;
    getCropyearList() {
        let url = serviceUrl+'AgvGetCropYear/GetCropYear/api/v1/getCropYear?getcropyer=true';
        this.http.get(url).subscribe((data) => {
            // console.log('crp year data:',data.json().CropYear);
            this.cropYearList = data.json().CropYear;
            // this.selectedCommodity1=this.commodities[0].C_NAME;
            // console.log(this.commodities);
        })
    }

    submitBrkevenForcast
    @ViewChild('myModal') public myModal;
    addBrakevenForcast() {

        this.spinnerService.show();
        this.headers = new Headers({
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept': 'application/json'
        });
        this.options = new RequestOptions({ headers: this.headers });
        console.log('break even forecast', this.commodity);
        this.commodity["U_ID"] = this.currentUser.USER.U_ID;
        this.commodity["CustomFields"] = { otherCost: this.customFieldArray }

        let params = {
            BreakEvenForecast: this.commodity,
            // CustomFields : {otherCost : this.customFieldArray}
        };
        console.log('Params', params);

        // this.commodity.SCC_U_ID=1;
        this.url = serviceUrl+"AgvAddBreakEvenForCast/AgvAddBreakEvenForeCast/api/v1/addForeCast"

        this.http.post(this.url, params, this.options).subscribe((data) => {
            console.log('resp', data.json());
            this.submitBrkevenForcast = data.json().Outcome;
            this.addBrkevenfrcstForm.reset();
            this.broadCaster.broadcast('Save Event', 'Saved Successfully.');
            this.spinnerService.hide();
            $(".modal-content").scrollTop(0);
            this.myModal.hide();
        })
    }

    customFieldArray = [];

    createNewCustomFields() {
        let obj = { label: '', value: '' };
        let newObj = Object.assign({}, obj);
        // obj.disable = true;
        if (this.customFieldArray.length < 11) {
            newObj.label = '';
            newObj.value = '';
            this.customFieldArray.push(newObj);
            console.log('create new quality fields...', this.customFieldArray);
        }
    }

    removeNewCustomFields(obj, index) {
        console.log('obj', obj, 'i:', index);
        this.customFieldArray.splice(index, 1);
    }


}
