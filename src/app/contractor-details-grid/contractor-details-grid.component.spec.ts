import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorDetailsGridComponent } from './contractor-details-grid.component';

describe('ContractorDetailsGridComponent', () => {
  let component: ContractorDetailsGridComponent;
  let fixture: ComponentFixture<ContractorDetailsGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorDetailsGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorDetailsGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
