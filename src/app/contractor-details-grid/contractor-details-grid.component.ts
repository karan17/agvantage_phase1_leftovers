import { Component, OnInit } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import { UserRoleService } from '../user-role.service';
import { Http, Headers, RequestOptions } from '@angular/http';
import { EditDeleteRendererComponent } from '../edit-delete-renderer/edit-delete-renderer.component';
import { serviceUrl } from '../app.module';
import {Broadcaster} from '../broadcaster';

@Component({
  selector: 'app-contractor-details-grid',
  templateUrl: './contractor-details-grid.component.html',
  styleUrls: ['./contractor-details-grid.component.scss']
})
export class ContractorDetailsGridComponent implements OnInit {

  frameworkComponents: any;
  context: any;
  components: any;
  currentUser: any;
  gridOptions: GridOptions;
  gridApi;
  gridColumnApi;

  columnDefs;

  rowData;
  defaultColDef;
  getRowNodeId;
  saveMessage: any = false;

  constructor(private user: UserRoleService, private http: Http, private broadCaster: Broadcaster) {
    this.gridOptions = <GridOptions>{
      onGridReady: () => {
        // this.gridOptions.columnApi.autoSizeAllColumns();
        this.gridOptions.api.sizeColumnsToFit();
      }

    };
    this.currentUser = user.getUser();
    this.frameworkComponents = {
      'editdeletecellrenderer': EditDeleteRendererComponent
    }
    this.context = { componentParent: this };
    this.columnDefs = [
      { headerName: "Date Commencement", field: "DateCommenced", pinned: 'left' },
      { headerName: "Contractor Company Name", field: "CompanyName", pinned: 'left' },
      { headerName: "Key Contact", field: "KeyContact", pinned: 'left' },      
      { headerName: "Operator Phone", field: "Phone", pinned: 'left' },
      { headerName: "Operator UHF", field: "UHF", pinned: 'left' },
      { headerName: "Job/Tasks", field: "Job", pinned: 'left' },
      { headerName: "Fields/Location of Job/Task", field: "FieldLocation", pinned: 'left' },
      { headerName: "Hectares Completed", field: "HectaresCompleted", pinned: 'left' },
      { headerName: "Machinery Regn No", field: "MachineryDetails.MachineryRegNo", pinned: 'left' },
      { headerName: "Date Completed", field: "DateCompleted", pinned: 'left' }
    ];
    this.rowData = [];

    // this.context = { componentParent: this };
    // this.frameworkComponents = {
    //   commodityUpdateCellRendererComponent: CommodityUpdateCellRendererComponent
    // };


    this.getRowNodeId = function (data) {
      return data.id;
    };
    this.broadCaster.on<any>('Save Event ContractDetail')
    .subscribe(res => {
        console.log(res);
        this.gridOptions.api.showLoadingOverlay()
         this.getAllContractorDetails();
       
        this.saveMessage = res.msg;
        setTimeout(()=>{
          this.saveMessage = false;
        },3000);
    });
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.getAllContractorDetails();
  }
  getAllContractorDetails() {
    let url = serviceUrl+"ContractorOnFarmSB/ContractorsOnFarmPS/api/v1/GetAllContractors?U_ID=" + this.currentUser.USER.U_ID;
    this.gridApi.showLoadingOverlay();
    this.http.get(url).subscribe((data) => {
      console.log(data.json());
      this.rowData = data.json().Contractors ? data.json().Contractors.Contractor : [];
      this.gridApi.hideOverlay();
    })
  }

  ngOnInit() {

  }



  // this.broadCaster.on<string>('Save Event')
  //   .subscribe(message => {
  //     this.gridOptions.api.showLoadingOverlay()
  //     this.getGridData();
  //     this.saveMessage=message;
  //   });
  //      this.broadCaster.on<string>('Update Event')
  //   .subscribe(message => {
  //     this.gridOptions.api.showLoadingOverlay()
  //     this.getGridData();
  //     this.saveMessage=message;
  //   });

}
