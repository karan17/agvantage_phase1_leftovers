import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddQualityGridComponent } from './add-quality-grid.component';

describe('AddQualityGridComponent', () => {
  let component: AddQualityGridComponent;
  let fixture: ComponentFixture<AddQualityGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddQualityGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddQualityGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
