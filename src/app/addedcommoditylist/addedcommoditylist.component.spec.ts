import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddedcommoditylistComponent } from './addedcommoditylist.component';

describe('AddedcommoditylistComponent', () => {
  let component: AddedcommoditylistComponent;
  let fixture: ComponentFixture<AddedcommoditylistComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddedcommoditylistComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddedcommoditylistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
