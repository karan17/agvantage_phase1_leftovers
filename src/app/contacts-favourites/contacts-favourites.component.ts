import { Component, OnInit, ViewChild, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { UserRoleService } from '../user-role.service';
import { serviceUrl } from "../app.module";
import { Http, Headers, RequestOptions } from '@angular/http';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {Broadcaster} from '../broadcaster';


@Component({
  selector: 'app-contacts-favourites',
  templateUrl: './contacts-favourites.component.html',
  styleUrls: ['./contacts-favourites.component.scss']
})
export class ContactsFavouritesComponent implements OnInit {

  marketZones: any;
  headers: Headers;
  options: RequestOptions;
  storageTypes: string[];
  portZones: any;
  farmingRegions: any;
  states: any;
  @ViewChild('myModal') public myModal;
  @ViewChild('delModal') public delModal;
  favourites;
  U_A_COUNTRY = "Australia";
  U_A_STATE = "";
  commodities: any;
  currentUser;
  url: string;
  grades;
  emailNotify: any;
  smsNotify: any;
  EntityName; NGR;
  editFav: boolean = false;
  deliveredDestinations: any;
  updateEnable: boolean = false;
  addEnable: boolean = false;
  constructor(private _ngZone: NgZone, private router: Router, private role: UserRoleService,
    private http: Http, public spinnerService: Ng4LoadingSpinnerService, private broadCaster: Broadcaster) {
    this.getWarehouseTypes();
    this.currentUser = this.role.getUser();
    this.favourites = {};
    this.EntityName = this.currentUser.USER.U_TRADING_NAME;
    this.NGR = this.currentUser.USER.U_NGR;
  }

  ngOnInit() {
    this.getCommodities();
    this.getStates();
    this.getCropyearList();
    this.currentUser = this.role.getUser();
    console.log('this.currentUser', this.currentUser);
    this.storageTypes = ["On-Farm", "PublicWarehouse"];
  }

  gotoHome() {
    this._ngZone.run(() => this.router.navigate(['dashboard']));
  }

  openAddFavourites() {
    this.editFav = false;
    this.addEnable = false;
    this.updateEnable  = false;
    this.favourites = {};
    this.myModal.show();
  }

  closeAddFavourites() {
    this.myModal.hide();
  }

  WarehouseTypes: any;
  getWarehouseTypes() {
    console.log('this.role.getRole(): ', this.role.getRole());
    // if (this.role.getRole() === "superadmin") {
    this.WarehouseTypes = [
      { T_ID: '1', T_NAME: 'On Farm', T_NM: 'On-farm' },
      { T_ID: '2', T_NAME: 'Public Warehouse', T_NM: 'PublicWarehouse' },
      { T_ID: '3', T_NAME: 'Delivered Destination', T_NM: 'DeliveredDestination' }
    ]
    // } 
    // else if (this.role.getRole() === "P") {
    //   this.WarehouseTypes = [
    //     { T_ID: '1', T_NAME: 'On Farm' },
    //     { T_ID: '2', T_NAME: 'Public' }
    //   ]
    // }
  }

  siteList: any;
  callSelectedWHType() {
    let user_id = this.currentUser.USER.U_ID;
    if (this.favourites.storagetype == 'On-farm') {
      this.spinnerService.show();
      let url = serviceUrl +"AgvantageGetSitesSB/AgvantageGetSitesRestPS/api/v1/GetSitesList?user_id=" + user_id + "&storage_type=On-farm";
      this.http.get(url).subscribe((data) => {
        this.spinnerService.hide();
        this.siteList = data.json() ? data.json().getSites : [];
      },
        (err) => {
          this.siteList = [];
        });
    } else if (this.favourites.storagetype == 'PublicWarehouse') {
      this.spinnerService.show();
      let url = serviceUrl+ "AgvantageGetSitesSB/AgvantageGetSitesRestPS/api/v1/GetSitesList?fr_id=" + user_id + "&storage_type=PublicWarehouse"
      this.http.get(url).subscribe((data) => {
        this.spinnerService.hide();
        this.siteList = data.json() ? data.json().getSites : [];
      },
        (err) => {
          this.siteList = [];
        });
    }
    else {
      this.spinnerService.show();
      let url = serviceUrl +"AgvantageGetSitesSB/AgvantageGetSitesRestPS/api/v1/GetSitesList?state_id=" + this.favourites.state + "&pz_id=" + this.favourites.portzone + "&storage_type=DelieveredDestination";
      this.http.get(url).subscribe((data) => {
        this.spinnerService.hide();
        this.siteList = data.json() ? data.json().getSites : [];
        //this.marketZones =  this.siteList.map((mzone)=>{ return mzone[0].MZ_Name })
      },
        (err) => {
          this.siteList = [];
        });
    }
    
  }

  getCommodities() {
    this.spinnerService.show();
    this.url = serviceUrl + "AgVantageGetCommoditiesSB/getCommodities/api/v1/getCommodity?getCommodities=true";
    this.http.get(this.url).subscribe((data) => {
      this.spinnerService.hide();
      this.commodities = data.json().CommodityCollection.Commodity;
    })
  }

  getStates() {
    this.spinnerService.show();
    this.url = serviceUrl + "AgvantageGetStatesSB/AgvantageGetStatesRestPS/api/v1/getStates?country_id=1"
    this.http.get(this.url).subscribe((data) => {
      this.spinnerService.hide();
      this.states = data.json()?data.json().getStates:[];
    })
  }

  getFarmingRegions() {
    this.spinnerService.show();
    this.url = serviceUrl + "AgvantageGetFarmingRegionsSB/AgvantageGetFarmingRegionsRestPS/api/v1/GetFarmingRegions?state_id=" + this.favourites.state + "&pz_id=" + this.favourites.portzone;
    this.http.get(this.url).subscribe((data) => {
      this.spinnerService.hide();
      this.farmingRegions = data.json()?data.json().getFarmingRegions:[];

    })
  }

  getPortZones() {
    this.spinnerService.show();
    this.url = serviceUrl + "AgvantageGetPortZonesSB/AgvantageGetPortZoneRestPS/api/v1/getPortZones?state_id=" + this.favourites.state;
    this.http.get(this.url).subscribe((data) => {
      this.spinnerService.hide();
      this.portZones = data.json()?data.json().getPortZones:[];
    })
  }

  callGrades() {
    this.getCommodityGrade(this.favourites.commodity);
  }

  getCommodityGrade(id) {
    this.spinnerService.show();
    this.url = serviceUrl + "AgVantageGetCommodityGrade/AgVantageGetGradePS/api/v1/getGrade?C_ID=" + id;
    this.http.get(this.url).subscribe((data) => {
      this.spinnerService.hide();
      this.grades = data.json().gradeCollection?data.json().gradeCollection.grade:[];
    })
  }

  cropYearList;
  getCropyearList() {
    this.spinnerService.show();
    let url = serviceUrl + 'AgvGetCropYear/GetCropYear/api/v1/getCropYear?getcropyer=true';
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.cropYearList = data.json().CropYear;
    })
  }
  openDeleteDialog(evnt){
    this.favourites.Favourite_id = evnt.Favourite_id;

    //open delete dialog
    this.delModal.show();
  }
  closeDeleteDialog(evnt){
    //close delete dialog
    this.delModal.hide();
  }
  //Edit favorite flow
  reUseAddFav(evnt) {
    console.log("from child to parent data:", evnt);
    this.editFav = true;
    this.updateEnable  = false;
    this.getWarehouseTypes();
    
    this.favourites = {
      state: evnt.State_id,
      portzone: evnt.Port_Zone_id,
      commodity: evnt.Commodity_id,
      farmingregion: evnt.Farming_Region_ID,
      storagetype: evnt.Storage_Type,
      site : evnt.Site_id,
      crop: evnt.Crop_year,
      grade: evnt.Grade,
      targetroi: evnt.Target_ROI,
      alert: evnt.Alert_price,
      emailnotify: evnt.Alert_Email,
      smsnotify: evnt.Alert_SMS
    };
    this.favourites.Favourite_id = evnt.Favourite_id;
    this.getPortZones();
    this.callSelectedWHType();
    this.getFarmingRegions();
    this.callGrades();
    
    if(this.favourites.storagetype == 'DeliveredDestination'){
     this.favourites.MZ = evnt.Market_Zone.MZ_Name;
    setTimeout(()=>{
      this.setDD();
    },1000);
     this.favourites.DDS = evnt.Market_Zone.DeliveredDestinations.DD_ID;
    }
    
    
    
    this.myModal.show();
    console.log("market zone is-----------",this.favourites.MZ);
  }
  setDD(){
    this.siteList.forEach(element => {
      if(this.favourites.MZ == element.Market_Zone[0].MZ_Name){
        this.deliveredDestinations =  element.Market_Zone[0].DeliveredDestinations;
        return;
      }
    });;
  }
  deleteFavourites() {
   

   
    this.headers = new Headers({
      'Content-Type': 'application/json; charset=UTF-8',
      'Accept': 'application/json'
    });
    this.options = new RequestOptions({ headers: this.headers });
    


    let url = serviceUrl + "AgvantageEntityAddFavouritesSB/AgvantageEntityManageFavouritesRestPS/api/V1/ManageFavourites";

    this.http.post(url,{Favourite_id : this.favourites.Favourite_id}, this.options).subscribe(
      (data) => {
        this.spinnerService.hide();
        console.log("del fav res: ", data);
        this.delModal.hide();
        this.broadCaster.broadcast('Delete Favorite Event', "Deleted Succesfully");
        // if(this.editFav){
        //   this.broadCaster.broadcast('Update Favorite Event', "Updated Succesfully");
        // }else{
        //   this.broadCaster.broadcast('Save Favorite Event', "Saved Succesfully");
        // }

      },
      (err) => {
        console.log("del contractor err: ", err);
      }
    );
   }
   

  addFavourites() {
    this.updateEnable = true;
    this.addEnable = true;
    this.favourites.u_id = this.currentUser.USER.U_ID;

    this.spinnerService.show();
    this.headers = new Headers({
      'Content-Type': 'application/json; charset=UTF-8',
      'Accept': 'application/json'
    });
    this.options = new RequestOptions({ headers: this.headers });
    let addFavouritesRequest = {
      'addFavouritesRequest': this.editFav ?
        {
          "Favourite_id": this.favourites.Favourite_id,
          "User_id": this.currentUser.USER.U_ID,
          "Commodity_id": this.favourites.commodity,
          "Country": 1,//this.favourites.country,
          "State": this.favourites.state,
          "Port_Zone": this.favourites.portzone,
          "Farming_Region": this.favourites.farmingregion,
          "Storage_Type": this.favourites.storagetype,
          "Site_id": (this.favourites.site && this.favourites.MZ==undefined) ? this.favourites.site : this.favourites.DDS,
          "Crop_year": this.favourites.crop,
          "Grade": this.favourites.grade,
          "Target_ROI": this.favourites.targetroi,
          "Alert_price": this.favourites.alert,
          "Alert_Email": this.favourites.emailnotify,
          "Alert_SMS": this.favourites.smsnotify
        }
        :
        {
          "User_id": this.currentUser.USER.U_ID,
          "Commodity_id": this.favourites.commodity,
          "Country": 1,//this.favourites.country,
          "State": this.favourites.state,
          "Port_Zone": this.favourites.portzone,
          "Farming_Region": this.favourites.farmingregion,
          "Storage_Type": this.favourites.storagetype,
          "Site_id": (this.favourites.site && this.favourites.MZ==undefined) ? this.favourites.site : this.favourites.DDS,
          "Crop_year": this.favourites.crop,
          "Grade": this.favourites.grade,
          "Target_ROI": this.favourites.targetroi,
          "Alert_price": this.favourites.alert,
          "Alert_Email": this.favourites.emailnotify,
          "Alert_SMS": this.favourites.smsnotify
        }
    }



    let url = serviceUrl + "AgvantageEntityAddFavouritesSB/AgvantageEntityManageFavouritesRestPS/api/V1/ManageFavourites";
   
    this.http.post(url, addFavouritesRequest.addFavouritesRequest, this.options).subscribe(
      (data) => {
        this.spinnerService.hide();
        console.log("Add fav res: ", data);
        this.myModal.hide();
        if(this.editFav){
          this.broadCaster.broadcast('Update Favorite Event', "Updated Succesfully");
        }else{
          this.broadCaster.broadcast('Save Favorite Event', "Saved Succesfully");
        }

         
      },
      (err) => {
        console.log("Add contractor err: ", err);
      }
    );
  }

}
