import { Component, OnInit,ChangeDetectorRef,AfterViewInit,AfterViewChecked } from '@angular/core';
import  {Router} from '@angular/router';
import {Http, Headers, RequestOptions} from '@angular/http';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {serviceUrl} from "../app.module";
declare var $:any;
@Component({
  selector: 'app-addcommodity',
  templateUrl: './addcommodity.component.html',
  styleUrls: ['./addcommodity.component.scss']
})
export class AddcommodityComponent implements OnInit, AfterViewInit, AfterViewChecked {
    url: string;
    headers: Headers;
    options: RequestOptions;
    commodities:any;
    grades:any;
    selectedCommodity1:any;
    commodity:any;
    submitCommodity:string;
    grade:any;
  
  constructor(private cdRef:ChangeDetectorRef,private router:Router,private http:Http,public spinnerService: Ng4LoadingSpinnerService) {
    this.commodity = {};
  }

  ngOnInit() {
      this.getCommodities();
      this.getCropyearList();
  }
  ngAfterViewInit(){
      this.cdRef.detectChanges();
      
      }
    ngAfterViewChecked() {
    $('.selectpicker').selectpicker('refresh');
}
  gotoHome(){
    this.router.navigate(['dashboard']);    
    }
    getCommodities(){
        this.url=serviceUrl+"AgVantageGetCommoditiesSB/getCommodities/api/v1/getCommodity?getCommodities=true";
        this.http.get(this.url).subscribe((data)=>{
            this.commodities=data.json().CommodityCollection.Commodity; 
            //this.selectedCommodity1=this.commodities[0].C_NAME;
            console.log(data.json());  
        })
        
    }
    cropYearList;
    getCropyearList(){
        let url = serviceUrl+'AgvGetCropYear/GetCropYear/api/v1/getCropYear?getcropyer=true';
        this.http.get(url).subscribe((data)=>{
          // console.log('crp year data:',data.json().CropYear);
          this.cropYearList= data.json().CropYear;
          // this.selectedCommodity1=this.commodities[0].C_NAME;
          // console.log(this.commodities);
        })
      }
//    IDGenerator() {
//     
//         let length = 8;
//         let timestamp = +new Date;
//         
//         var _getRandomInt = function( min, max ) {
//            return Math.floor( Math.random() * ( max - min + 1 ) ) + min;
//         }
//         
//         let generate = function() {
//             var ts = timestamp.toString();
//             var parts = ts.split( "" ).reverse();
//             var id = "";
//             
//             for( var i = 0; i < length; ++i ) {
//                var index = _getRandomInt( 0, parts.length - 1 );
//                id += parts[index];  
//             }
//             
//             return id;
//         }
//        return generate();
//        }

    callGrades(){
        this.getCommodityGrade(this.commodity.SCC_C_ID);  
    }
    getCommodityGrade(id){
        this.url=serviceUrl+"AgVantageGetCommodityGrade/AgVantageGetGradePS/api/v1/getGrade?C_ID="+id;
        this.http.get(this.url).subscribe((data)=>{
            this.grades=data.json().gradeCollection.grade;
        })
    }
    addCommodity(){
        this.spinnerService.show();
        this.headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Accept':'application/json'
        });
        this.options = new RequestOptions({ headers: this.headers });
        console.log(this.commodity);
        this.commodity.SCC_U_ID=1;
        this.url=serviceUrl+"AgVCommodityRegistartion/AgvantageCommodityRegPS/api/v1/commRegister";
        
        this.http.post(this.url,$.param(this.commodity),this.options).subscribe((data)=>{
            this.submitCommodity=data.json().outcome;
            this.commodity={};
            this.grade="";
            this.spinnerService.hide();
            $(".modal-content").scrollTop(0);
        })
    }
}
