import { Component, OnInit, NgZone } from '@angular/core';
import {UserRoleService} from '../user-role.service';
import  {Router} from '@angular/router';
import {Http, Headers, RequestOptions} from '@angular/http';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {serviceUrl} from "../app.module";
declare var $: any;

@Component({
  selector: 'app-pages-profile',
  templateUrl: './pages-profile.component.html',
  styleUrls: ['./pages-profile.component.scss']
})
export class PagesProfileComponent implements OnInit {
currentUser:any;
     headers: Headers;
    options: RequestOptions;
    url:string;
    USER:any;
    showOutcome:string;
    outcomeMsg:boolean=false;
  constructor(private role:UserRoleService,private router:Router,private _ngZone: NgZone,private http: Http, public spinnerService: Ng4LoadingSpinnerService) { 
  this.currentUser = this.role.getUser();
      this.USER={};
  }

  ngOnInit() {
      this.getNotificationById();
  }
    emailNotify:any;
    smsNotify:any;
     getNotificationById(){
    let url=serviceUrl+"GetNotificationPreferenceById/GetNotificationPreferenceById/api/v1/getNotificationPreferenceById?user_id="+this.currentUser.USER.U_ID;    
    this.http.get(url).subscribe((succ)=>{
      let res=succ.json().preferences?succ.json().preferences:null;
        
       
        if(res.EMAIL=="true"){
          this.emailNotify=true;   
        }else{
          this.emailNotify=false;    
        }
          if(res.SMS=="true"){
         this.smsNotify=true;
        }else{
           this.smsNotify=false;
        }
       
    })
         }
    notifyMsg:any;
    setNoitifcation($event,status, notifyFor){
      this.notifyMsg=undefined;
       
        if(notifyFor=='email'){
          let url=serviceUrl+"SetNotificationPreferences/SetNotificationPreferencePS/api/v1/setNotificationPreferenceById";
          let obj = {
              "user_id": this.currentUser.USER.U_ID,
              "preference": "EMAIL",
              "preferenceStatus": status
          };
            this.http.post(url,obj).subscribe((success)=>{
              console.log(success.json()); 
                this.notifyMsg=success.json().response?success.json().response:"Some error occurred"; 
            },
            (err)=>{
               console.log(err.json());
                this.notifyMsg="Server related errors.We will fix it soon.";
                 
            }    
                );
                
        }
        if(notifyFor=='sms'){
          let url=serviceUrl+"SetNotificationPreferences/SetNotificationPreferencePS/api/v1/setNotificationPreferenceById";
              let obj = {
              "user_id": this.currentUser.USER.U_ID,
              "preference": "SMS",
              "preferenceStatus": status
          }; 
            this.http.post(url,obj).subscribe((success)=>{
              console.log(success.json());  
                this.notifyMsg=success.json().response?success.json().response:"Some error occurred";
            },
            (err)=>{
               console.log(err.json());
                 this.notifyMsg="Server related errors.We will fix it soon.";
                 
            }    
                );
                
        
        }
    }

  gotoHome(){
    this._ngZone.run(() => this.router.navigate(['dashboard']));
  }
    showMsg(){
       this.headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Accept':'application/json'
        });
        this.options = new RequestOptions({ headers: this.headers });
        this.url = serviceUrl+"AccountGatewayResetPassword/AccountGatewayPasswordResetPS/api/v1/resetPassword";
    this.spinnerService.show();
           this.http.post(this.url,$.param(this.USER),this.options).subscribe((data)=>{
               console.log(data.json());
               this.spinnerService.hide();
               this.showOutcome=data.json().outcome;
               this.outcomeMsg=true;
           
           })
    
    }

}
