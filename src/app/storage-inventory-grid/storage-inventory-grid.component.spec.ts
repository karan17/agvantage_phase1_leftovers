import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StorageInventoryGridComponent } from './storage-inventory-grid.component';

describe('StorageInventoryGridComponent', () => {
  let component: StorageInventoryGridComponent;
  let fixture: ComponentFixture<StorageInventoryGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StorageInventoryGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StorageInventoryGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
