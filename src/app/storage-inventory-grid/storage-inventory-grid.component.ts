
import {GridOptions} from "ag-grid/main";

import { CommodityUpdateCellRendererComponent } from '../commodity-update-cell-renderer/commodity-update-cell-renderer.component';
import { TooltipcellrendererComponent } from '../tooltipcellrenderer/tooltipcellrenderer.component';

import { Component, OnInit, OnDestroy, NgZone, ViewChild, ElementRef, Input, Output, EventEmitter, ViewContainerRef,
    ComponentFactoryResolver, ComponentFactory, ComponentRef } from '@angular/core';
import {Router} from '@angular/router';
import {Http, Headers, RequestOptions} from '@angular/http';
import { FormControl, FormsModule, ReactiveFormsModule } from "@angular/forms";
import { UserRoleService } from '../user-role.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {Broadcaster} from '../broadcaster';
import {serviceUrl} from "../app.module";
import "ag-grid-enterprise";



declare var google;
declare var $: any;

@Component({
    selector: 'app-storage-inventory-grid',
    templateUrl: './storage-inventory-grid.component.html',
    styleUrls: ['./storage-inventory-grid.component.scss']
})
export class StorageInventoryGridComponent {

    gridOptions: GridOptions;
    gridApi;
    gridColumnApi;
    gridOptions1: GridOptions;
    gridApi1;
    gridColumnApi1;
    bsVal;

    columnDefs;
    columnDefs1;
    rowData;
    rowData1;
    defaultColDef;
    getRowNodeId;
    storageGridData;
    context;
    frameworkComponents;
    inventoryObj;

    url: string;
    headers: Headers;
    option: RequestOptions;

    commodities: any;
    WarehouseTypes: any;
    PublicList: any;
    OnfarmList: any;
    ExfarmList: any;
    inventory: any;
    targetprice: any;
    quantity: any;
    quality: any;
    qualityList: any;
    qualityValue: any;
    grade: any;
    details: any;
    autocomplete;
    location: any;
    fieldFrom: any;
    date_harvested: any;
    fumigationORChemical: any;
    chemicalRatesApplied: any;
    dateApplied: any;
    loc;
    showExfarm: boolean = false;
    showOnfarm: boolean = false;
    showPublic: boolean = false;
    ihName;
    ihLocation;
    ofName;
    ofLocation;
    storageLocation;
    exfarm;
    onfarm;
    addStorage;
    storageName;
    StorageDistance;
    storageDirection
    storageLocationGPS;
    storageLocationDistance;
    storageLocationDirection;
    storageLocationNearestTown;
    storageLocationAddress;
    storageLocationCity;
    storageLocationState;
    storageLocationPostCode;
    storageLocationCountry;
    address; city; state; country; zipcode; neartown;
    public latitude: number;
    public longitude: number;
    public searchControl: FormControl;
    public zoom: number;
    map;
    service;
    infoWindow;
    sourceBox; destBox;
    cropYearList;
    loadList: any = [];
    storageid;

    @Output('pushInventory') pushInventory = new EventEmitter();
    @ViewChild("search")
    public searchElementRef: ElementRef;

    @ViewChild("sourceSearch")
    public sourceSearchElementRef: ElementRef;

    @ViewChild("destiSearch")
    public destiSearchElementRef: ElementRef;

    @ViewChild("mapDiv")
    public mapDiv: ElementRef;

    // public searchControl: FormControl;
 
    // @ViewChild('location')
    // public locationElementRef: ElementRef;

    @ViewChild('addInventoryForm') public addInventoryForm;
    @ViewChild('noBDModal') public myModal;

    public options = { types: ['address'], componentRestrictions: { country: 'AUS' } };
    bsValue1: Date = new Date();
    bsValue2: Date = new Date();
    // @ViewChild("alertContainer", { read: ViewContainerRef }) container;
    componentRef; inputSource; inputDest;
    srcsrch; destsrch;
    dist: any; duration: any;
    rowSelection: any;
    saveMessage;
    params:any;
    params1: any;
    autoGroupColumnDef : any;
    constructor(private router: Router, private role: UserRoleService, private _ngZone: NgZone,
        private http: Http, public spinnerService: Ng4LoadingSpinnerService, private broadCaster: Broadcaster,
        private resolver: ComponentFactoryResolver) {
        // we pass an empty gridOptions in, so we can grab the api out
        this.currentUser = this.role.getUser();
        this.inventoryObj = {};
        this.inventory = {};
        this.exfarm = {};
        this.onfarm = {};
        this.StorageData = {};
        this.addStorage = {};
        this.broadCaster.on<any>('Save Event')
            .subscribe(res => {
                console.log(res);
                this.gridOptions.api.showLoadingOverlay()
                 this.getGridData(res.id);
               
                this.saveMessage = res.msg;
            });
        this.broadCaster.on<any>('Save Storage Event')
            .subscribe(res => {
                console.log(res);
                this.gridOptions.api.showLoadingOverlay()
                 this.getGridData(undefined);     
                 this.saveMessage = res;
                   
            });
        this.broadCaster.on<any>('Update Event')
            .subscribe(res => {
                this.gridOptions.api.showLoadingOverlay()
                this.getGridData(res.id);
                 
                
                this.saveMessage = res.msg;
            });

        this.gridOptions = <GridOptions>{
            onGridReady1: (params) => {
                this.params=params;
                this.gridApi = params.api;
                this.gridOptions.api.sizeColumnsToFit();
                
              

            }
        };
        this.gridOptions1 = <GridOptions>{
            onGridReady: (params) => {
                this.params1=params;
                this.gridApi1 = params.api;
                this.gridOptions1.api.sizeColumnsToFit();
                this.getGridData(undefined);
              

            }
        };


        this.columnDefs = [

            { headerName: "Unsold", field: "unsoldQuantity",aggFunc: 'sum', enableValue: true,
            allowedAggFuncs: ['sum'] },
            { headerName: "Sold", field: "soldQuantity",aggFunc: 'sum', enableValue: true
           },
            { headerName: "Storage Name", field: "storage_name" },
            // { headerName: "Field From", field: "field_from", headerClass: "text-center" },
            { headerName: "Commodity", field: "comm_name", rowGroup:true },
            { headerName: "Crop Year", field: "crop_year" },
            // { headerName: "Load Name", field: "loadName" },
            // { headerName: "Qualities", field: "qualities", width: 200, tooltipField: "qualities" },            
            { headerName: "Grade", field: "grade_name" },
           
            { headerName: "Target Price", field: "target_price" },
            
            // { headerName: "Date Harvested", field: "date_harvested" },            
            // { headerName: "Storage Location", field: "storage_location", tooltipField: "storage_location" },
            // { headerName: "Chemicals", field: "chemicals", width: 200, tooltipField: "chemicals" }
        ];
        this.autoGroupColumnDef = {
            headerName: "Commodity",
            field: "comm_name",
            width: 200,
            rowGroup:true,
            cellRenderer:'agGroupCellRenderer',
            cellRendererParams: {
              footerValueGetter: '"Total (" + x + ")"',
              padding: 5
            }
          };

        this.columnDefs1 = [

            { headerName: "Storage Name", field: "STORAGE_NAME" },
            { headerName: "Storage Type", field: "STORAGE_LOC_DIRECTION" },
            { headerName: "Storage Name Capacity", field: "STORAGE_NEAR_TOWN", headerClass: "text-center" },
            { headerName: "Distance To Nearest City", field: "STORAGE_LOCA_DISTANCE" },
            { headerName: "Direction", field: "STORAGE_LOC_DIRECTION" },
            { headerName: "City", field: "U_A_CITY" },
            { headerName: "State", field: "U_A_STATE" },
            { headerName: "Country", field: "U_A_COUNTRY" },
            { headerName: "Post/ZipCode", field: "U_A_POSTCODE" },
            { headerName: "GPS", field: "GPS_LOCATION" },
            { headerName: "Farming Region", field: "FARMING_REGION" },
            { headerName: "Truck Type", field: "TRUCK_TYPE" },
        ];

        
        this.context = { componentParent: this };
        this.rowSelection = "multiple";
        this.frameworkComponents = {
            commodityUpdateCellRendererComponent: CommodityUpdateCellRendererComponent,
            tooltipCellRenderer: TooltipcellrendererComponent
        };


        
        this.defaultColDef = { editable: false };

    }
    currentUser;
    ngOnInit() {
        this.searchControl = new FormControl();

        console.log(this.currentUser);

        this.getCommodities();
        this.getWarehouseTypes();
        this.getWareHouseList();
        this.getChemicalList();
        this.getCropyearList();
        this.getOnfarmWarehouseList();
        this.getPublicWarehouseList();
        // this.getStorageRecord();
        // this.getCommodityGrade();
        // this.loc = this.locationElementRef.nativeElement;
        this.loc = new google.maps.places.SearchBox(this.loc);
        console.log(this.loc);
        //set google maps defaults
        this.zoom = 4;
        //create search FormControl
        this.searchControl = new FormControl();
        this.infoWindow = new google.maps.InfoWindow;
    }
    
    getStorageRecord(){
        // let url = `${serviceUrl}AgvInvGetStorageByIDSB/AgvInvGetStorageByIDPS/api/v1/getStoreByID?S_ID=${this.currentUser.USER.U_ID}`;
        
        // this.http.get(url).subscribe(data=>{
        //     if(data.json()!= null){
        //     this.gridOptions.api.setRowData([data.json()]);
        //         this.gridOptions.api.sizeColumnsToFit();
        //     }else{
        //         this.gridOptions.api.setRowData([]);
        //     }
        // },err=>{
        //     this.gridOptions.api.setRowData([]);
        // })
        // this.OnfarmList; this.PublicList;
        // this.OnfarmList.concat(this.PublicList);
        console.log("get storage strg inv grid", this.OnfarmList.concat(this.PublicList));
        this.gridOptions.api.setRowData(this.OnfarmList.concat(this.PublicList));
        this.gridOptions.api.hideOverlay();
    }

    actionRenderer() {
        let actiontemplate = `
<a style="cursor:pointer">
          <span class="glyphicon glyphicon-edit" style="margin-right:12px;margin-left:2px" aria-hidden="true"></span>
        </a>
<a style="cursor:pointer">
          <span class="glyphicon glyphicon-trash"  aria-hidden="true"></span>
        </a>
        
    `;
        return actiontemplate;
    }

    @ViewChild('noBDModal') noBDModal;

    @ViewChild('noBDModal1') noBDModal1;
    StorageData: any;
     onCellClicked($event) {

        if ($event.colDef.field == "storage_name") {
            this.storageid = $event.data.storage_id;
            let url = serviceUrl+"AgvInvGetStorageByIDSB/AgvInvGetStorageByIDPS/api/v1/getStoreByID?S_ID=" + this.storageid;
            this.http.get(url).subscribe((data) => {
                this.StorageData = data.json();
                // this.selectedCommodity1=this.commodities[0].C_NAME;
            })
            this.noBDModal1.show();
        }

    }

    methodFromParent(rowObj) {
        console.log("invoke modal", rowObj.data);
        this.inventory = rowObj.data;
        this.inventory.inventory_id = rowObj.data.storage_add_cid;
        this.inventory.storageid = rowObj.data.storage_id;
        this.inventory.SCC_C_ID = rowObj.data.comm_id;
        this.inventory.unsold_quantity = rowObj.data.unsoldQuantity;
        this.inventory.sold_quantity = rowObj.data.soldInventory
        this.getLoadList();
        this.getCommodityGrade(this.inventory.SCC_C_ID);
        this.getQualityList(this.inventory.SCC_C_ID);

        this.inventory.load_id = rowObj.data.loadId;
        let url = serviceUrl+"AgvInvGetStorageByIDSB/AgvInvGetStorageByIDPS/api/v1/getStoreByID?S_ID=" + rowObj.data.storage_id;
            this.http.get(url).subscribe((data) => {
                this.inventory.WarehouseType = data.json()?data.json().STORAGE_TYPE:'';
                // this.selectedCommodity1=this.commodities[0].C_NAME;
            })

        this.inventory.WarehouseType = rowObj.data.storage_name;

        this.inventory.Grade = rowObj.data.grade_id;
        this.inventory.quantity = rowObj.data.soldInventory;
        this.inventory.FieldFrom = rowObj.data.field_from;
        this.inventory.date_harvested = rowObj.data.date_harvested;
        this.inventory.INV_TargetPrice = rowObj.data.target_price;
        this.inventory.Cropyear = rowObj.data.crop_year;

        //this.qualityArray = rowObj.data.qualityattributes.quality;


        //this.chemArray = rowObj.data.chemicalattributes.chemical;


        // this.inventoryObj=rowObj.data;
        //this.commodity=rowObj.data;
        this.noBDModal.show();
    }
    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
    }
    onGridReady1(params) {
        this.gridApi1 = params.api;
        this.gridColumnApi1 = params.columnApi;
        // this.getStorageRecord();
    }
    onSelectionChanged(event) {
        var summaryArr = event.api.getSelectedNodes();
        this.pushInventory.emit(summaryArr);

        console.log(event.api.getSelectedNodes());
    }

    selectAllRows() {
        this.gridOptions.api.selectAll();
    }
    highlightrow(id:number){
     let index=0;
                 this.gridOptions.api.forEachNode((node) => {
                     index++;
                    if (node.data.storage_add_cid == id) {
                        // Master open detail.  You could also call node.setSelected( true ); for alternate design.
                        node.setSelected(true,true);
                        this.gridOptions.api.ensureIndexVisible(index-1, 'bottom')
                    }
                });   
    
    }

    getGridData(id) {
        let url = serviceUrl+'AgvInvGetCommodity/AgvInvGetCommPS/api/v1/getInvComm?U_ID=' + this.currentUser.USER.U_ID;
        this.http.get(url).subscribe((data) => {
            console.log('storage data:', data.json());
            if (data.json() != null) {
                let inventoryData = data.json().comStore;
                inventoryData.forEach((val) => {
                   /*  var chemicalsCSV = [];
                    var qualitiesCSV = [];
                    val.chemicalattributes.chemical.forEach((valin) => {
                        chemicalsCSV.push(valin.chemical_name + '-' + valin.chemical_value + '|' + valin.date_applied);
                    })
                    val.qualityattributes.quality.forEach((valin) => {
                        qualitiesCSV.push(valin.quality_name + '-' + valin.quality_value);
                    })
                    val.chemicals = chemicalsCSV.join(',');
                    val.qualities = qualitiesCSV.join(','); */

                });

               //params.api.setRowData(inventoryData);
               if(inventoryData[0]== null){
                inventoryData = [];
               }
               
                this.gridOptions1.api.setRowData(inventoryData);

                if(id!=undefined){
                  this.highlightrow(id);  
                }
                
               

            } else {

                this.gridOptions1.api.setRowData([]);
                 
            }


            // this.selectedCommodity1=this.commodities[0].C_NAME;
            // console.log(this.commodities);
        }, (err)=>{
            this.gridOptions1.api.setRowData([]);
        })  
          
    }

    getCommodities() {
        this.url = serviceUrl+"AgVantageGetCommoditiesSB/getCommodities/api/v1/getCommodity?getCommodities=true";
        this.http.get(this.url).subscribe((data) => {
            this.commodities = data.json().CommodityCollection.Commodity;
            // this.selectedCommodity1=this.commodities[0].C_NAME;
            console.log(this.commodities);
        })
    }

    getWarehouseTypes() {
        console.log('this.role.getRole(): ', this.role.getRole());
        if (this.role.getRole() === "superadmin") {
            this.WarehouseTypes = [
                { T_ID: '1', T_NAME: 'On Farm' },
                { T_ID: '2', T_NAME: 'Ex Farm' }
            ]
        } else if (this.role.getRole() === "P") {
            this.WarehouseTypes = [
                { T_ID: 'OFW', T_NAME: 'On Farm' },
                { T_ID: 'PW', T_NAME: 'Public' }
            ]
        }
    }

    getWareHouseList() {
        this.PublicList = [
            { PB_ID: '1', PB_NAME: 'public abcd' },
            { PB_ID: '2', PB_NAME: 'public syd' }
        ];

        this.OnfarmList = [
            { OF_ID: '1', OF_NAME: 'OnFarm syd' },
            { OF_ID: '2', OF_NAME: 'OnFarm asdf' }
        ];

        this.ExfarmList = [
            { IH_ID: '1', IH_NAME: 'exfarm asdf' },
            { IH_ID: '2', IH_NAME: 'exfarm qwee' }
        ];
    }

    getCropyearList() {
        let url = serviceUrl+'AgvGetCropYear/GetCropYear/api/v1/getCropYear?getcropyer=true';
        this.http.get(url).subscribe((data) => {
            // console.log('crp year data:',data.json().CropYear);
            this.cropYearList = data.json().CropYear;
            // this.selectedCommodity1=this.commodities[0].C_NAME;
            // console.log(this.commodities);
        })
    }

    getQualityList(id) {
        this.url = serviceUrl+"AgvInvengetQuality/AvgInvGetQualityPS/api/v1/getQuality?C_ID=" + id;
        this.http.get(this.url).subscribe((data) => {
            // console.log('data:',data);
            this.qualityList = data.json().Quality;
            // this.selectedCommodity1=this.commodities[0].C_NAME;
            // console.log(this.commodities);
        })
    }

    chemicalList;
    getChemicalList() {
        this.url = serviceUrl+"AgvInvGetChemical/AgvInvGetChemPS/api/v1/getchemicals?getChemical=true";
        this.http.get(this.url).subscribe((data) => {
            // console.log('chemical data:', data);
            this.chemicalList = data.json().Chemicals;
            // this.selectedCommodity1=this.commodities[0].C_NAME;
            // console.log(this.commodities);
        })

    }

    callSelectedType() {
        console.log('selected warehouse type: ', this.inventory.WarehouseType);
        if (this.inventory.WarehouseType == 1) {
            this.showExfarm = false;
            this.showPublic = false;
            this.showOnfarm = true;
        } else if (this.inventory.WarehouseType == 2) {
            if (this.role.getRole() === "superadmin") {
                this.showExfarm = true;
            }
            else {
                // if (this.role.getRole() === "p"){
                this.showPublic = true;
                console.log('--- show public : ', this.showPublic);
                // }
            }
            this.showOnfarm = false;
            console.log('--- show showOnfarm : ', this.showOnfarm);
        }
        // this.getWareHouseData(this.inventory.WarehouseType); 
    }

    callselectedQuality() {
        console.log('selected quality', this.inventory.Quality);
    }

    // qualityArray = [{disable:false}]; "quality_name" : "string",
    qualityArray = [{ quality_name: '', quality_value: '' }];
    hideAddButton: boolean = true;
    createNewQualityFields(obj, index) {

        let newObj = Object.assign({}, obj);
        // obj.disable = true;
        if (this.qualityArray.length < 11) {
            newObj.qualityName = '';
            newObj.qualityValue = '';
            this.qualityArray.push(newObj);
            console.log('create new quality fields...', this.qualityArray);
        }
    }

    removeNewQualityFields(obj, index) {
        console.log('obj', obj, 'i:', index);
        this.qualityArray.splice(index, 1);
    }
    // chemArray = [0];"chemical_name" : "string",
  

    chemArray = [{ chemical_name: '', chemical_value: '', date_applied: '' }];

    createNewChemFields(obj, index) {
        let newChemObj = Object.assign({}, obj);
        if (this.chemArray.length < 11) {
            newChemObj.fumgnORChem = '';
            newChemObj.chemicalRatesApplied = '';
            newChemObj.dateApplied = '';
            this.chemArray.push(newChemObj);
        }
        console.log('create new chemical fields...', this.chemArray);
    }

    removeNewChemFields(obj, index) {
        console.log('obj', obj, 'i:', index);
        // const idx: number = this.chemArray.indexOf(obj);
        this.chemArray.splice(index, 1);
    }

    // ngOnDestroy() {
    //   this.componentRef.destroy();
    // }

    callGrades() {
        this.getCommodityGrade(this.inventory.SCC_C_ID);
        this.getQualityList(this.inventory.SCC_C_ID);
    }

    grades;
    getCommodityGrade(id) {
        this.url = serviceUrl+"AgVantageGetCommodityGrade/AgVantageGetGradePS/api/v1/getGrade?C_ID=" + id;
        this.http.get(this.url).subscribe((data) => {
            this.grades = data.json().gradeCollection.grade;
        })
    }

    showMapPage;
    getDistance() {
        this.setCurrentPosition();
        this.inputSource = this.sourceSearchElementRef.nativeElement;
        this.inputDest = this.destiSearchElementRef.nativeElement;
        this.sourceBox = new google.maps.places.SearchBox(this.inputSource);
        this.destBox = new google.maps.places.SearchBox(this.inputDest);
        // this.initializeAutocomplete();
        // this.initMap();
    }


    getOnfarmWarehouseList() {
        console.log('storage inventory.Onfarm : ', this.inventory.Onfarm);
        let url = serviceUrl+'AgcInvgetStorage/AgvInvGetStoragePS/api/v1/getStorage?store_type=OFW&u_id='+this.currentUser.USER.U_ID;
        this.http.get(url).subscribe((data) => {
            console.log('storage data:', data);
            this.OnfarmList = data.json()?data.json().store : [];
            // this.selectedCommodity1=this.commodities[0].C_NAME;
            // console.log(this.commodities);
        })
    }

    getPublicWarehouseList() {
        console.log('staorage inventory.public : ', this.inventory.Onfarm);
        let url = serviceUrl+'AgcInvgetStorage/AgvInvGetStoragePS/api/v1/getStorage?store_type=PW&u_id='+this.currentUser.USER.U_ID;
        this.http.get(url).subscribe((data) => {
            console.log('storage data:', data);
            this.PublicList = data.json()?data.json().store : [];
            // this.selectedCommodity1=this.commodities[0].C_NAME;
            // console.log(this.commodities);
            this.getStorageRecord();
        })
    }
    getLoadList() {
        console.log('inventory.Onfarm : ', this.inventory.Onfarm);
        let url = serviceUrl+'AgvInvGetLoad/AgvInvGetLoadPS/api/v1/getLoad?input=true';
        this.http.get(url).subscribe((data) => {
            console.log('storage data:', data);
            this.loadList = data.json().Loadvalues;
            // this.selectedCommodity1=this.commodities[0].C_NAME;
            // console.log(this.commodities);
        })
    }


    submitInventory;
    changeUnsold() {
        this.inventory.unsold_quantity = this.inventory.unsold_quantity - this.inventory.sold_quantity;
    }
    updateInventory() {
        console.log('Post Inventory json :', this.inventory);
        this.myModal.hide();

        let params = {
            "inventory_id": this.inventory.inventory_id,
            "storage_id": this.inventory.storageid,
            "AddCommodityCollection": {
                "AddCommodity": [{
                    "comm_id": this.inventory.SCC_C_ID,
                    "load_id": this.inventory.load_id,
                    "unsold_quantity": this.inventory.unsold_quantity,
                    "sold_inventory": this.inventory.sold_quantity,
                    "u_id": this.currentUser.USER.U_ID,
                    "grade_id": this.inventory.Grade,
                    "quantity": this.inventory.quantity,
                    "field_from": this.inventory.FieldFrom,
                    "date_harvested": this.inventory.date_harvested,
                    "target_price": this.inventory.INV_TargetPrice,
                    "crop_year": this.inventory.Cropyear,
                    "qualityattributes": {
                        "quality": this.qualityArray
                    },
                    "chemicalattributes": {
                        "chemical": this.chemArray
                    }
                }]
            }
        }




        console.log('params', params);


        let URL = serviceUrl+'AgvInvAddCommodity/AgvInvAddCommPS/api/v1/AddInv';
        //this.spinnerService.show();
        this.headers = new Headers({
            'Content-Type': 'application/json; charset=UTF-8',
            'Accept': 'application/json'
        });
        // this.option = new RequestOptions({ headers: this.headers });
        this.http.post(URL, params, new RequestOptions({ headers: this.headers })).subscribe((data) => {
            console.log('--- add inv data.json() : ', data.json());
            this.submitInventory = data.json().outcome;
            let res={"id":data.json().inventory_id,"msg":"Updated Successfully"};
            this.addInventoryForm.reset();
            this.broadCaster.broadcast('Update Event',res);
            this.myModal.hide();
            // this.spinnerService.hide();
            $(".modal-content").scrollTop(0);
        },
            (err) => {
                console.log(err);
            })
    }
    submitStorage;
    addStorageInfo() {

        let params = {
            STORAGE_NAME: this.addStorage.StorageName,
            U_A_STREET: this.addStorage.U_A_STREET,
            U_A_POSTCODE: this.addStorage.U_A_POSTCODE,
            U_A_CITY: this.addStorage.U_A_CITY,
            U_A_STATE: this.addStorage.U_A_STATE,
            U_A_COUNTRY: this.addStorage.U_A_COUNTRY,
            STORAGE_LOCA_DISTANCE: this.addStorage.StorageDistance,
            STORAGE_LOC_DIRECTION: this.addStorage.StorageDirection,
            STORAGE_NEAR_TOWN: this.addStorage.NEAR_TOWN,
            STORAGE_TYPE: 'PW',
            U_ID: this.currentUser.USER.U_ABN
        }
        console.log('params', params);

        let URL = serviceUrl+'AgvStoreRegistration/AgvStoreAddPS/api/v1/addStore';
        this.spinnerService.show();
        this.headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Accept': 'application/json'
        });
        // this.option = new RequestOptions({ headers: this.headers });
        this.http.post(URL, $.param(params), new RequestOptions({ headers: this.headers })).subscribe((data) => {
            console.log('--- add store data.json() : ', data.json());
            this.submitStorage = data.json().Result.Outcome;
            this.inventory = {};
            this.grade = "";
            this.spinnerService.hide();
            $(".modal-content").scrollTop(0);
        })
    }

    /* Show current position */
    private setCurrentPosition() {

        if ("geolocation" in navigator) {
            navigator.geolocation.getCurrentPosition((position) => {
                console.log("setCurrentPosition ", position);
                this.latitude = position.coords.latitude;
                this.longitude = position.coords.longitude;
                this.zoom = 12;
                // google.maps.event.addListener(window, 'load', this.initialize());
                // this.initializeAutocomplete();
            });
        }
    }


    dst;
    initMap() {
        var directionsService = new google.maps.DirectionsService;

        // let src = document.getElementById('source-input');
        // let dest = document.getElementById('destination-input');
        var src = this.sourceBox.getPlaces();
        var dest = this.destBox.getPlaces();
        console.log('source ', src);
        console.log('Destination ', dest);

        // var bounds = new google.maps.LatLngBounds;
        // var markersArray = [];
        // this.initializeAutocomplete(input, map);
        var origin1 = src[0].formatted_address;//'Pune, Maharashtra, India'; //{lat: 55.93, lng: -3.118};
        // var origin2 = 'Greenwich, England';
        var destinationA = dest[0].formatted_address; //'Delhi, India';
        // var destinationB = {lat: 50.087, lng: 14.421};

        // var destinationIcon = 'https://chart.googleapis.com/chart?' +
        //     'chst=d_map_pin_letter&chld=D|FF0000|000000';
        // var originIcon = 'https://chart.googleapis.com/chart?' +
        //     'chst=d_map_pin_letter&chld=O|FFFF00|000000';
        let currentPosition = new google.maps.LatLng(this.latitude, this.longitude);

        // var distcalcmap = new google.maps.Map(document.getElementById('distancemap'), {
        //     center: currentPosition,
        //     zoom: 13,
        //     mapTypeId: 'roadmap'
        // });

        // var directionsDisplay = new google.maps.DirectionsRenderer({ map: distcalcmap });

        var geocoder = new google.maps.Geocoder;

        var service = new google.maps.DistanceMatrixService;
        service.getDistanceMatrix({
            // origins: [origin1, origin2],
            origins: [origin1],
            // destinations: [destinationA, destinationB],
            destinations: [destinationA],
            travelMode: 'DRIVING',
            unitSystem: google.maps.UnitSystem.METRIC,
            avoidHighways: false,
            avoidTolls: false
        }, function(response, status) {
            if (status !== 'OK') {
                alert('Error was: ' + status);
            } else {
                var originList = response.originAddresses;
                var destinationList = response.destinationAddresses;
                var outputDiv = document.getElementById('output');
                outputDiv.innerHTML = '';
                // this.deleteMarkers(markersArray);
                // for (var i = 0; i < markersArray.length; i++) {
                //     markersArray[i].setMap(null);
                // }
                // markersArray = [];

                // var showGeocodedAddressOnMap = function(asDestination) {
                //     // var icon = asDestination ? destinationIcon : originIcon;
                //     return function(results, status) {
                //         if (status === 'OK') {
                //             this.distcalcmap.fitBounds(bounds.extend(results[0].geometry.location));
                //             markersArray.push(new google.maps.Marker({
                //                 map: this.distcalcmap,
                //                 position: results[0].geometry.location,
                //                 icon: icon
                //             }));
                //         } else {
                //             alert('Geocode was not successful due to: ' + status);
                //         }
                //     };
                // };

                for (var i = 0; i < originList.length; i++) {
                    var results = response.rows[i].elements;
                    geocoder.geocode({ 'address': originList[i] },
                        // showGeocodedAddressOnMap(false)
                    );
                    for (var j = 0; j < results.length; j++) {
                        geocoder.geocode({ 'address': destinationList[j] },
                            // showGeocodedAddressOnMap(true)
                        );
                        // var dist = document.getElementById('dist').innerHTML;
                        // var dur = document.getElementById('duration').innerHTML;
                        // dur = results[j].duration.text;
                        // this.dist = results[j].distance.text;
                        // this.duration = results[j].duration.text;
                        outputDiv.innerHTML += originList[i] + ' to ' + destinationList[j] +
                            '<br> <b> Distance: ' + results[j].distance.text + '</b> in <b>' +
                            results[j].duration.text + '</b> <br>';
                        this.addStorage.StorageDistance = results[j].distance.text;
                        console.log('--- storage grid distance :', this.addStorage.StorageDistance);
                    }
                }
            }
        });

        // Retrieve the start and end locations and create a DirectionsRequest using
        // WALKING directions.
        // directionsService.route({
        //     origin: origin1, //document.getElementById('start').value,
        //     destination: destinationA,//document.getElementById('end').value,
        //     travelMode: 'DRIVING'
        // }, function(response, status) {
        //     // Route the directions and pass the response to a function to create
        //     // markers for each step.
        //     if (status === 'OK') {
        //         // document.getElementById('warnings-panel').innerHTML =
        //         //     '<b>' + response.routes[0].warnings + '</b>';
        //         directionsDisplay.setDirections(response);
        //         // showSteps(response, markerArray, stepDisplay, map);
        //     } else {
        //         window.alert('Directions request failed due to ' + status);
        //     }
        // });
    }

    deleteMarkers(markersArray) {
        for (var i = 0; i < markersArray.length; i++) {
            markersArray[i].setMap(null);
        }
        markersArray = [];
    }
    

}

