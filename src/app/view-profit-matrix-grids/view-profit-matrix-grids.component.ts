import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { serviceUrl } from "../app.module";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router } from '@angular/router';
import { UserRoleService } from '../user-role.service';


@Component({
  selector: 'app-view-profit-matrix-grids',
  templateUrl: './view-profit-matrix-grids.component.html',
  styleUrls: ['./view-profit-matrix-grids.component.scss']
})
export class ViewProfitMatrixGridsComponent implements OnInit {
 
  commonColDefs: {}[];
  breakevenPerUnitData: any;
  profitPerHectareData: any;
  profitPerUnitData: any;
  ROIData: any;
  commodities: any;
  cropYears: any;
  profitMatrix = {commodity:"", cropyear:""};
  commonQueryParam: string;
  showGrids : boolean = false;
  whatIfMatrixData: any;
  colDefWhatIfMatrix: any[];
  colTypes: any;
  constructor(private http: Http, public spinnerService: Ng4LoadingSpinnerService, private router : Router, private user : UserRoleService) { }

  ngOnInit() {
  
    this.getCommonColDefs();
    this.getColDefForWhatIfMatrix();
    this.getCommodity();
    this.getCropyear();
    
  }
  gotoHome() {
    this.router.navigate(['dashboard']);
}
  getCommonColDefs(): any {
    this.commonColDefs = [ { headerName: "", field: "C1" },
    { headerName: "-25.00%", field: "C2" },
    { headerName: "-22.50%", field: "C3" },
    { headerName: "-20.00%", field: "C4" },
    { headerName: "-17.50%", field: "C5" },
    { headerName: "-15.00%", field: "C6" },
    { headerName: "-12.50%", field: "C7" },
    { headerName: "-10.00%", field: "C8" },
    { headerName: "-7.50%", field: "C9" },
    { headerName: "-5.00%", field: "C10" },
    { headerName: "-2.50%", field: "C11" },
    { headerName: "Yield", field: "C12" },
    { headerName: "2.50%", field: "C13" },
    { headerName: "5.00%", field: "C14" },
    { headerName: "7.50%", field: "C15" },
    { headerName: "10.00%", field: "C16" },
    { headerName: "12.50%", field: "C17" },
    { headerName: "15.00%", field: "C18" },
    { headerName: "17.50%", field: "C19" },
    { headerName: "20.00%", field: "C20" },
    { headerName: "22.50%", field: "C21" },
    { headerName: "25.00%", field: "C22" }];

  }
  getColDefForWhatIfMatrix(): any {
    this.colTypes = {
      yieldColumn: {
        width: 100,
        columnGroupShow: "open",
        suppressFilter: true
      }
    };
      
    
    this.colDefWhatIfMatrix = [
      { headerName: "Commodity", field: "commodity" },
      { headerName: "Price", field: "starting_quantity" },
      { headerName: "Yield", field: "yield_per_hectare" },
      { headerName: "Area(ha.)", field: "MAX_SB_AREA_HECTARES_" },
      { headerName: "Units", field: "MAX_SB_GROWINGBREAKEVENPERUNIT_" },
      {
        headerName: "Yield",
        groupId: "yieldGroup",
        children: [
          {
            headerName: "Profit/Unit",
            field : 'ProfitPerUnit',
            valueParser: "Number(newValue)",
            type: "yieldColumn"
          },
          {
            headerName: "Profit/Hectare",
            editable: true,
            field  : 'ProfitPerHectare',
            valueParser: "Number(newValue)",
            type: "yieldColumn",
            valueSetter: (params)=>{
              if (params.oldValue!==params.newValue) {
                params.data['ProfitPerHectare'] = params.newValue;
                // get grid to refresh the cell
                return true;
            } else {
                // no change, so no refresh needed
                return false;
            }
            
          }},
          {
            headerName: "ROI",
            field : 'ROI',
            valueParser: "Number(newValue)",
            valueGetter : (params)=>{
              return  (((params.data.ProfitPerHectare) - params.data.MAX_SB_GROWINGBREAKEVENPERUNIT_)/ params.data.MAX_SB_GROWINGBREAKEVENPERUNIT_) * 100;
            },
            type: "yieldColumn"
          }
        ]
      }

    ];
  }
 
  
  getROI(): any {
    let url = `${serviceUrl}ProfitMatrixServiceSB/ProfitMatrixRestService/api/v1/getROI${this.commonQueryParam}`;
    
    this.http.get(url).subscribe(data => {
      this.ROIData = data.json() ? data.json().Row : [];
      
    },
      err => {
        alert("Some server error occured");
        this.ROIData = [];
      })
  }
  getProfitPerUnit(): any {
    
    let url = `${serviceUrl}ProfitMatrixServiceSB/ProfitMatrixRestService/api/v1/getProfitPerUnit${this.commonQueryParam}`;
    this.http.get(url).subscribe(data => {
      this.profitPerUnitData = data.json() ? data.json().Row : [];
      
    },
      err => {
        alert("Some server error occured");
        this.profitPerUnitData = [];
      })
  }
  getProfitPerHectare(): any {
    let url = `${serviceUrl}ProfitMatrixServiceSB/ProfitMatrixRestService/api/v1/getProfitPerHectare${this.commonQueryParam}`;
    this.http.get(url).subscribe(data => {
      this.profitPerHectareData = data.json() ? data.json().Row : [];
    },
      err => {
        alert("Some server error occured");
        this.profitPerHectareData  = [];
      })
  }
  getBreakevenPerUnit(): any {
    let url = `${serviceUrl}ProfitMatrixServiceSB/ProfitMatrixRestService/api/v1/getBreakevenPerUnit${this.commonQueryParam}`;
    this.http.get(url).subscribe(data => {
      this.breakevenPerUnitData = data.json() ? data.json().Row : [];
    },
      err => {
        alert("Some server error occured");
        this.breakevenPerUnitData = [];
      })
  }
  getWhatIfMatrix(): any {
    let url = `${serviceUrl}ProfitMatrixServiceSB/ProfitMatrixRestService/api/v1/getWhatIfMatrixData?UserId=${this.user.getUser().USER.U_ID}`;
    this.http.get(url).subscribe(data => {
      this.whatIfMatrixData = data.json() ? data.json().CommodityEntry : [];
    },
      err => {
        alert("Some server error occured");
        this.whatIfMatrixData = [];
      })
  }
   callGridApis(){
     this.spinnerService.show();
    this.showGrids = false;
    setTimeout(()=>{
      this.showGrids = true;
      this.spinnerService.hide();
    },5000);
    this.commonQueryParam=`?Commodity=${this.profitMatrix.commodity}&CropYear=${this.profitMatrix.cropyear}&UserId=${this.user.getUser().USER.U_ID}`;
    this.getBreakevenPerUnit();
    this.getProfitPerHectare();
    this.getProfitPerUnit();
    this.getROI();
    this.getWhatIfMatrix();
   }
   getCropyear(): any {
    let url = `${serviceUrl}AgvGetCropYear/GetCropYear/api/v1/getCropYear?getcropyear=true`;
    this.http.get(url).subscribe(data => {
      this.cropYears = data.json() ? data.json().CropYear : [];
      this.spinnerService.hide();
    },
      err => {
        this.spinnerService.hide();
        alert("Some server error occured");
      })
  }
  getCommodity(): any {
    let url = `${serviceUrl}AgVantageGetCommoditiesSB/getCommodities/api/v1/getCommodity?getCommodities=true`;
    this.spinnerService.show();
    this.http.get(url).subscribe(data => {
      this.commodities = data.json() ? data.json().CommodityCollection.Commodity : [];
      this.spinnerService.hide();
    },
      err => {
        this.spinnerService.hide();
        alert("Some server error occured");

      })
  }

  

}
