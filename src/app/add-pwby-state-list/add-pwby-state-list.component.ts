import { Component, ViewChild, OnInit,ChangeDetectorRef } from '@angular/core';
import {GridOptions} from "ag-grid/main";

@Component({
  selector: 'app-add-pwby-state-list',
  templateUrl: './add-pwby-state-list.component.html',
  styleUrls: ['./add-pwby-state-list.component.scss']
})
export class AddPWByStateListComponent implements OnInit {

  gridOptions: GridOptions;
  gridApi;
  gridColumnApi;
   
   columnDefs;

   rowData;
   defaultColDef;
   getRowNodeId;
  
   constructor(private cdRef: ChangeDetectorRef) { 
    // we pass an empty gridOptions in, so we can grab the api out

    this.gridOptions = <GridOptions>{
      onGridReady: () => {
        // this.gridOptions.columnApi.autoSizeAllColumns();
        this.gridOptions.api.sizeColumnsToFit();
      }
    };


    this.columnDefs = [
      {
        headerName: "",
        children: [
        { headerName: "", field: "attrs" },
        { headerName: "Actions", field: "actions", cellRenderer: this.actionRenderer, cellEditor: 'agRichSelect', editable: true },
        { headerName: "Unit", field: "unit", width:100},
        { headerName: "Street", field: "street"},
        { headerName: "Suburb", field: "suburb" },
        { headerName: "City", field: "city"},
        { headerName: "State", field: "state" },
        { headerName: "PinCode", field: "pinCode" },
        { headerName: "Nearest Terminal Port", field: "nearTrmnlPort" }
      ]
      }
    ];

    this.rowData = [
      {
        attrs: "Wheat",
        unit: "100000",
        street: "12,355.00",
        suburb: "4942.00",
        city: "2471.00",
        state: "0.00",
        pinCode: "1235.00",
        nearTrmnlPort: "1235.00"
      },
      {
        attrs: "Wheat",
        unit: "100000",
        street: "12,355.00",
        suburb: "4942.00",
        city: "2471.00",
        state: "0.00",
        pinCode: "1235.00",
        nearTrmnlPort: "1235.00"
      },
      {
        attrs: "Wheat",
        unit: "100000",
        street: "12,355.00",
        suburb: "4942.00",
        city: "2471.00",
        state: "0.00",
        pinCode: "1235.00",
        nearTrmnlPort: "1235.00"
      },
      {
        attrs: "Wheat",
        unit: "100000",
        street: "12,355.00",
        suburb: "4942.00",
        city: "2471.00",
        state: "0.00",
        pinCode: "1235.00",
        nearTrmnlPort: "1235.00"
      },
      {
        attrs: "Wheat",
        unit: "100000",
        street: "12,355.00",
        suburb: "4942.00",
        city: "2471.00",
        state: "0.00",
        pinCode: "1235.00",
        nearTrmnlPort: "1235.00"
      }
      
    ];

    this.defaultColDef = { editable: false };

    this.getRowNodeId = function (data) {
      return data.id;
    };
  }

  ngOnInit() {
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

  }

  selectAllRows() {
    this.gridOptions.api.selectAll();
  }
  actionRenderer() {
    let actiontemplate = `
<a href="#">
          <span class="glyphicon glyphicon-edit" style="margin-right:12px;margin-left:2px" aria-hidden="true"></span>
        </a>
<a href="#">
          <span class="glyphicon glyphicon-trash"  aria-hidden="true"></span>
        </a>`;
    return actiontemplate;
  }


  
}
