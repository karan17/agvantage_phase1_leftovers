import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommodityUpdateCellRendererComponent } from './commodity-update-cell-renderer.component';

describe('CommodityUpdateCellRendererComponent', () => {
  let component: CommodityUpdateCellRendererComponent;
  let fixture: ComponentFixture<CommodityUpdateCellRendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommodityUpdateCellRendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommodityUpdateCellRendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
