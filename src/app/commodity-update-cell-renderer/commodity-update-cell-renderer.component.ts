import { Component, OnInit } from '@angular/core';
import {ICellRendererAngularComp} from "ag-grid-angular";

@Component({
  selector: 'app-commodity-update-cell-renderer',
  templateUrl: './commodity-update-cell-renderer.component.html',
  styleUrls: ['./commodity-update-cell-renderer.component.scss']
})
export class CommodityUpdateCellRendererComponent implements ICellRendererAngularComp  {

  
    public params: any;

    agInit(params: any): void {
        this.params = params;
    }

    public invokeParentMethod() {
        console.log(this.params.node);
        this.params.context.componentParent.methodFromParent(this.params.node);
    }

    refresh(): boolean {
        return false;
    }

}
