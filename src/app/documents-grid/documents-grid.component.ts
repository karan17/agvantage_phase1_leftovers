import { Component, OnInit } from '@angular/core';
import {GridOptions} from "ag-grid/main";
import {Http} from "@angular/http";
import { serviceUrl } from '../app.module';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { UserRoleService } from '../user-role.service';
@Component({
  selector: 'app-documents-grid',
  templateUrl: './documents-grid.component.html',
  styleUrls: ['./documents-grid.component.scss']
})
export class DocumentsGridComponent implements OnInit {

   gridOptions: GridOptions;
   gridApi;
   gridColumnApi;
   columnDefs;
   rowData;
   defaultColDef;
   getRowNodeId;
  currentUser: any;

  constructor(private http : Http, public spinnerService: Ng4LoadingSpinnerService, private role :UserRoleService ) { 
    this.gridOptions = <GridOptions>{
      onGridReady: () => {
          this.gridOptions.api.sizeColumnsToFit();
          
      }
    };
    this.currentUser = this.role.getUser();

    this.columnDefs = [
      { headerName: "Select", checkboxSelection: true, headerCheckboxSelection: true,
     field: "actions", width: 200,  editable: true },
      {headerName:"Document name",field: "name", onCellClicked : (params)=>{
        this.downloadDoc(params.data)
      },
      cellRenderer: (params) => `<a>${params.value}</a>`
      , cellStyle: (params) => {
        return { cursor: 'pointer', color: 'blue' }
      }},
      {headerName: "Contract Number", field: "ContractNo"},
      {headerName: "Reference Number", field: "RefNo"},
      {headerName: "Created Date", field: "createdDate"},
      {headerName: "Buyer", field: "Buyer"},
      {headerName: "Seller", field: "Seller"},
      {headerName: "Modified Date", field: "Modified_Date"},
      {headerName: "Uploaded By", field: "Uploaded_By"},
      {headerName: "Kind", field: "DocType"}
    ]
      

     

    this.defaultColDef = { editable: false };

    this.getRowNodeId = function(data) {
      return data.id;
    };

  }

  downloadDoc(doc){
    this.spinnerService.show();
    let url = `${serviceUrl}DocumentsService/DocumentsServicePS/api/v1/GetDocumentById?docId=${doc.documentId}`;
    this.http.get(url).subscribe(data=>{
      this.saveByteArray()([this.base64ToArrayBuffer(data.json()?data.json().Document:[])], `${doc.name}`);
      this.spinnerService.hide();
    },err=>{
      this.spinnerService.hide();
    })
  }
    base64ToArrayBuffer(base64) {
    var binaryString =  window.atob(base64);
    var binaryLen = binaryString.length;
    var bytes = new Uint8Array(binaryLen);
    for (var i = 0; i < binaryLen; i++)        {
        var ascii = binaryString.charCodeAt(i);
        bytes[i] = ascii;
    }
    return bytes;
}

 saveByteArray() {
    let a = document.createElement("a");
    document.body.appendChild(a);
    //a.style = "display: none";
    return function (data, name) {
        var blob = new Blob(data, {type: "octet/stream"}),
            url = window.URL.createObjectURL(blob);
        a.href = url;
        a.download = name;
        a.click();
        window.URL.revokeObjectURL(url);
    };
};

  ngOnInit() {
  }
  getDocuments(){
    
    let url = `${serviceUrl}DocumentsService/DocumentsServicePS/api/v1/GetDocumentsByUser?UserId=${this.currentUser.USER.U_ID}`
    this.http.get(url).subscribe(data=>{
      this.gridOptions.api.setRowData(data.json()?data.json().Documents:[]);
      this.gridOptions.api.sizeColumnsToFit();
    },err=>{
      alert("Some error occured on the server");
    })
  }
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.getDocuments();

  }

  selectAllRows() {
    this.gridOptions.api.selectAll();
  }

}
