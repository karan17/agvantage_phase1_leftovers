import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import {serviceUrl } from '../app.module';
import {UserRoleService} from '../user-role.service';
import { GridOptions } from "ag-grid/main";
import { Router } from '@angular/router';

@Component({
  selector: 'app-production-sales-forecast',
  templateUrl: './production-sales-forecast.component.html',
  styleUrls: ['./production-sales-forecast.component.scss']
})
export class ProductionSalesForecastComponent implements OnInit {
  currentUser: any;
  gridOptions: GridOptions;
  rowData: any;
  columnDefs : any;
  detailCellRendererParams : any;
  detailRowHeight: number;
  getRowHeight: (params: any) => number;
  constructor(private http: Http,private router : Router, private user : UserRoleService) { }
 
  ngOnInit() {
    this.currentUser = this.user.getUser();
    
    this.gridOptions = <GridOptions>{
      onGridReady: () => {
        this.gridOptions.api.sizeColumnsToFit();
        
      }
    };
    this.columnDefs = [
      {headerName:"Commodity", field:'Commodity_name', cellRenderer: "agGroupCellRenderer"},
      {headerName:"Crop Year", field:'cropYear'},
      {headerName:"Total Production", field:'totalProduction'},
      {headerName:"Retained Speed", field:'RetainedSeed'},
      {headerName:"Saleable Production", field:'SaleableProduction'},
      {headerName:"Recommended Sale", field:'RecommendedSale'},
      {headerName:"RecommendedSale Quantity", field:'RecommendedSaleQuantity'},
      {headerName:"Modified Datetime", field:'ModifiedDatetime'}
     
    ];
    this.detailRowHeight = 300;
    this.getRowHeight = function(params) {
      if (params.node && params.node.detail) {
        var offset = 100;
        var allDetailRowHeight = (params.data.ForwardSaleSummary?params.data.ForwardSaleSummary.ForwardSale:[]).length * 28;
        return allDetailRowHeight + offset;
      } else {
        return 80;
      }
    };
  this.detailCellRendererParams = {
    detailGridOptions: {
      columnDefs: [
        {headerName : 'Forward Sales', children : [
          {headerName:"Contract Number", field: 'contract_number'},
        {headerName:"Sold Quantity", field: 'sold_quantity'},
        ]}
       
      ]
      
    },
    getDetailRowData: function(params) {
      params.successCallback(params.data.ForwardSaleSummary?params.data.ForwardSaleSummary.ForwardSale:[]);
    }
  };
}


  onGridReady(params){
    this.getProductionSales();
    params.api.sizeColumnsToFit();
    params.api.setGridAutoHeight(true);
  }

  getProductionSales(){
    let url = `${serviceUrl}AgvantageGetProductionAndSalesForecast/AgvantageGetProductionSalesForecastRestPS/api/v1/GetSalesForecast?user_id=${this.currentUser.USER.U_ID}`;
    this.http.get(url).subscribe(data=>{
      this.rowData = data.json()?data.json().SalesForecast.commodityDetails:[];
    },err=>{
      this.rowData = [];
    })
  }
  gotoHome() {
    this.router.navigate(['dashboard']);
}
}
