import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';

// Import Plugins
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { AccordionModule } from 'ngx-bootstrap/accordion';
//import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { ModalModule } from 'ngx-bootstrap/modal';
import { CarouselModule } from 'ngx-bootstrap/carousel';
//import { PopoverModule } from 'ngx-bootstrap/popover';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { AmChartsModule } from "@amcharts/amcharts3-angular";
import { UiSwitchModule } from 'ngx-ui-switch';
//import { LaddaModule } from 'angular2-ladda';
//import { SortablejsModule } from 'angular-sortablejs';
//import { SweetAlert2Module } from '@toverux/ngsweetalert2';
//import { ConfirmationPopoverModule } from 'angular-confirmation-popover';
//import { NgxDatatableModule } from '@swimlane/ngx-datatable';
//import { CustomFormsModule } from 'ng2-validation';
import { GooglePlaceModule } from 'ng2-google-place-autocomplete/src';
import { AgGridModule } from 'ag-grid-angular';
import {LicenseManager} from "ag-grid-enterprise/main";
LicenseManager.setLicenseKey("Vedantus_Solutions_Pty_Ltd_agnalysis.io__1Devs19_June_2019__MTU2MDg5ODgwMDAwMA==0a6caab9b5fe3dc86f539a9934d778e0");
import { BsDatepickerModule } from 'ngx-bootstrap';
import { Broadcaster } from './broadcaster';
import "ag-grid-enterprise";
// Import routes
import { routes } from './app.routes';

// Import Services
import { SettingsService } from './_services/settings.service';
import { UserRoleService } from './user-role.service';
import { MessageService } from './message-service.service';

// Components
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { SettingsComponent } from './settings/settings.component';
import { TopNavbarComponent } from './top-navbar/top-navbar.component';
import { Dashboard2Component } from './dashboard-2/dashboard-2.component';
import { GoogleMapComponent } from './google-map/google-map.component';
//import { ColorPrimaryComponent } from './color-primary/color-primary.component';
//import { ColorDangerComponent } from './color-danger/color-danger.component';
//import { ColorSuccessComponent } from './color-success/color-success.component';
//import { ColorWarningComponent } from './color-warning/color-warning.component';
//import { ColorInfoComponent } from './color-info/color-info.component';
//import { ColorBlackComponent } from './color-black/color-black.component';
//import { ColorGrayComponent } from './color-gray/color-gray.component';
//import { UiButtonsComponent } fraaom './ui-buttons/ui-buttons.component';
//import { UiPanelsComponent } from './ui-panels/ui-panels.component';
//import { UiModalsComponent } from './ui-modals/ui-modals.component';
//import { UiPageHeadersComponent } from './ui-page-headers/ui-page-headers.component';
//import { UiConfirmationComponent } from './ui-confirmation/ui-confirmation.component';
//import { UiDropdownMenuComponent } from './ui-dropdown-menu/ui-dropdown-menu.component';
//import { UiMediaObjectsComponent } from './ui-media-objects/ui-media-objects.component';
//import { BtAccordionsComponent } from './bt-accordions/bt-accordions.component';
//import { BtAlertsComponent } from './bt-alerts/bt-alerts.component';
//import { BtButtonDropdownComponent } from './bt-button-dropdown/bt-button-dropdown.component';
//import { BtButtonGroupsComponent } from './bt-button-groups/bt-button-groups.component';
//import { BtButtonsComponent } from './bt-buttons/bt-buttons.component';
//import { BtCarouselComponent } from './bt-carousel/bt-carousel.component';
//import { BtCodeComponent } from './bt-code/bt-code.component';
//import { BtCollapseComponent } from './bt-collapse/bt-collapse.component';
//import { BtDropdownsComponent } from './bt-dropdowns/bt-dropdowns.component';
//import { BtGridComponent } from './bt-grid/bt-grid.component';
//import { BtHelpersComponent } from './bt-helpers/bt-helpers.component';
//import { BtLabelsBadgesComponent } from './bt-labels-badges/bt-labels-badges.component';
//import { BtPaginationComponent } from './bt-pagination/bt-pagination.component';
//import { BtPopoverComponent } from './bt-popover/bt-popover.component';
//import { BtTabsComponent } from './bt-tabs/bt-tabs.component';
//import { BtTooltipsComponent } from './bt-tooltips/bt-tooltips.component';
//import { BtTypographyComponent } from './bt-typography/bt-typography.component';
//import { TableBasicComponent } from './table-basic/table-basic.component';
//import { Table10kRowsComponent } from './table-10k-rows/table-10k-rows.component';
//import { TableInlineEditComponent } from './table-inline-edit/table-inline-edit.component';
//import { TableFilterComponent } from './table-filter/table-filter.component';
//import { ChartColumnBarComponent } from './chart-column-bar/chart-column-bar.component';
//import { ChartLineAreaComponent } from './chart-line-area/chart-line-area.component';
//import { ChartPieChartComponent } from './chart-pie-chart/chart-pie-chart.component';
//import { ChartFunnelComponent } from './chart-funnel/chart-funnel.component';
//import { ChartOthersComponent } from './chart-others/chart-others.component';
//import { Error404Component } from './error-404/error-404.component';
//import { Error500Component } from './error-500/error-500.component';
//import { FormBasicComponent } from './form-basic/form-basic.component';
//import { FormLayoutsComponent } from './form-layouts/form-layouts.component';
//import { FormValidationsComponent } from './form-validations/form-validations.component';
import { PagesPricingComponent } from './pages-pricing/pages-pricing.component';
import { PagesProfileComponent } from './pages-profile/pages-profile.component';
import { LoginComponent } from './login/login.component';
import { BlankComponent } from './blank/blank.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SignupComponent } from './signup/signup.component';
import { ForgetpswrdComponent } from './forgetpswrd/forgetpswrd.component';
import { ResetpswrdComponent } from './resetpswrd/resetpswrd.component';
import { ProductionCostGridComponent } from './production-cost-grid/production-cost-grid.component';
import { MarketsalesgridComponent } from './marketsalesgrid/marketsalesgrid.component';
import { StorageInventoryGridComponent } from './storage-inventory-grid/storage-inventory-grid.component';
import { ViewProductionGridComponent } from './view-production-grid/view-production-grid.component';
import { ViewStorageGridComponent } from './view-storage-grid/view-storage-grid.component';
import { ViewMarketGridComponent } from './view-market-grid/view-market-grid.component';
import { AddcommodityComponent } from './addcommodity/addcommodity.component';
import { ContactsAndFavoriteComponent } from './contacts-and-favorite/contacts-and-favorite.component';
import { AddedcommoditylistComponent } from './addedcommoditylist/addedcommoditylist.component';
import { AddedContactListComponent } from './added-contact-list/added-contact-list.component';
import { AddCommodityByComponent } from './add-commodity-by/add-commodity-by.component';
import { CommodityUpdateCellRendererComponent } from './commodity-update-cell-renderer/commodity-update-cell-renderer.component';
import { AddCommodityByGradeListComponent } from './add-commodity-by-grade-list/add-commodity-by-grade-list.component';
import { AddPWByStateListComponent } from './add-pwby-state-list/add-pwby-state-list.component';
import { AddCommodityByTypeListComponent } from './add-commodity-by-type-list/add-commodity-by-type-list.component';
import { AddQualityComponent } from './add-quality/add-quality.component';
import { AddQualityGridComponent } from './add-quality-grid/add-quality-grid.component';
import { BreakEvenForcastComponent } from './break-even-forcast/break-even-forcast.component';
import { BreakEvenForcastGridComponent } from './break-even-forcast-grid/break-even-forcast-grid.component';
import { AccountDetailsGridComponent } from './account-details-grid/account-details-grid.component';
import { TooltipcellrendererComponent } from './tooltipcellrenderer/tooltipcellrenderer.component';
import { ContactAndFavCellRendererComponentComponent } from './contact-and-fav-cell-renderer-component/contact-and-fav-cell-renderer-component.component';
import { FreightCalculatorComponent } from './freight-calculator/freight-calculator.component';
import { OnlyLoggedInUserGuardService } from './only-logged-in-user-guard.service';
import { AboutusComponent } from './aboutus/aboutus.component';
import { TeamComponent } from './team/team.component';
import { ProductsComponent } from './products/products.component';
import { OurServicesComponent } from './our-services/our-services.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { FaqComponent } from './faq/faq.component';
import { ProducerComponent } from './producer/producer.component';
import { SalesAndM2mPositionComponent } from './sales-and-m2m-position/sales-and-m2m-position.component';
import { ContactsFavouritesComponent } from './contacts-favourites/contacts-favourites.component';
import { FavouritesGridComponent } from './favourites-grid/favourites-grid.component';

import { ContactsFavNameDetailsCellRendererComponent } from './contacts-fav-name-details-cell-renderer/contacts-fav-name-details-cell-renderer.component';
import { ProfitabilityMatrixComponent } from './profitability-matrix/profitability-matrix.component';
import { ProfitabilityMatrixGridComponent } from './profitability-matrix-grid/profitability-matrix-grid.component';
import { ProfitabilityMatrixYieldComponent } from './profitability-matrix-yield/profitability-matrix-yield.component';
import { ProfitabilityMatrixGrowingProfitComponent } from './profitability-matrix-growing-profit/profitability-matrix-growing-profit.component';
import { ContractorsOnFarmComponent } from './contractors-on-farm/contractors-on-farm.component';
import { ContractorDetailsGridComponent } from './contractor-details-grid/contractor-details-grid.component';
import { CommodityMovementGridComponent } from './commodity-movement-grid/commodity-movement-grid.component';
import { ViewPcbGridComponent } from './view-pcb-grid/view-pcb-grid.component';
import { EditDeleteRendererComponent } from './edit-delete-renderer/edit-delete-renderer.component';
import { DocumentsComponent } from './documents/documents.component';
import { DocumentsGridComponent } from './documents-grid/documents-grid.component';
import { PcbgridComponent } from './pcbgrid/pcbgrid.component';
import { SalesPosByCommAndStorageGridComponent } from './sales-pos-by-comm-and-storage-grid/sales-pos-by-comm-and-storage-grid.component';
import { SalesPosByCommodityGridComponent } from './sales-pos-by-commodity-grid/sales-pos-by-commodity-grid.component';
import { SuperadminComponent } from './superadmin/superadmin.component';
import { ProductionSalesForecastComponent } from './production-sales-forecast/production-sales-forecast.component';
import { SalesPosDetailsViewComponent } from './sales-pos-details-view/sales-pos-details-view.component';
import { SalesPosM2MViewComponent } from './sales-pos-m2m-view/sales-pos-m2m-view.component';
import { SuperadminDetailedRevenueViewComponent } from './dashboard-2/superadmin-detailed-revenue/superadmin-detailed-revenue.component';
import { SalesAndPurchaseComponent } from './sales-and-purchase/sales-and-purchase.component';
import { SuperadminDetailedClientViewComponent } from './dashboard-2/superadmin-detailed-client/superadmin-detailed-client.component';
import { NgrEntityDisplayComponent } from './ngr-entity-display/ngr-entity-display.component';
import { ViewProfitMatrixGridsComponent } from './view-profit-matrix-grids/view-profit-matrix-grids.component';
import { AgGridWrapperComponent } from './ag-grid-wrapper/ag-grid-wrapper.component';
import { AddNewFieldsComponent } from './addfields/addfields.component';




@NgModule({
    declarations: [
        AppComponent,
        DashboardComponent,
        Dashboard2Component,
        SettingsComponent,
        TopNavbarComponent,
        PagesPricingComponent,
        PagesProfileComponent,
        LoginComponent,
        BlankComponent,
        HeaderComponent,
        FooterComponent,
        SignupComponent,
        ForgetpswrdComponent,
        ResetpswrdComponent,
        ProductionCostGridComponent,
        MarketsalesgridComponent,
        StorageInventoryGridComponent,
        ViewProductionGridComponent,
        ViewStorageGridComponent,
        ViewMarketGridComponent,
        AddcommodityComponent,
        ContactsAndFavoriteComponent,
        AddedcommoditylistComponent,
        AddedContactListComponent,
        GoogleMapComponent,
        CommodityUpdateCellRendererComponent,
        AddCommodityByComponent,
        AddCommodityByGradeListComponent,
        AddPWByStateListComponent,
        AddCommodityByTypeListComponent,
        AddQualityComponent,
        AddQualityGridComponent,
        BreakEvenForcastComponent,
        BreakEvenForcastGridComponent,
        AccountDetailsGridComponent,
        TooltipcellrendererComponent,
        ContactAndFavCellRendererComponentComponent,
        FreightCalculatorComponent,
        AboutusComponent,
        TeamComponent,
        ProductsComponent,
        OurServicesComponent,
        ContactUsComponent,
        FaqComponent,
        ProducerComponent,
        SalesAndM2mPositionComponent,
        ContactsFavouritesComponent,
        FavouritesGridComponent,
        ContactsFavNameDetailsCellRendererComponent,
        ProfitabilityMatrixComponent,
        ProfitabilityMatrixGridComponent,
        ProfitabilityMatrixYieldComponent,
        ProfitabilityMatrixGrowingProfitComponent,
        ContractorsOnFarmComponent,
        ContractorDetailsGridComponent,
        CommodityMovementGridComponent,
        ViewPcbGridComponent,
        EditDeleteRendererComponent,
        DocumentsComponent,
        DocumentsGridComponent,
        PcbgridComponent,
        SalesPosByCommAndStorageGridComponent,
        SalesPosByCommodityGridComponent,
        SuperadminComponent,
        ProductionSalesForecastComponent,
        SalesPosDetailsViewComponent,
		SalesPosM2MViewComponent,
		SuperadminDetailedRevenueViewComponent,
		SalesAndPurchaseComponent,
		SuperadminDetailedClientViewComponent,
		NgrEntityDisplayComponent,
		ViewProfitMatrixGridsComponent,
		AgGridWrapperComponent,
        AddNewFieldsComponent

    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        TabsModule.forRoot(),
        CarouselModule,
        RouterModule.forRoot(routes),
        BsDropdownModule.forRoot(),
        ModalModule.forRoot(),
        AccordionModule.forRoot(),
        AgGridModule.withComponents([CommodityUpdateCellRendererComponent,
            TooltipcellrendererComponent,
            ContactAndFavCellRendererComponentComponent, EditDeleteRendererComponent]),
        TooltipModule.forRoot(),
        Ng4LoadingSpinnerModule.forRoot(),
        AmChartsModule,
        UiSwitchModule,
        ReactiveFormsModule,
        GooglePlaceModule,
        BsDatepickerModule.forRoot()
    ],
    providers: [
        SettingsService,
        UserRoleService,
        MessageService,
        Broadcaster,
        OnlyLoggedInUserGuardService
    ],
    bootstrap: [
        BlankComponent
    ]
})
export class AppModule { }
export const serviceUrl = "https://agnalysis.io:9074/";
