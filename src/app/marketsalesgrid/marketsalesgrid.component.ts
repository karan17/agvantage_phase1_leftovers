import { Component, OnInit } from '@angular/core';
import {GridOptions} from "ag-grid/main";

@Component({
  selector: 'app-marketsalesgrid',
  templateUrl: './marketsalesgrid.component.html',
  styleUrls: ['./marketsalesgrid.component.scss']
})
export class MarketsalesgridComponent  {

  gridOptions: GridOptions;
     gridApi;
   gridColumnApi;

   columnDefs;

   rowData;
   defaultColDef;
   getRowNodeId;

    constructor() {
        // we pass an empty gridOptions in, so we can grab the api out
        this.gridOptions = <GridOptions>{
            onGridReady: () => {
                this.gridOptions.api.sizeColumnsToFit();
                
            }
        };
        this.columnDefs = [
            {headerName:"Sold Quantity and Covered Value",groupId:"area",headerClass:"text-center",
            children:[
            {headerName: "", field: "attrs",width:300},
            {headerName: "Sample", field: "sample"},
            {headerName: "Wheat", field: "wheat"},
            {headerName: "Durum", field: "durum"},
            {headerName: "Feed Barley", field: "feed"},
            {headerName: "Malt Barley", field: "malt"},
            {headerName: "Canola", field: "canola"},
            {headerName: "Chickpeas", field: "chickpeas"},
            {headerName: "Faba Beans", field: "faba"}]
            }
        ];
        
         this.rowData=[{
        id: "gg",
        attrs: "Total Production",
        sample: "1,85,000.00",
        wheat: "15,000.00",
        durum:"6,400.00",
        feed:"3,300.00",
        malt:"0.00",
        canola:"900.00",
        chickpeas:"900.00",
        faba:"1100.00"
      },
      {
        id: "gg",
        attrs: "Insurance Coverage Level",
        sample: "85%",
        wheat:"85%",
        durum:"85%",
        feed:"85%",
        malt:"85%",
        canola:"85%",
        chickpeas:"85%",
        faba:"85%"
      },
      {
        id: "gg",
        attrs: "Salable Production",
        sample: "1,57,250.00",
        wheat: "12,750.00",
        durum:"5,400.00",
        feed:"2,805.00",
        malt:"0.0",
        canola:"765.00",
        chickpeas:"765.00",
        faba:"935.00"
      },
      {
        id: "gg",
        attrs: "Recommended Sales %",
        sample: "20%",
        wheat: "20%",
        durum:"20%",
        feed:"20%",
        malt:"20%",
        canola:"20%",
        chickpeas:"20%",
        faba:"20%"
      }];
   
     this.defaultColDef = { editable: false };
    this.getRowNodeId = function(data) {
      return data.id;
    };
  }



  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

    selectAllRows() {
        this.gridOptions.api.selectAll();
    }
}

