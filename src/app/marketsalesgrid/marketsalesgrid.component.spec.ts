import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketsalesgridComponent } from './marketsalesgrid.component';

describe('MarketsalesgridComponent', () => {
  let component: MarketsalesgridComponent;
  let fixture: ComponentFixture<MarketsalesgridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarketsalesgridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketsalesgridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
