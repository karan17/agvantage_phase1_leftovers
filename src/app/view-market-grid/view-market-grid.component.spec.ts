import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewMarketGridComponent } from './view-market-grid.component';

describe('ViewMarketGridComponent', () => {
  let component: ViewMarketGridComponent;
  let fixture: ComponentFixture<ViewMarketGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewMarketGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewMarketGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
