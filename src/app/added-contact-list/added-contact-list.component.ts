import { Component, ViewChild, OnInit, NgZone, ChangeDetectorRef, Output, Input, EventEmitter } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import { UserRoleService } from '../user-role.service';
// import { CommodityUpdateCellRendererComponent } from '../commodity-update-cell-renderer/commodity-update-cell-renderer.component';
import { ContactAndFavCellRendererComponentComponent } from '../contact-and-fav-cell-renderer-component/contact-and-fav-cell-renderer-component.component';
import { ContactsFavNameDetailsCellRendererComponent } from '../contacts-fav-name-details-cell-renderer/contacts-fav-name-details-cell-renderer.component';

import {Http, Headers, RequestOptions} from '@angular/http';
import {serviceUrl} from "../app.module";
import {Broadcaster} from '../broadcaster';
import { EditDeleteRendererComponent } from '../edit-delete-renderer/edit-delete-renderer.component';
import {FormGroup, FormBuilder, Validators, FormControl, NgForm} from '@angular/forms';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';

@Component({
    selector: 'app-added-contact-list',
    templateUrl: './added-contact-list.component.html',
    styleUrls: ['./added-contact-list.component.scss']
})
export class AddedContactListComponent implements OnInit {

    gridOptions: GridOptions;
    gridApi;
    gridColumnApi;
    columnDefs;
    rowData;
    defaultColDef;
    getRowNodeId;
    frameworkComponents;
    nameDetailsFrameworkComponents;
    context;
    currentUser: any;
    saveMessage;
    currentContactuser: any;
    condetails;
    disableSubmit = true;
    USER;
    @ViewChild('contactAndFavEditModel') contactAndFavEditModel;
    @ViewChild('addContactForm') addContactForm;
    @ViewChild('viewContactDetails') viewContactDetails;
    @ViewChild('userform') public userform;
    @ViewChild('delModal') public delModal;
    @Output('pushSelectedContact') pushSelectedContact = new EventEmitter();
    // @Input('selectedMenu') selectedMenu;
    cntxt;showViewDetails;
    form: FormGroup;
    userStatusRes; checking;titleList;sameAdd;
    public options = { types: ['address'], componentRestrictions: { country: 'AUS' } };
    
    constructor(private _ngZone: NgZone, private cdRef: ChangeDetectorRef, private broadCaster: Broadcaster, 
        private http: Http, private role: UserRoleService, private spinnerService: Ng4LoadingSpinnerService) {
        // we pass an empty gridOptions in, so we can grab the api out
        this.cntxt = this;
        this.condetails = {};
        this.USER = {};
        this.userStatusRes = {};
        this.sameAdd=true;
        this.currentUser = this.role.getUser();
        this.titleList = [{ title: 'Mr' }, { title: 'Mrs' }, { title: 'Other' }];
        this.getRegion();
        this.gridOptions = <GridOptions>{
            onGridReady: () => {
                // this.gridOptions.columnApi.autoSizeAllColumns();
                // this.gridOptions.api.sizeColumnsToFit();
            }
        };

        this.broadCaster.on<any>('Save Event Contact')
            .subscribe(res => {
                this.gridOptions.api.showLoadingOverlay()
                this.getGridData(undefined);
                this.saveMessage = res;
                setTimeout(()=>{this.saveMessage = false},4000);
            });

        this.broadCaster.on<any>('Edit Event Contact')
            .subscribe(res => {
                console.log(res);
                this.gridOptions.api.showLoadingOverlay()
                this.getGridData(undefined);
                this.saveMessage = res;
                setTimeout(()=>{this.saveMessage = false},4000);
            });
        
            this.broadCaster.on<any>('Delete Contact Event')
            .subscribe(res => {
                console.log(res);
                this.gridOptions.api.showLoadingOverlay()
                this.getGridData(undefined);
                this.saveMessage = res;
                setTimeout(()=>{this.saveMessage = false},4000);
            });
           
        this.columnDefs = [
            { headerName: "Select", checkboxSelection: true, field: "actions", width: 200, cellRenderer: 'commodityUpdateCellRendererComponent', cellEditor: 'agRichSelect', editable: true },
            { headerName: "Actions", field: "",  cellRenderer: 'editdeletecellrenderer', cellEditor: 'agRichSelect', editable: true },
            { headerName: "First Name", field: "first_name" ,cellRenderer: (params) => `<a>${params.value}</a>`, cellStyle: (params) => {
                return { cursor: 'pointer', color: 'blue' }
              } },
            { headerName: "NGR", field: "ngr" },
            { headerName: "Title", field: "add_title" },
            { headerName: "Last Name", field: "last_name" },
            { headerName: "Role Title", field: "role_title" },
            { headerName: "Account Mgr", field: "account_mgr" },
            { headerName: "Office phone", field: "office_ph" },
            { headerName: "Office Fax", field: "office_fax" },
            { headerName: "Home phone", field: "home_ph" },
            { headerName: "Home Fax", field: "home_fax" },
            { headerName: "Work Mobile", field: "work_mobile" },
            { headerName: "Personal Mobile", field: "personal_mobile" },
            { headerName: "Work Email", field: "work_email" },
            { headerName: "Address", field: "address" },
            { headerName: "City", field: "city" },
            { headerName: "State", field: "state" },
            { headerName: "Country", field: "country" },
            { headerName: "Postcode", field: "postcode" },
            { headerName: "Region", field: "region" },
            { headerName: "UHF", field: "UHF" },
            { headerName: "Partner Name", field: "partner_name" },
            { headerName: "Spouse Email", field: "spouse_email" },
            { headerName: "Homephone", field: "add_homephone" },
            { headerName: "Home fax", field: "add_homefax" },
            { headerName: "Partner mobile", field: "add_partnermobile" },
            { headerName: "Notes", field: "add_notes" }
        ];

        
        this.defaultColDef = { editable: false };
        this.context = { componentParent: this };
        this.frameworkComponents = {
            'editdeletecellrenderer': EditDeleteRendererComponent
          }

        this.nameDetailsFrameworkComponents = {
            ContactsFavNameDetailsCellRendererComponent: ContactsFavNameDetailsCellRendererComponent
        };

        // this.getRowNodeId = function(data) {
        //     return data.id;
        // };

        

    }

    

    tabs = [
        {title: 'Dynamic Title 1', content: 'Dynamic content 1', active: true, disabled: false},
        {title: 'Dynamic Title 2', content: 'Dynamic content 2', active: false, disabled: true},
        {title: 'Dynamic Title 2', content: 'Dynamic content 3', active: false, disabled: true},
        {title: 'Dynamic Title 2', content: 'Dynamic content 4', active: false, disabled: true},
        {title: 'Dynamic Title 2', content: 'Dynamic content 5', active: false, disabled: true},
        {title: 'Dynamic Title 2', content: 'Dynamic content 6', active: false, disabled: true}
      ];

      selectNextTab(index) {
        this.tabs[index].active = true;
        this.tabs[index].disabled = false;
      }


      onFilterChange(event: any) {
        console.log("event",event);
        if (event.target.checked) {
          this.disableSubmit= false;
          console.log("--disableSubmit",this.disableSubmit);
        }
        else {
          this.disableSubmit= true;
          console.log("--disableSubmit",this.disableSubmit);
        }    
      }

      onSelectionChanged(event) {
        var summaryArr = event.api.getSelectedNodes();
        this.pushSelectedContact.emit(summaryArr);

        console.log(event.api.getSelectedNodes());
    }

      farmRegions: any;
      getRegion() {
        let url = serviceUrl + 'AgvGetRegionSB/AGVGetRegionPS/api/v1/getRegion?getRegion=true';
        // console.log();
        this.http.get(url).subscribe(data => {
          // Read the result field from the JSON response.
          console.log(data.json().getRegion);
          this.farmRegions = data.json().getRegion;
        });
      }

      radioDiffAddChange(e){
        console.log('radio diff add--', e);
        this.USER = {};
        // console.log('--- on change user',this.USER);
    }

    radioSameAddChange(e){
        console.log(' radio same add--', e);
        this.USER = this.role.getUser().USER;
        // console.log('--- on change user',this.USER);
    }

    place;
    getAddress(place: Object) {
        console.log("Address", place);
        this.place = place;
    }

    telephoneRequired;mobileRequired;
    keyPress(event: any) {

        console.log("keypressed...");
        const pattern = /[0-9\+\-\ ]/;
    
        let inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode != 8 && !pattern.test(inputChar)) {
          event.preventDefault();
        }
        if (this.USER.U_MOBILE && (this.USER.U_MOBILE.length == 4 || this.USER.U_MOBILE.length == 8)) {
          this.mobileRequired = this.isFieldValid(this.form.get(this.USER.U_MOBILE));
          this.USER.U_MOBILE = this.USER.U_MOBILE + '-';
        }
        if (this.USER.U_TELEPHONE && (this.USER.U_TELEPHONE.length == 2 || this.USER.U_TELEPHONE.length == 7)) {
          console.log("",this.form.get(this.USER.U_TELEPHONE));
          this.telephoneRequired = this.isFieldValid(this.form.get(this.USER.U_TELEPHONE));
          this.USER.U_TELEPHONE = this.USER.U_TELEPHONE + '-';      
        }
      }
     
      isFieldValid(field) {
        Object.keys(this.form.controls).forEach(field => {  //{2}
          const control = this.form.get(field);  
          console.log("--- control: ", control);           //{3}
          if (control instanceof FormControl) { 
            console.log("--- FormControl: ", control);            //{4}
            control.markAsTouched({ onlySelf: true });
          }
           else if (control instanceof FormGroup) {
            console.log("--- FormGroup: ", control);  
            
            control.markAsTouched({ onlySelf: true });
            return true       //{5}
            // this.isFieldValid(control);            //{6}
          }
        });
        // console.log("isFieldValid...", field);
      
      }

      

    onCellClicked($event) {

        console.log("--- Event:",$event);
        if ($event.colDef.field == "first_name") {
            this.viewContactDetails.show();
            this.condetails = $event.data;
            console.log("--- this.condetails:",this.condetails);
            // this.storageid = $event.data.storage_id;
            // let url = serviceUrl+"AgvInvGetStorageByIDSB/AgvInvGetStorageByIDPS/api/v1/getStoreByID?S_ID=" + this.storageid;
            // this.http.get(url).subscribe((data) => {
            //     this.StorageData = data.json();
            //     // this.selectedCommodity1=this.commodities[0].C_NAME;
            // })
            // this.noBDModal1.show();
        }

    }

    // this.showViewDetails = function(params) {
    //     console.log("--- showview: ", params);
    //     this.viewContactDetails.show()
    // }

    ngOnInit() {
        //this.getCommodities();
    }

    openNameDetails(params){
        console.log("--- params: ",params);
    }

    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;
        this.getGridData(undefined);
        this.getRowNodeId = function (data) {
            return data.id;
          };
    }

    selectAllRows() {
        this.gridOptions.api.selectAll();
    }

    closeModal() {
        this.contactAndFavEditModel.hide();
    }

    closeDeleteDialog(evt){
        this.delModal.hide();
    }

    

    closeViewContact(){
        this.viewContactDetails.hide();
    }

    highlightrow(id: number) {
        let index = 0;
        this.gridOptions.api.forEachNode((node) => {
            index++;
            if (node.data.id == id) {
                // Master open detail.  You could also call node.setSelected( true ); for alternate design.
                node.setSelected(true, true);
                this.gridOptions.api.ensureIndexVisible(index - 1, 'bottom')
            }
        });

    }
    
    getGridData(id) {
        // console.log("this.fromcontact", this.selectedMenu);
        let url;
        // if(this.selectedMenu == "fromcontact")
         url = serviceUrl+'GetAllContacts/GetAllContacts/api/v1/getAllContactsById?EntityAdminId=' + this.currentUser.USER.U_ID;
        // else
        //  url = `${serviceUrl}AgvantageAdminGetAllContacts/AgvantageAminGetAllContactsRestPS/api/v1/GetAllContacts?input=true`;
        // https://agnalysis.io:9074/AgvantageAdminGetAllContacts/AgvantageAminGetAllContactsRestPS/api/v1/GetAllContacts?input=true
        
       
        this.http.get(url).subscribe((data) => {
            console.log('storage data:', data.json());
            if (data.json().Contacts != null) {
                let contactData = data.json().Contacts.Contact;


                //params.api.setRowData(inventoryData);
                this.gridOptions.api.setRowData(contactData);
                if (id != undefined) {
                    this.highlightrow(id);
                }



            } else {

                this.gridOptions.api.setRowData([])

            }


            // this.selectedCommodity1=this.commodities[0].C_NAME;
            // console.log(this.commodities);
        })
    }


    actionRenderer() {
        let actiontemplate = `
<a href="#">
          <span class="glyphicon glyphicon-edit" style="margin-right:12px;margin-left:2px" aria-hidden="true"></span>
        </a>`;
        return actiontemplate;
    }

    commodities
    getCommodities() {
        let url = serviceUrl+"AgvContactGetCommSB/AgvGetContactComPS/api/v1/getContComm?U_ID=" + this.currentContactuser;
        this.http.get(url).subscribe((data) => {
            this.commodities = data.json().CommoditiesList ? data.json().CommoditiesList.Commodity : [];
            // this.selectedCommodity1=this.commodities[0].C_NAME;
            this.commodities.forEach((val) => {
                if (val.Status == "true") { val.Status = true } else { val.Status = false }
            })
        })
    }
    favs: any = [];
    getFavorite() {
        let url = serviceUrl+"AgvContactGetFavSB/AgvContactGetFavPS/api/v1/getContactFav?uid=" + this.currentContactuser;
        this.http.get(url).subscribe((data) => {
            this.favs = data.json().CommoditiesList ? data.json().CommoditiesList.Commodityfavs : [];
            // this.selectedCommodity1=this.commodities[0].C_NAME;
            this.favs.forEach((val) => {
                if (val.Status == "true") { val.Status = true } else { val.Status = false }
            })
        })
    }
    notifyMsg: any;
    setFav($event, status, contactComm) {

        let favObj = { "commodity_id": contactComm.CommID, "status": status, user_id: this.currentContactID };
        let url = serviceUrl+"SetFavourites/SetFavourite/api/v1/setFavouriteById";
        this.notifyMsg = undefined;
        this.http.post(url, favObj).subscribe((data) => {
            console.log(data);
            this.notifyMsg = data.json().response;
        },
            (err) => {

            });
    }

    customFieldArray;

    currentContactID: any;
    public methodFromParent(rowObj) {
        this.contactAndFavEditModel.show();
        console.log("contact list modal", rowObj.data);
        this.USER = rowObj.data;
        this.currentContactuser = rowObj.data.Id;
        this.currentContactID = rowObj.data.Id;
        //this.getCommodities();
        //this.getFavorite();
        this.notifyMsg = undefined;
        
    }

    methodFromParentDelete(rowObj){
        console.log("contact delete modal", rowObj.data);
        this.delModal.show();
        this.currentContactID = rowObj.data.u_id;
    }

    submitContactAndFavEditModel; submitContactAndFav;
    updateContactAndFav() {

    }

    ngrlist = [];
    selectedNGR(value) {
      this.USER.U_NGR = value;
  
      console.log("selected ngr", this.USER.NGRDDL);
      for (let i = 0; i < this.ngrlist.length; i++) {
        if (this.ngrlist[i].NGR == this.USER.U_NGR) {
          // console.log(this.ngrlist[i].NGRUserList.Users.User);
          this.USER.U_TITLE = this.ngrlist[i].Title;
          this.USER.U_FNAME = this.ngrlist[i].Fname;
          this.USER.U_LNAME = this.ngrlist[i].Lname;
          this.USER.U_USERNAME = this.ngrlist[i].Username;
          this.USER.U_MOBILE = this.ngrlist[i].Mobile;
          this.USER.U_TELEPHONE = this.ngrlist[i].Telephone;
          this.USER.U_A_STREET = this.ngrlist[i].A_Street;
          this.USER.U_A_CITY = this.ngrlist[i].A_City;
          this.USER.U_A_STATE = this.ngrlist[i].A_State;
          this.USER.U_A_COUNTRY = this.ngrlist[i].A_Country;
          this.USER.U_A_POSTCODE = this.ngrlist[i].A_Postcode;
        }
      }
    }
    hideValid;
    checkUserStatus() {
      console.log(this.userform);
      let emailValid = this.userform.form.controls.email.status;
      this.checking = true;
      if (this.USER.work_email != undefined && this.USER.work_email.length > 0 && emailValid === "VALID") {
        this.checking = true;
        let url = serviceUrl + "AccountGatewayCheckUserStatus/AccountGatewayUserStatusPS/checkUserStatus?USERNAME=" + this.USER.U_USERNAME;
        this.http.get(url).subscribe(data => {
          // Read the result field from the JSON response.
          this.checking = false;
          this.hideValid = true;
          console.log(data.json());
          this.userStatusRes = data.json();
        });
      } else if (emailValid === "INVALID") {
        this.checking = false;
        this.hideValid = false;
      }
    }

    setCity(event) {

        this._ngZone.run(() => this.USER.U_A_CITY = event)
    
      }

      selectedRegion() {
        console.log('...selected region', this.USER.U_Farm_Region);
      }
  
    NGRStatus;
    checkNgr() {
      this.spinnerService.show();
      let url = serviceUrl + "AccountGatewayCheckNGRStatusSB/ProxyService/AccountGatewayNGRPS/api/v1/checkNGRStatus?NGR=" + this.USER.U_NGR;
      this.http.get(url).subscribe(data => {
        // Read the result field from the JSON response.
        this.spinnerService.hide();
        console.log(data.json());
        var ngres = data.json();
        this.NGRStatus = ngres.Status;
        if (this.NGRStatus == "NOT EXISTS") {
          this.USER.U_TITLE = '',
            this.USER.U_FNAME = '',
            this.USER.U_LNAME = '',
            this.USER.U_USERNAME = '',
            this.USER.U_MOBILE = '',
            this.USER.U_TELEPHONE = '',
            this.USER.U_A_STREET = '',
            this.USER.U_A_CITY = '',
            this.USER.U_A_STATE = '',
            this.USER.U_A_COUNTRY = '',
            this.USER.U_A_POSTCODE = ''
        }
      });
  
    }
    private _markFormPristine(form: FormGroup | NgForm): void {
      Object.keys(form.controls).forEach(control => {
        form.controls[control].markAsDirty();
      });
    }
  
    status: {isopen: boolean} = {isopen: false};
  
    toggleDropdown($event: MouseEvent): void {
      $event.preventDefault();
      $event.stopPropagation();
      this.status.isopen = !this.status.isopen;
    }
  
    change(value: boolean): void {
      this.status.isopen = value;
    }

    
    postContact() {

        var contactDetail = {
          "entity_id": this.currentUser.USER.U_ID,
          "ngr": this.USER.ngr,
          "first_name": this.USER.first_name,
          "last_name": this.USER.last_name,
          "role_title": this.USER.role_title,
          "account_mgr": this.USER.account_mgr,
          "office_ph": this.USER.office_ph,
          "office_fax": this.USER.office_fax,
          "home_ph": this.USER.home_ph,
          "home_fax": this.USER.home_fax,
          "work_mobile": this.USER.work_mobile,
          "personal_mobile": this.USER.personal_mobile,
          "work_email": this.USER.work_email,
          "address": this.USER.address,
          "city": this.USER.city,
          "state": this.USER.state,
          "country": this.USER.country,
          "postcode": this.USER.postcode,
          "region": this.USER.region,
          "UHF": this.USER.UHF,
          "add_title": this.USER.add_title,
          "partner_name": this.USER.partner_name,
          "spouse_email": this.USER.spouse_email,
          "add_homephone": this.USER.add_homephone,
          "add_homefax": this.USER.add_homefax,
          "add_partnermobile": this.USER.add_partnermobile,
          "add_notes": this.USER.add_notes
        }
    
        let url = serviceUrl + "AgvEntityEditContactsSB/AgvEntityEditContactsRestPS/api/v1/EditContact";
        this.contactAndFavEditModel.hide();
        this.spinnerService.show();
        this.http.post(url, contactDetail).subscribe(
          (data) => {
            this.addContactForm.reset();
            this.spinnerService.hide();
            this.broadCaster.broadcast('Edit Event Contact', "Contact Edited Successfully");
          },
          (err) => {
            console.log(err);
          }
        );
      }

      deleteContact(){        
          
          let url = serviceUrl + "AgvEntityDeleteContactSB/AgvEntityDeleteContactRestPS/api/v1/DeleteContact";
          this.spinnerService.show();
          this.http.post(url,{U_ID : this.currentContactID}).subscribe(
            (data) => {
              this.spinnerService.hide();
              console.log("del fav res: ", data);
              this.delModal.hide();
              this.broadCaster.broadcast('Delete Contact Event', "Deleted Succesfully");
            
            },
            (err) => {
              console.log("del contractor err: ", err);
            }
          );
      }
}
