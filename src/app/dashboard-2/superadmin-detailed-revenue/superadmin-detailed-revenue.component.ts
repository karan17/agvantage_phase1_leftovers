import {Component, OnInit} from '@angular/core';
import {GridOptions} from "ag-grid/main";
import {serviceUrl} from "../../app.module";
import {Http, Headers, RequestOptions} from '@angular/http';
import {UserRoleService} from '../../user-role.service';

@Component({
	selector: 'aap-superadmin-detailed-revenue-view',
	templateUrl: './superadmin-detailed-revenue.component.html',
	styleUrls: ['./superadmin-detailed-revenue.component.scss']
})
export class SuperadminDetailedRevenueViewComponent implements OnInit {

	gridOptions: GridOptions;
	gridApi;
	gridColumnApi;
	getRowNodeId;
	defaultColDef;
	columnDefs;
	rowData;
	currentUser;

	constructor(private http: Http, private role: UserRoleService) {
		this.currentUser = this.role.getUser();
		this.gridOptions = <GridOptions>{
			onGridReady: () => {
				 this.gridOptions.api.sizeColumnsToFit();
				 this.gridOptions.suppressHorizontalScroll=true;
			}
		};
		this.defaultColDef = {editable: false};
		this.getRowNodeId = function (data) {
			return data.id;
		};
		this.columnDefs = [
			{headerName: "ConfirmationDate", field: "ConfirmationDate", pinned: 'left',width: 90},
			{headerName: "BrokerConsultantCompany", field: "BrokerConsultantCompany", pinned: 'left',width: 90},
			{headerName: "BrokerConsultantOffice", field: "BrokerConsultantOffice", pinned: 'left',width: 90},
			{headerName: "BrokerConsultantName", field: "BrokerConsultantName", pinned: 'left',width: 90},
			{headerName: "Entity", field: "Entity", pinned: 'left',width: 90},
			{headerName: "Buyer", field: "Buyer", pinned: 'left', hide : true,width: 90},
			{headerName: "Status", field: "Status", pinned: 'left',hide : true,width: 90},
			{headerName: "ReferenceNumber", field: "ReferenceNumber", pinned: 'left',hide : true,width: 90},
			{headerName: "FFSRatePerUnit", field: "FFSRatePerUnit", pinned: 'left',hide : true,width: 90},
			{headerName: "ContractQuantity", field: "ContractQuantity", pinned: 'left',hide : true,width: 90},
			{headerName: "ConfirmationRevenue", field: "ConfirmationRevenue", pinned: 'left',hide : true,width: 90},
			{headerName: "Commodity", field: "Commodity", pinned: 'left',hide : true,width: 90},
			{headerName: "UserStatus", field: "UserStatus", pinned: 'left',width: 90},
			{headerName: "AgnalysisUserType", field: "AgnalysisUserType", pinned: 'left',hide : true,width: 90},
			{headerName: "NumberOfContacts", field: "NumberOfContacts", pinned: 'left',hide : true,width: 90}
		];
		this.rowData = [];
	}

	ngOnInit() {
		this.getDetailedRevenue();
	}

	onGridReady(params) {
		this.gridApi = params.api;
		this.gridColumnApi = params.columnApi;
	}

	selectAllRows() {
		this.gridOptions.api.selectAll();
	}


	getDetailedRevenue() {
		// https://129.154.71.56:9074/SalesAndM2MPosition/SalesPositionPS/api/v1/GetSalesPositionDetailView?U_ID=279
		// https://agnalysis.io:9074/GetM2MPosition/M2MPosition/api/v1/GetM2MPosition?user_id=261&farming_region_id=142
		let url = serviceUrl + 'SuperadminDashboardReportsSB/SuperadminDashboardReportsRestPS/api/v1/GetDetailedRevenue?Entity_Name=' + this.currentUser.USER.U_TRADING_NAME;
		this.http.get(url).subscribe((data) => {
			if (data.json() != null) {
				this.rowData = data.json() ? data.json().RevenueDetails : [];
			} else {
				this.rowData = [];
			}
		})
	}

}
