import {Component, OnInit, AfterViewInit, NgZone} from '@angular/core';
import {AmChartsService} from "@amcharts/amcharts3-angular";
import {Router} from '@angular/router';
import {UserRoleService} from '../user-role.service'
import {Http, Headers, RequestOptions} from '@angular/http';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import {serviceUrl} from "../app.module";

declare var $: any;


@Component({
	selector: 'app-dashboard-2',
	templateUrl: './dashboard-2.component.html',
	styleUrls: ['./dashboard-2.component.scss']
})
export class Dashboard2Component implements OnInit, AfterViewInit {
	url: string;
	Users: any;
	currentUser: any;
	headers: Headers;
	options: RequestOptions;
	private timer: any;
	private chart: any;
	private chart1: any;
	private chart2: any;
	private chart3: any;
	private chart4: any;
	private chart5: any;

	showApproved: Array<boolean>;
	showRejected: Array<boolean>;
	widgetForSuperAdmin: boolean;


	constructor(private AmCharts: AmChartsService, private router: Router, private role: UserRoleService, private _ngZone: NgZone, private http: Http, private spinnerService: Ng4LoadingSpinnerService) {
		this.widgetForSuperAdmin = false;
		this.showApproved = [];
		this.showRejected = [];
		if (this.role.getRole() === "SUPERADMIN") {
			this.widgetForSuperAdmin = true;
		} else {
			this.widgetForSuperAdmin = false;
		}
		this.currentUser = this.role.getUser();
		this.getproviderForAddProducerData();
	}


	ngAfterViewInit() {
	}

	approve(user, status, i) {
		let userObj = {
			"USER": {
				"U_ID": user.uId,
				"U_TYPE": user.uType,
				"U_ACTION": status,
				"U_USERNAME": user.uUsername
			}
		}
		this.headers = new Headers({
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
			'Accept': 'application/json'
		});
		this.options = new RequestOptions({headers: this.headers});
		this.url = serviceUrl + "AccountGateWaySignup/AccountGatewaySignup/doSignUp";
		this.spinnerService.show();
		this.http.post(this.url, $.param(userObj.USER), this.options).subscribe((data) => {

			console.log(data.json());
			if (data.json().Result.Outcome == "Success") {
				this.showApproved[i] = true;
				this.showRejected[i] = false;
			} else if (data.json().Result.Outcome == "Your Application is Rejected") {
				this.showRejected[i] = true;
				this.showApproved[i] = false;
			}
			this.spinnerService.hide();
		})
	}

	fetchTaskForAdmin() {
		this.url = serviceUrl + "AccountGatewayFetchTasks/AccountGatewayFetchTasks/doFetchTasks";
		this.headers = new Headers({
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
			'Accept': 'application/json'
		});
		this.options = new RequestOptions({headers: this.headers});
		this.http.post(this.url, $.param({"getInactive": "true"}), this.options).subscribe((data) => {

			console.log(data.json());
			if (data.json().AgvUser.length >= 1) {
				this.Users = data.json().AgvUser;
			}
			// this.Users = data.json().AgvUser;
		})
	}

	ngOnInit() {
		if (this.role.getRole() === "SUPERADMIN") {
			this.fetchTaskForAdmin();
		}
	}

//call for get All graph data for Producer dashboard




	providerForAddProducerData: any;
	getproviderForAddProducerData() {
		let url = serviceUrl + "ProducerDashboard/ProducerDashboardPS/api/v1/GetProducerDashboard?user_id=" + this.currentUser.USER.U_ID;
		this.http.get(url).subscribe((data) => {
				this.providerForAddProducerData = data.json();
				console.log("this.providerForAddProducerData", this.providerForAddProducerData);
			},
			(err) => {
				console.log(err.json());
			}
		)
	}

	viewMore(url) {
		this._ngZone.run(() => this.router.navigate([url]));
		// window.location.reload()
	}






}
