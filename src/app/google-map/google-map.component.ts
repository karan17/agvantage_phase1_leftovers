import { Component, ViewChild, ElementRef, NgModule, NgZone, OnInit } from '@angular/core';
import { FormControl, FormsModule, ReactiveFormsModule } from "@angular/forms";
import { AgmCoreModule, MapsAPILoader } from '@agm/core';
import  {Router} from '@angular/router';
// import { google } from '@agm/core/services/google-maps-types';
declare var google;

@Component({
  selector: 'app-google-map',
  templateUrl: './google-map.component.html',
  styleUrls: ['./google-map.component.scss']
})
export class GoogleMapComponent implements OnInit {

  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;
  map;
  service;
  infoWindow;
  sourceBox; destBox;

  @ViewChild("search")
  public searchElementRef: ElementRef;
  
  @ViewChild("sourceSearch")
  public sourceSearchElementRef: ElementRef;
  
  @ViewChild("destiSearch")
  public destiSearchElementRef: ElementRef;

  
  @ViewChild("mapDiv")
  public mapDiv: ElementRef;

   constructor(
    // private mapsAPILoader: MapsAPILoader,
    private _ngZone: NgZone, private router:Router
  ) { }

  ngOnInit() {
    //set google maps defaults
    this.zoom = 4;
    //create search FormControl
    this.searchControl = new FormControl();
    this.infoWindow = new google.maps.InfoWindow;
    //set current position
    this.setCurrentPosition();
    this.inputSource = this.sourceSearchElementRef.nativeElement;
    this.inputDest = this.destiSearchElementRef.nativeElement;
    this.sourceBox = new google.maps.places.SearchBox(this.inputSource);
    this.destBox = new google.maps.places.SearchBox(this.inputDest);
  }

  gotoHome(){
    this._ngZone.run(() => this.router.navigate(['dashboard']));
  }

  /* Show current position */
  private setCurrentPosition() {

    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        console.log("setCurrentPosition ", position);
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
        // google.maps.event.addListener(window, 'load', this.initialize());
        this.initializeAutocomplete();
      });
    }
  }

  initializeAutocomplete() {

    let latitude, longitude;
    console.log('--- lat', this.latitude, '--- long', this.longitude);
    let currentPosition = new google.maps.LatLng(this.latitude, this.longitude);
    var map = new google.maps.Map(document.getElementById('map'), {
      center: currentPosition,
      zoom: 13,
      mapTypeId: 'roadmap'
    });

    // Create the search box and link it to the UI element.
    var input = this.searchElementRef.nativeElement;
    var searchBox = new google.maps.places.SearchBox(input);
    map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });

    var markers = [];
    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function() {
      var places = searchBox.getPlaces();

      if (places.length == 0) {
        return;
      }

      // Clear out the old markers.
      markers.forEach(function(marker) {
        marker.setMap(null);
      });
      markers = [];

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      places.forEach(function(place) {
        if (!place.geometry) {
          console.log("Returned place contains no geometry");
          return;
        }
        var icon = {
          url: place.icon,
          size: new google.maps.Size(71, 71),
          origin: new google.maps.Point(0, 0),
          anchor: new google.maps.Point(17, 34),
          scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        markers.push(new google.maps.Marker({
          map: map,
          icon: icon,
          title: place.name,
          position: place.geometry.location
        }));

        if (place.geometry.viewport) {
          // Only geocodes have viewport.
          bounds.union(place.geometry.viewport);
        } else {
          bounds.extend(place.geometry.location);
        }
      });
      map.fitBounds(bounds);
    });
  }

  inputSource; inputDest;
  srcsrch; destsrch;
  dist:any;duration:any;
  
 initMap() {
  var directionsService = new google.maps.DirectionsService;
  
  // let src = document.getElementById('source-input');
  // let dest = document.getElementById('destination-input');
  var src = this.sourceBox.getPlaces();
  var dest = this.destBox.getPlaces();
  console.log('source ', src[0].formatted_address);
  console.log('Destination ', dest[0].formatted_address);

  var bounds = new google.maps.LatLngBounds;
  var markersArray = [];
  // this.initializeAutocomplete(input, map);
  var origin1 = src[0].formatted_address;//'Pune, Maharashtra, India'; //{lat: 55.93, lng: -3.118};
  // var origin2 = 'Greenwich, England';
  var destinationA = dest[0].formatted_address;//'Delhi, India';
  // var destinationB = {lat: 50.087, lng: 14.421};

  var destinationIcon = 'https://chart.googleapis.com/chart?' +
      'chst=d_map_pin_letter&chld=D|FF0000|000000';
  var originIcon = 'https://chart.googleapis.com/chart?' +
      'chst=d_map_pin_letter&chld=O|FFFF00|000000';
  let currentPosition = new google.maps.LatLng(this.latitude, this.longitude);
  
  var distcalcmap = new google.maps.Map(document.getElementById('distancemap'), {
    center: currentPosition,//{lat: 55.53, lng: 9.4},
    zoom: 13,
    mapTypeId: 'roadmap'
  });

  var directionsDisplay = new google.maps.DirectionsRenderer({map: distcalcmap});

  var geocoder = new google.maps.Geocoder;

  var service = new google.maps.DistanceMatrixService;
  service.getDistanceMatrix({
    // origins: [origin1, origin2],
    origins: [origin1],
    // destinations: [destinationA, destinationB],
    destinations: [destinationA],
    travelMode: 'DRIVING',
    unitSystem: google.maps.UnitSystem.METRIC,
    avoidHighways: false,
    avoidTolls: false
  }, function(response, status) {
    if (status !== 'OK') {
      alert('Error was: ' + status);
    } else {
      var originList = response.originAddresses;
      var destinationList = response.destinationAddresses;
      var outputDiv = document.getElementById('output');
      outputDiv.innerHTML = '';
      // this.deleteMarkers(markersArray);
      for (var i = 0; i < markersArray.length; i++) {
        markersArray[i].setMap(null);
      }
      markersArray = [];

      var showGeocodedAddressOnMap = function(asDestination) {
        var icon = asDestination ? destinationIcon : originIcon;
        return function(results, status) {
          if (status === 'OK') {
            this.distcalcmap.fitBounds(bounds.extend(results[0].geometry.location));
            markersArray.push(new google.maps.Marker({
              map: this.distcalcmap,
              position: results[0].geometry.location,
              icon: icon
            }));
          } else {
            alert('Geocode was not successful due to: ' + status);
          }
        };
      };

      for (var i = 0; i < originList.length; i++) {
        var results = response.rows[i].elements;
        geocoder.geocode({'address': originList[i]},
            showGeocodedAddressOnMap(false));
        for (var j = 0; j < results.length; j++) {
          geocoder.geocode({'address': destinationList[j]},
              showGeocodedAddressOnMap(true));
              // var dist = document.getElementById('dist').innerHTML;
              // dist = results[j].distance.text;
              // var dur = document.getElementById('duration').innerHTML;
              // dur = results[j].duration.text;
              // this.dist = results[j].distance.text;
              // this.duration = results[j].duration.text;
          outputDiv.innerHTML += originList[i] + ' to ' + destinationList[j] +
              ' <b> Distance: ' + results[j].distance.text + '</b> in <b>' +
              results[j].duration.text + '</b> <br>';
        }
      }
    }
  });

  // Retrieve the start and end locations and create a DirectionsRequest using
  // WALKING directions.
  directionsService.route({
    origin: origin1, //document.getElementById('start').value,
    destination: destinationA,//document.getElementById('end').value,
    travelMode: 'DRIVING'
  }, function(response, status) {
    // Route the directions and pass the response to a function to create
    // markers for each step.
    if (status === 'OK') {
      // document.getElementById('warnings-panel').innerHTML =
      //     '<b>' + response.routes[0].warnings + '</b>';
      directionsDisplay.setDirections(response);
      // showSteps(response, markerArray, stepDisplay, map);
    } else {
      window.alert('Directions request failed due to ' + status);
    }
  });
}

deleteMarkers(markersArray) {
  for (var i = 0; i < markersArray.length; i++) {
    markersArray[i].setMap(null);
  }
  markersArray = [];
}


}
