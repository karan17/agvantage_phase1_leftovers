import { Component, OnInit, NgZone } from '@angular/core';
import { UserRoleService } from '../user-role.service';
import { Router } from '@angular/router';


@Component({
    selector: 'app-top-navbar',
    templateUrl: './top-navbar.component.html',
    styleUrls: ['./top-navbar.component.scss']
})
export class TopNavbarComponent implements OnInit {
    widgetSuperAdmin: any;
    currentUser: any;
    constructor(private role: UserRoleService, private router: Router, private _ngZone: NgZone) {
        this.currentUser = this.role.getUser();
        localStorage.setItem('user',this.currentUser);
        // if(this.currentUser == 'undefined'){
        //     this.currentUser = localStorage.getItem('user');
        // }
        console.log(this.currentUser);
        if (this.role.getRole() === "SUPERADMIN") {
            this.widgetSuperAdmin = true;
        } else {
            this.widgetSuperAdmin = false;
        }
    }

    ngOnInit() {
    }

    stopPro(event) {
        event.stopPropagation();
        return false;
    }
    gotoPC() {
        this._ngZone.run(() => this.router.navigate(['viewproductiongrid']));
    }

    gotoPCB() {
        this._ngZone.run(() => this.router.navigate(['viewpcbgrid']));
    }

    gotoDocuments() {
        this._ngZone.run(() => this.router.navigate(['documentsgrid']));
    }

    gotoSuperadmin() {
        this._ngZone.run(() => this.router.navigate(['Superadmin']));
    }

    gotoMS() {
        this._ngZone.run(() => this.router.navigate(['viewmarketgrid']));
    }
    gotoSI() {
        this._ngZone.run(() => this.router.navigate(['viewstoragegrid']));
    }

    gotoCF() {
        this._ngZone.run(() => this.router.navigate(['contacts']));
    }

    gotoAQ() {
        // if (this.role.getRole() === "superadmin"){
            // this.showQualitySubMenu = true;
            this._ngZone.run(() => this.router.navigate(['addquality']));
        // }else{
            // this.showQualitySubMenu = false;
        // }
    }

    gotoAC() {
         if (this.role.getRole() === "SUPERADMIN") {
             this._ngZone.run(() => this.router.navigate(['addcommodityby']));
        } else {
             this._ngZone.run(() => this.router.navigate(['addcommodity']));
        }
    }

    gotoBEF() {
        this._ngZone.run(() => this.router.navigate(['breakEvenForcast']));
    }

    gotoFC(){
        this._ngZone.run(() => this.router.navigate(['freightCalculator']));
    }

    gotoLogin() {
        this._ngZone.run(() => this.router.navigate(['login']));
        this.role.setUser(null);
    }
    goToSettingPage() {
        this._ngZone.run(() => this.router.navigate(['pages-profile']));
    }
    
    gotoMap() {
        this._ngZone.run(() => this.router.navigate(['direction']));
    }

    gotoADG(){
        if (this.role.getRole() === "SUPERADMIN") {
            this._ngZone.run(() => this.router.navigate(['accountDetailsGrid']));
        }
    }

    gotoSM2MPos(){
        // if (this.role.getRole() === "superadmin") {
            this._ngZone.run(() => this.router.navigate(['SalesAndM2mPositionComponent']));
        // }
    }
   
    gotoSalesPurchase(){
        // if (this.role.getRole() === "superadmin") {
            this._ngZone.run(() => this.router.navigate(['SalesPurchaseComponent']));
        // }
    }
    
    gotoFavourites(){
        // if (this.role.getRole() === "superadmin") {
            this._ngZone.run(() => this.router.navigate(['favourites']));
        // }
    }
    
    gotoProfMatrix(){
        // if (this.role.getRole() === "superadmin") {
            this._ngZone.run(() => this.router.navigate(['profMatrix']));
        // }
    }
    gotoPSF(){
        this._ngZone.run(() => this.router.navigate(['prodsaleforecast']));
    }
    
    gotoConOnFarm(){
        // if (this.role.getRole() === "superadmin") {
            this._ngZone.run(() => this.router.navigate(['conOnFarm']));
        // }
    }

    gotoAddNewFields(){
		if (this.role.getRole() === "SUPERADMIN") {
			this._ngZone.run(() => this.router.navigate(['addNewFields']));
		}
	}

}
