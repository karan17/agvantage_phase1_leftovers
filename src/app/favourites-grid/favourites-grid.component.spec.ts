import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FavouritesGridComponent } from './favourites-grid.component';

describe('FavouritesGridComponent', () => {
  let component: FavouritesGridComponent;
  let fixture: ComponentFixture<FavouritesGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FavouritesGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FavouritesGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
