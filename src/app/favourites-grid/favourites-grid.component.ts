import { Component, OnInit, Output } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import { serviceUrl } from '../app.module';
import { UserRoleService } from '../user-role.service';
import {Http, Headers, RequestOptions} from '@angular/http';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { EditDeleteRendererComponent } from '../edit-delete-renderer/edit-delete-renderer.component';
import { CommodityUpdateCellRendererComponent } from '../commodity-update-cell-renderer/commodity-update-cell-renderer.component';
import { EventEmitter } from '@angular/core';
import {Broadcaster} from '../broadcaster';

@Component({
  selector: 'app-favourites-grid',
  templateUrl: './favourites-grid.component.html',
  styleUrls: ['./favourites-grid.component.scss']
})
export class FavouritesGridComponent implements OnInit {

  saveMessage: any;
  gridOptions: GridOptions;
  gridApi;
  gridColumnApi;
  frameworkComponents:any;
  columnDefs;
  context:any;
  rowData;
  defaultColDef;
  getRowNodeId;

  constructor(private http: Http, public spinnerService: Ng4LoadingSpinnerService,
    private broadCaster: Broadcaster, private user : UserRoleService) {
    this.gridOptions = <GridOptions>{
      onGridReady: () => {
        // this.gridOptions.columnApi.autoSizeAllColumns();
        this.gridOptions.api.sizeColumnsToFit();
      }
    };
    this.broadCaster.on<any>('Save Favorite Event')
            .subscribe(res => {
                 this.getFavorites();
                this.saveMessage = res;
                setTimeout(()=>{this.saveMessage = false},4000);
            });
    this.broadCaster.on<any>('Update Favorite Event')
    .subscribe(res => {
          this.getFavorites();
        this.saveMessage = res;
        setTimeout(()=>{this.saveMessage = false},4000);
    });
    this.broadCaster.on<any>('Delete Favorite Event')
    .subscribe(res => {
        this.getFavorites();
        this.saveMessage = res;
        setTimeout(()=>{this.saveMessage = false},4000);
    });
    this.frameworkComponents = {
      'editdeletecellrenderer': EditDeleteRendererComponent
    }
    this.context = { componentParent: this };
    this.columnDefs = [
      {
        headerName: "Actions", pinned: 'left', width: 180,
        cellRenderer: 'editdeletecellrenderer'
      },
      { headerName: "Site/Location",field: "Site_name", pinned: 'left' },
      { headerName: "Commodity", field: "Commodity_name", pinned: 'left' },
      { headerName: "Crop Year", field: "Crop_year", pinned: 'left' },
      { headerName: "Grade", field: "Grade", pinned: 'left' },
      { headerName: "Price", field: "Price",pinned: 'left' },
      
      { headerName: "Target ROI (in %)", field: "Target_ROI", pinned: 'left' }
    ];
    this.rowData = [];
  
    
  
  
    this.getRowNodeId = function (data) {
      return data.id;
    };
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.getFavorites();
  }

  getFavorites(){
    let url= serviceUrl + "AgvEntityGetFavouritesSB/AgvEntityGetFavouritesRestPS/api/v1/GetFavourites?User_id="+this.user.getUser().USER.U_ID;
    this.gridApi.showLoadingOverlay();
    this.http.get(url).subscribe((data) =>{
      this.spinnerService.hide();
      this.rowData =data.json()?data.json().Favourite:[];
    },
    (err)=>{
      this.rowData=[];
    });
  }
  @Output() openEditDialog = new EventEmitter();
  methodFromParent(params){
    console.log("from grid to componenet----",params.data);
    this.openEditDialog.emit(params.data);
  }
  @Output() openDeleteDialog = new EventEmitter();
  methodFromParentDelete(params) {
    //console.log(this.gridOptions.api.getSelectedRows());
    this.openDeleteDialog.emit(params.data);
  } 

  ngOnInit() {
  }

  

  // this.broadCaster.on<string>('Save Event')
  //   .subscribe(message => {
  //     this.gridOptions.api.showLoadingOverlay()
  //     this.getGridData();
  //     this.saveMessage=message;
  //   });
  //      this.broadCaster.on<string>('Update Event')
  //   .subscribe(message => {
  //     this.gridOptions.api.showLoadingOverlay()
  //     this.getGridData();
  //     this.saveMessage=message;
  //   });
      
}
