import { Component, OnInit, NgZone, ViewChild } from '@angular/core';
import { UserRoleService } from '../user-role.service';
import { Router } from '@angular/router';
import { serviceUrl } from "../app.module";
import { Http, Headers, RequestOptions } from '@angular/http';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { GridOptions } from "ag-grid/main";
import { element } from '../../../node_modules/protractor';

@Component({
  selector: 'app-sales-and-purchase',
  templateUrl: './sales-and-purchase.component.html',
  styleUrls: ['./sales-and-purchase.component.scss']
})
export class SalesAndPurchaseComponent implements OnInit {

  EntityName: any;
  NGR: any;
  currentUser;
  salesPurchase;
  colDefViewSalesPurchaseGrid: any[];
  getRowNodeId;
  headers: Headers;
  options: RequestOptions;
  addSalesPurchasemessage;
  gridOptionsSales : GridOptions;
  defaultColDef;
  rowSelection: string;
  columnDefsNTPDetails;
  gridApiTruck: any;
  gridColumnApiTruck: any;
  gridOptionsTruck :GridOptions;

  @ViewChild('salesAndPurchaseModal') public salesAndPurchaseModal;
  @ViewChild('salesAndPurchaseModalForm') public salesAndPurchaseModalForm;
  @ViewChild('editSalesAndPurchaseModal') public editSalesAndPurchaseModal;
  @ViewChild('editSalesAndPurchaseModalForm') public editSalesAndPurchaseModalForm;

  constructor( private role: UserRoleService, private _ngZone: NgZone, private router: Router, 
    private http: Http, public spinnerService: Ng4LoadingSpinnerService) { 
    this.currentUser = this.role.getUser();
    this.EntityName = this.currentUser.USER.U_TRADING_NAME;
    this.NGR = this.currentUser.USER.U_NGR;
    this.salesPurchase={};
    this.rowSelection = "single";    
    this.gridOptionsTruck = <GridOptions>{
        onGridReadyTruck: () => {
           //this.setTruckGrid();
            
        }
      };

  this.colDefViewSalesPurchaseGrid = [
    { headerName: "Select", checkboxSelection: true, headerCheckboxSelection: true,
     field: "actions", width: 50,  editable: true },
     { 
      headerName: "", field: "", onCellClicked: (params) => {
        this.editSalesPurchase(params);
      },
      cellRenderer: (params) => `<a>Edit</a>`, cellStyle: (params) => {
        return { cursor: 'pointer', color: 'blue' }
      }
    } , 
    { 
      headerName: "Confirmation Completed By", field: "ConfirmationDoneBy"},
      { headerName: "FarmingRegion", field: "FarmingRegion" } ,
      { headerName: "Inventory Location", field: "InventoryLocation" }, 
      { headerName: "ReferenceNo", field: "ReferenceNo" },
      { headerName: "Seller", field: "Seller" },
      { headerName: "SaleType", field: "SaleType" },
      { headerName: "Buyer", field: "Buyer" },
      { headerName: "Commodity", field: "Commodity" },
      { headerName: "ContractNo", field: "ContractNo" }, 
      { headerName: "CropYear", field: "CropYear" } ,
      { headerName: "Grade", field: "Grade" }, 
   
   
      { headerName: "Quantity", field: "ContractQuantity" } ,
      { headerName: "ContractPrice", field: "ContractPrice" } ,
      { headerName: "Delivery Period Start", field: "DeliveryPeriodFrom" } ,
      { headerName: "Delivery Period End", field: "DeliveryPeriodTo" } ,
      { headerName: "Transfer To Be Done By", field: "TransferDoneBy" } ,
      { headerName: "Record Manager", field: "RecordManager" } ,
      { headerName: "Broker Company Name", field: "BrokerCompanyName" } ,

    { 
      headerName: "Broker Office", field: "BrokerOffice"} ,
    
    { headerName: "Contract Document", field: "ContractDocument" },    
    
   
   
   
   
   
   
  ];

  this.columnDefsNTPDetails = [
    { headerName: "NTP Name",  field: "NTPName"},
    { headerName: "Nominated Sites", field: "NomSites" },
    { headerName: "Location Differential", field: "LocDiff" },
    { headerName: "Site Price", field: "SitePrice" }
  ]

  
  }

  ffsInvoice;
  ngOnInit() {
    this.salesPurchase.contractPrice = 0;
    this.salesPurchase.sellerTradingName = this.currentUser.USER.U_TRADING_NAME;
    this.callSellerRepresents();
    this.getSalesPurchaseGridData();
    this.getSalesType();
    this.getSalesOriginDest();
    //this.getSalesAndPurchaseRefDataOriginDest();
    this.getCommodities();
    this.getCropyearList();
    this.getIncrement();
    this.getStatus();
    this.getBuyerTradingName();
    this.getSellerTradingName();
    this.getBrokerOffice();
   
    
    if(this.currentUser.USER.UserInRole === "BUYER"){
      this.salesPurchase.FFS = 2.5;
    }
    this.ffsInvoice = [
      {id:1, name:"Seller"},
      {id:2, name:"Buyer"}
    ]

    this.getNTPSiteList();
  }

  gotoHome() {
    this._ngZone.run(() => this.router.navigate(['dashboard']));
  }

  openAddSalesPurchase(){
    this.salesAndPurchaseModal.show();
  }
  
  closeAddSalesPurchase(){
    this.salesAndPurchaseModal.hide();
  }

  onGridReadySH(params) {
    this.getRowNodeId = function (data) {
      return data.id;
    };
  }

  showNTPDetails = false;
  saleOriginChange(){
    this.getCommodityOrigin();
    this.getCommodityDestination();
    this.getFarmingRegions();
    console.log("saleOrigin selection...");
    
    // this.showCommDestination();
  }

  callSellerRepresents(){
    this.getSellerRepresentative();
  }

  onChangeToDate($event){
    this.salesPurchase.finalDelvDate=$event;
  }

  // locDiff;
  getLocDiff(){   
    
    this.spinnerService.show();
    let url = `${serviceUrl}GetLocationDifferential/GetLocationDifferentialPS/api/v1/GetLocationDifferential?NTPName=`+this.salesPurchase.ntpNomSiteList.NTP_Name+`&NominatedSite=`+this.salesPurchase.ntpNomSiteList.NominatedSite_Name;
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.salesPurchase.locDiff = data.json().Differential;
    })
  }

  cropYearList;
  getCropyearList() {
    this.spinnerService.show();
    let url = serviceUrl + 'AgvGetCropYear/GetCropYear/api/v1/getCropYear?getcropyer=true';
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.cropYearList = data.json().CropYear;
    })
  }

  commodityDestination;
  getCommodityDestination() { 
        
    let url = `${serviceUrl}AgvantageGetSitesSB/AgvantageGetSitesRestPS/api/v1/GetSitesList?storage_type=${this.salesPurchase.saleOrigin.Name}&state_id=${this.salesPurchase.sellerRepresentative.StateId}`;
    this.spinnerService.show();
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();      
      this.commodityDestination = data.json() ? data.json().getSites : [];      
    },
      (err) => {
        this.commodityDestination = [];
      });
  }

  commodityOriginList;
  getCommodityOrigin() {
    let user_id = this.currentUser.USER.U_A_REGION;
    // https://129.154.71.56:9074/AgvantageGetCommodityOriginSB/AgvantageGetCommodityOriginRestPS/api/v1/GetCommodityOrigin?seller_user_id=1
    // let url = `${serviceUrl}AgvantageGetSitesSB/AgvantageGetSitesRestPS/api/v1/GetSitesList?user_id=${this.currentUser.USER.U_ID}&storage_type=${this.salesPurchase.saleOrigin.Name}&state_id=${this.currentUser.USER.U_A_STATE}`;
    let url = `${serviceUrl}AgvantageGetCommodityOriginSB/AgvantageGetCommodityOriginRestPS/api/v1/GetCommodityOrigin?seller_user_id=${this.salesPurchase.sellerRepresentative.UserId}`;
    this.spinnerService.show();
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      console.log("res", data);
      this.commodityOriginList = data.json() ? data.json().Storage.Storage_Name : [];
    },
      (err) => {
        this.commodityOriginList = [];
      });
  }

  brokerOffices;
  getBrokerOffice() {
    let url = serviceUrl + "AgvantageGetBrokerOfficeSB/AgvantageGetBrokerOfficeRestPS/api/v1/GetBrokerOffice?broker_name=Agvantage Commodities Pty Ltd";
    this.http.get(url).subscribe((data) => {
        this.brokerOffices = data.json() ? data.json().broker_offices.broker_office : [];
    }),
        (err) => {
            this.brokerOffices = [];
        }
}

  statusList;
  getStatus(){    
    this.statusList = [
      { key: 'Open', value: 'Open' },
      { key: 'Confirmed', value: 'Confirmed' },
      { key: 'Washed Out - No Confirmation Revenue', value: 'Washed Out - No Confirmation Revenue' },
      { key: 'Washed Out - Keep Confirmation Revenue', value: 'Washed Out - Keep Confirmation Revenue' }
    ];
    this.salesPurchase.status=this.statusList[0].key='Open';
  }
  
  incrList;
  getIncrement(){    
    this.incrList = [
      { key: 'Nil - Flat Priced', value: 'Nil - Flat Priced' },
      { key: 'As per contract conditions', value: 'As per contract conditions' },
      { key: 'As per AOF oil & admix bonifications', value: 'As per AOF oil & admix bonifications' }      
    ];    
  }

  buyerTradingNames: any;
  sellerTradingNames: any;
  sellerRepresentatives: any;
  getBuyerTradingName(){
    let url = `${serviceUrl}GetRegisteredCompaniesSB/GetRegisteredCompaniesPS/api/v1/GetRegisteredCompanies?CompanyType=Buyers`;
    this.http.get(url).subscribe(data=>{
      this.buyerTradingNames = data.json()?data.json().CompanyTradingName : [];
    },err=>{
      this.buyerTradingNames = [];
    })
  }
  getSellerTradingName(){
    let url = `${serviceUrl}GetRegisteredCompaniesSB/GetRegisteredCompaniesPS/api/v1/GetRegisteredCompanies?CompanyType=Sellers`;
    this.http.get(url).subscribe(data=>{
      this.sellerTradingNames = data.json()?data.json().CompanyTradingName : [];
    },err=>{
      this.sellerTradingNames = [];
    })
  }
  getSellerRepresentative(){
    let url = `${serviceUrl}GetCompanyMembers/GetCompanyMembersPS/api/v1/GetCompanyMembers?CompanyName=${this.salesPurchase.sellerTradingName}`;
    this.http.get(url).subscribe(data=>{
      this.sellerRepresentatives = data.json()?data.json().Member : [];
    },err=>{
      this.sellerRepresentatives = [];
    })
  }

  onSelectionSale(param) {
    if (param.node.selected) {
      // this.enableNext = false;
      // this.holdContractNo = param.data.ContractNo;
      // this.addCommMovmtRec.Buyer = param.data.BuyerName;
      // this.addCommMovmtRec.CommCollected = param.data.Commodity;
      // this.addCommMovmtRec.Grade = param.data.Grade;
    }
  }

  farmingRegions;
  getFarmingRegions() {
  //   this.spinnerService.show();
  //   let url = serviceUrl + "AgvantageGetFarmingRegionsSB/AgvantageGetFarmingRegionsRestPS/api/v1/GetFarmingRegions?state_id=" + this.addDestinationData.State + "&pz_id=" + this.addDestinationData.MarketZone;
  //   this.http.get(url).subscribe((data) => {
  //     this.spinnerService.hide();
      this.farmingRegions = [{FarmingRegion_Name : this.salesPurchase.sellerRepresentative.FarmingRegionId}];
      // this.farmingRegions = data.json()?data.json().getFarmingRegions:[];
  //   })
      this.salesPurchase.farmRegion = this.farmingRegions[0].FarmingRegion_Name;
  }

  salesTypeList;
  getSalesType() {
    let url = serviceUrl + "SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSalesType?Request=GetSalesType"
    console.log();
    this.spinnerService.show();
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      console.log("res", data);
      this.salesTypeList = data.json() ? data.json().SaleType : [];
    },
      (err) => {
        this.salesTypeList = [];
      });
  }

  salesOriginDestList;
  getSalesOriginDest() {
    let url = serviceUrl + "SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSaleOriginDestinations?Request=GetSaleOrigin"
    console.log();
    this.spinnerService.show();
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      console.log("res", data);
      this.salesOriginDestList = data.json() ? data.json().SiteOrigin : [];
    },
      (err) => {
        this.salesOriginDestList = [];
      });
  }

  commodities;
  getCommodities() {
    this.spinnerService.show();
    let url = serviceUrl + "AgVantageGetCommoditiesSB/getCommodities/api/v1/getCommodity?getCommodities=true";
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.commodities = data.json().CommodityCollection.Commodity;
    })
  }

  
  callGrades() {
    this.getCommodityGrade(this.salesPurchase.commodity);
    let refDataurl = serviceUrl + "SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSalesAndPurchaseReferenceData?SaleType=" + this.salesPurchase.SaleType.Name + "&SiteOrigin=" + this.salesPurchase.saleOrigin.Id + "&Commodity=" + this.salesPurchase.commodity
    this.getSalesAndPurchaseRefDataOriginDest(refDataurl);
  }

  grades;
  getCommodityGrade(id) {
    this.spinnerService.show();
    let url = serviceUrl + "AgVantageGetCommodityGrade/AgVantageGetGradePS/api/v1/getGrade?C_ID=" + id;
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.grades = data.json().gradeCollection.grade;
    })
  }

  salesPurchaseRefDataList;
  tolaranceRefDataList = [];
  tradeDisputeRuleRefDataList = [];
  transferredConfirmedByRefDataList = [];
  transferDoneByRefDataList = [];
  weightTermsRefData = [];
  rCTIRefDataList = [];
  paymentTermsRefData = [];
  gTAContractRefData = [];
  gradeStandardsRefData = [];
  gSTRefData = [];
  deliveryTermsRefData = [];
  arbitrationRuleRefData = [];
  contractTypeRefData = [];
  businessCreditRefData: any;

  getSalesAndPurchaseRefDataOriginDest(url) {
    // http://129.154.71.56:9073/SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSalesAndPurchaseReferenceData?SaleType=1&SiteOrigin=4&Commodity=Cotton 
    // let url = serviceUrl + "SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSalesAndPurchaseReferenceData?SaleType=" + this.salesPurchase.SaleType.Name + "&SiteOrigin=" + this.salesPurchase.saleOrigin.Id + "&Commodity=" + this.salesPurchase.commodity
    // console.log();
    this.spinnerService.show();
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      console.log("res", data);
      this.salesPurchaseRefDataList = data.json() ? data.json() : [];
      this.businessCreditRefData = this.salesPurchaseRefDataList.BusinessCreditedRefData ? this.salesPurchaseRefDataList.BusinessCreditedRefData.RefData : [];
      this.tolaranceRefDataList = this.salesPurchaseRefDataList.ToleranceRefData ? this.salesPurchaseRefDataList.ToleranceRefData.RefData : [];
      this.tradeDisputeRuleRefDataList = this.salesPurchaseRefDataList.TradeDisputeRuleRefData ? this.salesPurchaseRefDataList.TradeDisputeRuleRefData.RefData : [];
      this.transferredConfirmedByRefDataList = this.salesPurchaseRefDataList.TransferredConfirmedByRefData ? this.salesPurchaseRefDataList.TransferredConfirmedByRefData.RefData : [];
      this.transferDoneByRefDataList = this.salesPurchaseRefDataList.TransferDoneByRefData ? this.salesPurchaseRefDataList.TransferDoneByRefData.RefData : [];
      this.weightTermsRefData = this.salesPurchaseRefDataList.WeightTermsRefData ? this.salesPurchaseRefDataList.WeightTermsRefData.RefData : [];
      this.rCTIRefDataList = this.salesPurchaseRefDataList.RCTIRefData ? this.salesPurchaseRefDataList.RCTIRefData.RefData : [];
      this.paymentTermsRefData = this.salesPurchaseRefDataList.PaymentTermsRefData ? this.salesPurchaseRefDataList.PaymentTermsRefData.RefData : [];
      this.gTAContractRefData = this.salesPurchaseRefDataList.GTAContractRefData ? this.salesPurchaseRefDataList.GTAContractRefData.RefData : [];
      this.gradeStandardsRefData = this.salesPurchaseRefDataList.GradeStandardsRefData ? this.salesPurchaseRefDataList.GradeStandardsRefData.RefData : [];
      this.gSTRefData = this.salesPurchaseRefDataList.GSTRefData ? this.salesPurchaseRefDataList.GSTRefData.RefData : [];
      this.deliveryTermsRefData = this.salesPurchaseRefDataList.DeliveryTermsRefData ? this.salesPurchaseRefDataList.DeliveryTermsRefData.RefData : [];
      this.salesPurchase.delvTerms = this.deliveryTermsRefData[0].Id;
      this.arbitrationRuleRefData = this.salesPurchaseRefDataList.ArbitrationRuleRefData ? this.salesPurchaseRefDataList.ArbitrationRuleRefData.RefData : [];
      this.contractTypeRefData = this.salesPurchaseRefDataList.ContractTypeRefData ? this.salesPurchaseRefDataList.ContractTypeRefData.RefData : [];
    },
      (err) => {
        this.salesPurchaseRefDataList = [];
      });
  }

  onGridReadyTruck(params) {
    this.gridApiTruck = params.api;
    this.gridColumnApiTruck = params.columnApi;
    
    this.setTruckGrid();
   
  }

  setTruckGrid(){
    this.gridOptionsTruck.api.setRowData([]);
    //setTimeout(()=>{this.gridOptionsTruck.api.sizeColumnsToFit();});
    
  }

  load=0;ntpLoadData =[];NTPName; NomSites;LocDiff;SitePrice;
  addNTPSites() {
    // this.load=this.load+1;
    // NTP Contract Price - Location Differential = Site Price.
    this.salesPurchase.ntpSitePrice = this.salesPurchase.contractPrice - this.salesPurchase.locDiff;
    console.log("--- this.salesPurchase.ntpSitePrice", this.salesPurchase.ntpSitePrice);
    let obj = {
      NTPName: this.salesPurchase.ntpNameList.Site_Name,
      NomSites: this.salesPurchase.ntpNomSiteList.NominatedSite_Name, 
      LocDiff: this.salesPurchase.locDiff, 
      SitePrice: this.salesPurchase.ntpSitePrice
    }
    this.gridApiTruck.updateRowData({ add: [obj] });
    
    this.ntpLoadData.push(obj);
  }

  showCommDestInput=false; showCommDestDDL=true; showTolerance = true;disableCommDest = false;
showCommDestination(){  
  if(this.salesPurchase.saleOrigin.Name === "DelieveredDestination"){    
    this.showCommDestInput = true;
    this.showCommDestDDL = false;
  }else{    
    this.showCommDestInput=false; 
    this.showCommDestDDL=true;
  }

  if(this.salesPurchase.saleOrigin.Name==="NTP" && this.salesPurchase.SaleType.Name==="Forward"){
    this.showNTPDetails = true;
    this.showTolerance = false;
    this.getNTPSiteList();
  }else{
    this.showTolerance = true;
    this.showNTPDetails = false;
  }

  if(this.salesPurchase.saleOrigin.Name === "FOT-GinYard"){
    this.disableCommDest = true;
  }
}

  saleHistoryData;
  getSalesPurchaseGridData(){
        
      let url = `${serviceUrl}SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSalesAndPurchaseHistoryByUserId?U_ID=${this.currentUser.USER.U_ID}&U_Type=${this.currentUser.USER.U_TYPE}`;
      this.http.get(url).subscribe((data) => {
        this.saleHistoryData = data.json() ? data.json().SaleHistory : [];
      },
        (err) => {
          this.saleHistoryData = [];
        });
    
  }
  ntpSiteListData;
  getNTPSiteList(){
    this.spinnerService.show();
    if(this.salesPurchase && this.salesPurchase.saleOrigin.Name){
     
    
    // if(this.salesPurchase.saleOrigin.Name != undefined){
    let url=`${serviceUrl}AgvantageGetSitesSB/AgvantageGetSitesRestPS/api/v1/GetSitesList?state_id=1&storage_type=`+this.salesPurchase.saleOrigin.Name;
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.ntpSiteListData = data.json() ? data.json().getSites : [];
    },
      (err) => {
        this.ntpSiteListData = [];
      });
  }
}
  // getLocDiff()
  ntpNominatedSiteListData;
  getNTPNominatedSiteList(){
    this.spinnerService.show();
    let url=`${serviceUrl}AgvantageGetNTPNominatedSitesSB/AgvantageGetNTPNominatedSitesRestPS/api/v1/GetNominatedSites?ntp_id=`+this.salesPurchase.ntpNameList.Site_ID;
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.ntpNominatedSiteListData = data.json() ? data.json().NominatedSite : [];
    },
      (err) => {
        this.ntpNominatedSiteListData = [];
      });
  }

  submitSalesAndPurchaseMsg;
  submitSalesAndPurchase() {
    let params = {
      "SaleDate": new Date(this.salesPurchase.date),
      "SaleType": this.salesPurchase.SaleType.Id,
      "SaleOrigin": this.salesPurchase.saleOrigin.Id,
      "Commodity": this.salesPurchase.commodity,
      "BuyerName": this.salesPurchase.buyerTradingName,
      "SellerName": this.salesPurchase.sellerTradingName,
      "SellerRepresentative": this.salesPurchase.sellerRepresentative.UserId,
      "BuyerRepresentative": this.salesPurchase.buyersRepresentative,
      "BusinessCreditedTo": this.salesPurchase.businessCreditedTo,
      "OfficeCreditedTo": this.salesPurchase.officeCreditedTo,
      "ProducerBrokerTrader": this.salesPurchase.producer,
      "ReferenceNo": this.salesPurchase.refNumber,
      "FFS": this.salesPurchase.FFS,
      "FFSInvoicedTo": this.salesPurchase.FFSInvoiced,
      "Variety": this.salesPurchase.variety,
      "Grade": this.salesPurchase.grade,
      "CropYear": this.salesPurchase.cropYear,
      "ContractNo": this.salesPurchase.contractNo,
      "ContractType": this.salesPurchase.contractType,
      "Quantity": this.salesPurchase.quantity,
      "ContractPrice": this.salesPurchase.contractPrice,
      "Discounts": this.salesPurchase.discounts,
      "Increments": this.salesPurchase.increment,
      "Area": this.salesPurchase.areahec,
      "MinYield": this.salesPurchase.minyuphec,
      "MaxYield": this.salesPurchase.maxyuphec,
      "MinQuantity": this.salesPurchase.minQuantity,
      "MaxQuantity": this.salesPurchase.maxQuantity,
      "Tolerance": this.salesPurchase.toleranceList,
      "DeliveryTerms": this.salesPurchase.delvTerms,
      "PaymentTerms": this.salesPurchase.payTermsRefData,
      "CommodityOrigin": this.salesPurchase.commodityOrigin,
      "CommodityDestination": this.salesPurchase.commodityDestination,
      "FarmingRegion": this.salesPurchase.farmRegion,
      "AdditionalFreight": this.salesPurchase.addFreight,
      "DeliveryPeriodFrom": new Date (this.salesPurchase.delPeriodFrom),
      "DeliveryPeriodTo": this.salesPurchase.delPeriodTo,
      "WeightTerms": this.salesPurchase.weightTerms,
      "ArbitrationRules": this.salesPurchase.rulesArb,
      "GradeStandards": this.salesPurchase.gradeStd,
      "GTAContractNo": this.salesPurchase.gtaContractNo,
      "GST": this.salesPurchase.gst,
      "BuyerSuppliedRCTI": this.salesPurchase.buyerRCTI,
      "SellerTaxInvoice": this.salesPurchase.sellerRCTI,
      "TransferDoneBy": this.salesPurchase.transtobedoneby,
      "VendorDeclarationReqd": this.salesPurchase.vendorDecl,
      "Mortgage": this.salesPurchase.mortgage,
      "Encumbrance": this.salesPurchase.encumb,
      "PlantBreeders": this.salesPurchase.plactBreedRight,
      "Royalties": "string",
      "SecurityInterest": this.salesPurchase.regUnregSecIntrst,
      "DisclosureComment": this.salesPurchase.briefDetails,
      "TradeDisputeResolutionRule": this.salesPurchase.incTradeAndDispResRule,
      "ContractConditions": this.salesPurchase.contractCondtn,
      "Status": this.salesPurchase.status,
      "OKToProcess": this.salesPurchase.okToProcess,
      "ConfirmationRevenue": this.salesPurchase.confirmRevenue,
      "BankDetailsToSeller": this.salesPurchase.bankDetailsToSeller,
      "ReceivedConfirmationFromSeller": this.salesPurchase.confirmationRecFrmSeller,
      "SellerRCTIReturnedToBuyer": this.salesPurchase.sellerRCTIReturnBuyer,
      "ContractReceivedFromBuyer": this.salesPurchase.contractRecFrmBuyer,
      "ContractSentToSeller": this.salesPurchase.contractSentToSeller,
      "ReceivedContractFromSeller": this.salesPurchase.recContractFrmSeller,
      "TransferConfirmedBy": this.salesPurchase.transConfirmBy,
      "UnitsDeliveredToBuyer": this.salesPurchase.unitsDelvToBuyer,
      "FinalDeliveryData": this.salesPurchase.finalDelvDate,
      "OKToInvoice": this.salesPurchase.okToInvoice,
      "ConfirmationRevenueInclGST": this.salesPurchase.confrmRevenueInclGST,
      "ConfirmationRevenueExclGST": this.salesPurchase.confrmRevenueExclGST,
      "InvoiceNo": this.salesPurchase.invoiceNo,
      "InvoiceDate": new Date(this.salesPurchase.invoiceDate),
      "InvoiceReceived": this.salesPurchase.invoiceReceivedAndClosed,
      "U_ID": this.currentUser.USER.U_ID
    }
    console.log("params", params);
    this.spinnerService.show();
    this.headers = new Headers({
      'Content-Type': 'application/json; charset=UTF-8',
      'Accept': 'application/json'
    });
    this.options = new RequestOptions({ headers: this.headers });

    let url = serviceUrl + "SalesAndPurchase/SalesAndPurchasePS/api/v1/SubmitSalesAndPurchaseData";

    this.http.post(url, params, this.options).subscribe((data) => {
      this.spinnerService.hide();
      this.submitSalesAndPurchaseMsg = data.json().Response;
      console.log('resp', data.json());
      this.getSalesPurchaseGridData();
      // this.submitAddffForcast = data.json().Outcome;
      this.salesAndPurchaseModalForm.reset();
      // this.broadCaster.broadcast('Save Event', 'Saved Successfully.');
      // $(".modal-content").scrollTop(0);
      this.salesAndPurchaseModal.hide();
    },(err) => {
      this.spinnerService.hide();
       console.log("Error: submit sales purchase");
    })
  }

  closeEditSalesPurchase(){
    this.editSalesAndPurchaseModal.hide();
  }

  editSalesPurchase(params){
    console.log('--- edit sales purchase', params.data);
    this.editSalesAndPurchaseModal.show();
    this.getSalesType();
    this.getSaleTypeName(params.data);  
    // this.getConfirmationDoneBy(params.data);
    this.salesPurchase.transtobedoneby = params.data.TransferDoneBy;  
    this.salesPurchase.transConfirmBy = params.data.ConfirmationDoneBy;  
    // this.salesPurchase.bankDetailsToSeller = '';  
    // this.salesPurchase.contractSentToSeller = '';
      
    
  }

  saleTypeName;
  getSaleTypeName(data){
    // this.salesPurchase.saleOrigin.Id = data.S_ID;    
    // this.salesPurchase.commodity= data.Commodity;
    this.salesTypeList.forEach(element => {
      // console.log('--- element:', element);
      if(element.Id === data.SaleType){
        // this.salesPurchase.Name = element.Name;
        let refDataurl = serviceUrl + "SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSalesAndPurchaseReferenceData?SaleType=" + element.Name + "&SiteOrigin=" + data.S_ID + "&Commodity=" + data.Commodity;
        this.getSalesAndPurchaseRefDataOriginDest(refDataurl);
      }
    });    
  }

  getConfirmationDoneBy(data){
    this.transferredConfirmedByRefDataList.forEach(element=>{
      console.log('--- element:', element);
      // if(element.Id === data.SaleType){

      // }
    });
  }

  submitEditSalesAndPurchase(){
    // https://agnalysis.io:9074/SalesAndPurchase/SalesAndPurchasePS/api/v1/EditSalesAndPurchaseData
    let params = {
            
      "Status": this.salesPurchase.status,
      "OKToProcess": this.salesPurchase.okToProcess,
      "ConfirmationRevenue": this.salesPurchase.confirmRevenue,
      "BankDetailsToSeller": this.salesPurchase.bankDetailsToSeller,
      "ReceivedConfirmationFromSeller": this.salesPurchase.confirmationRecFrmSeller,
      "SellerRCTIReturnedToBuyer": this.salesPurchase.sellerRCTIReturnBuyer,
      "ContractReceivedFromBuyer": this.salesPurchase.contractRecFrmBuyer,
      "ContractSentToSeller": this.salesPurchase.contractSentToSeller,
      "ReceivedContractFromSeller": this.salesPurchase.recContractFrmSeller,
      "TransferConfirmedBy": this.salesPurchase.transConfirmBy,
      "UnitsDeliveredToBuyer": this.salesPurchase.unitsDelvToBuyer,
      "FinalDeliveryData": this.salesPurchase.finalDelvDate,
      "OKToInvoice": this.salesPurchase.okToInvoice,
      "ConfirmationRevenueInclGST": this.salesPurchase.confrmRevenueInclGST,
      "ConfirmationRevenueExclGST": this.salesPurchase.confrmRevenueExclGST,
      "InvoiceNo": this.salesPurchase.invoiceNo,
      "InvoiceDate": new Date(this.salesPurchase.invoiceDate),
      "InvoiceReceived": this.salesPurchase.invoiceReceivedAndClosed,
      "U_ID": this.currentUser.USER.U_ID
    }
    console.log("params", params);
    this.spinnerService.show();
    this.headers = new Headers({
      'Content-Type': 'application/json; charset=UTF-8',
      'Accept': 'application/json'
    });
    this.options = new RequestOptions({ headers: this.headers });

    let url = serviceUrl + "SalesAndPurchase/SalesAndPurchasePS/api/v1/EditSalesAndPurchaseData";

    this.http.post(url, params, this.options).subscribe((data) => {
      this.spinnerService.hide();
      this.submitSalesAndPurchaseMsg = data.json().Response;
      console.log('resp', data.json());
      this.getSalesPurchaseGridData();
      // this.submitAddffForcast = data.json().Outcome;
      this.editSalesAndPurchaseModalForm.reset();
      // this.broadCaster.broadcast('Save Event', 'Saved Successfully.');
      // $(".modal-content").scrollTop(0);
      this.closeEditSalesPurchase()
    },(err) => {
      this.spinnerService.hide();
       console.log("Error: submit sales purchase");
    })
  }

}
