import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesPosByCommodityGridComponent } from './sales-pos-by-commodity-grid.component';

describe('SalesPosByCommodityGridComponent', () => {
  let component: SalesPosByCommodityGridComponent;
  let fixture: ComponentFixture<SalesPosByCommodityGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesPosByCommodityGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesPosByCommodityGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
