import { Component, OnInit } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import {serviceUrl} from "../app.module";
import { Http, Headers, RequestOptions } from '@angular/http';
import { UserRoleService } from '../user-role.service';

@Component({
  selector: 'app-sales-pos-by-commodity-grid',
  templateUrl: './sales-pos-by-commodity-grid.component.html',
  styleUrls: ['./sales-pos-by-commodity-grid.component.scss']
})
export class SalesPosByCommodityGridComponent implements OnInit {

  gridOptions: GridOptions;
  gridApi;
  gridColumnApi;
  getRowNodeId;
  defaultColDef;
  columnDefs;
  rowData;
  currentUser;
  constructor(private http: Http, private role: UserRoleService) {
    this.currentUser = this.role.getUser();
    this.gridOptions = <GridOptions>{
      onGridReady: () => {
        this.gridOptions.api.sizeColumnsToFit();
      }
    };
    this.defaultColDef = { editable: false };
    this.getRowNodeId = function (data) {
      return data.id;
    };
    this.columnDefs = [
		{headerName: "Commodity", field: "Commodity", pinned: 'left'},
		{headerName: "StorageName", field: "StorageName", pinned: 'left'},
		{headerName: "CropYear", field: "CropYear", pinned: 'left'},
		{headerName: "StartingQuantity", field: "StartingQuantity", pinned: 'left'},
		{headerName: "SoldQuantity", field: "SoldQuantity", pinned: 'left'},
		{headerName: "UnsoldQuantity", field: "UnsoldQuantity", pinned: 'left'},
		{headerName: "FreightFarmToInventoryLocation", field: "FreightFarmToInventoryLocation", pinned: 'left'},
		{headerName: "FreightInventoryToSaleLocation", field: "FreightInventoryToSaleLocation", pinned: 'left'},
		{headerName: "CarryFumigationCost", field: "CarryFumigationCost", pinned: 'left'},
		{headerName: "ExFarmPrice", field: "ExFarmPrice", pinned: 'left',hide:true},
		{headerName: "M2MRevenue", field: "M2MRevenue", pinned: 'left',hide:true},
		{headerName: "BreakevenPerUnit", field: "BreakevenPerUnit", pinned: 'left',hide:true},
		{headerName: "M2MProfitLoss", field: "M2MProfitLoss", pinned: 'left',hide:true},
		{headerName: "M2MProfit", field: "M2MProfit", pinned: 'left',hide:true},
		{headerName: "M2MROI", field: "M2MROI", pinned: 'left',hide:true},
		{headerName: "PPDestination", field: "PPDestination", pinned: 'left',hide:true},
		{headerName: "SaleValuePerUnit", field: "SaleValuePerUnit", pinned: 'left',hide:true},
		{headerName: "FreightNTPLocationDifferential", field: "FreightNTPLocationDifferential", pinned: 'left',hide:true},
		{headerName: "TargetROI", field: "TargetROI", pinned: 'left',hide:true},
		{headerName: "WeightedAvgGrowingProfitPerUnit", field: "WeightedAvgGrowingProfitPerUnit", pinned: 'left',hide:true},
		{headerName: "WeightedAverageRevenue", field: "WeightedAverageRevenue", pinned: 'left',hide:true},
		{headerName: "AvgGrowingCost", field: "AvgGrowingCost", pinned: 'left',hide:true},
		{headerName: "WeightedAvgGrowingProfitPerHA", field: "WeightedAvgGrowingProfitPerHA", pinned: 'left',hide:true},
		{headerName: "TotalAssetsEmployedCostPerCrop", field: "TotalAssetsEmployedCostPerCrop", pinned: 'left',hide:true},
		{headerName: "TotalAssetsEmployedToGrow", field: "TotalAssetsEmployedToGrow", pinned: 'left',hide:true},
		{headerName: "ROI", field: "DateCompleted", pinned: 'left',hide:true},
		{headerName: "AverageYield", field: "AverageYield", pinned: 'left',hide:true},
		{headerName: "SoldPerc", field: "SoldPerc", pinned: 'left',hide:true}
    ];
    this.rowData = [];
  }

  ngOnInit() {
    this.getSalesPosByCommodity();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  selectAllRows() {
    this.gridOptions.api.selectAll();
  }


  getSalesPosByCommodity() {
  	//salesAndM2MPosition/SalesPositionPS/GetSalesPositionByCommodity/api/v1/GetSalesPositionByCommodity?U_ID=261
    let url = serviceUrl+'SalesAndM2MPosition/SalesPositionPS/api/v1/GetSalesPositionByCommodity?U_ID=' + this.currentUser.USER.U_ID;
    this.http.get(url).subscribe((data) => {
      if (data.json() != null) {
        this.rowData = data.json()?data.json().SalesPosition:[];
      } else {
        this.rowData = [];
      }
    })
  }


}
