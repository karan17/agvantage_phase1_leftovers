import { Component, OnInit, ViewChild } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import { Http } from '@angular/http';
import { serviceUrl } from '../app.module';
import { UserRoleService } from '../user-role.service';
import { Broadcaster } from '../broadcaster';
import "ag-grid-enterprise";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-pcbgrid',
  templateUrl: './pcbgrid.component.html',
  styleUrls: ['./pcbgrid.component.scss']
})
export class PcbgridComponent implements OnInit {

  gridOptions: GridOptions;
  gridApi;
  gridColumnApi;
  columnDefs;
  columnDefs1;
  columnDefs2;
  frameworkComponents;
  rowData: any;
  defaultColDef;
  getRowNodeId;
  currentUser: any;
  detailedInfo: any = { Net_Unsold_Quantity: 0 };
  url: string;
  @ViewChild('dvModel') public dvModel;
  @ViewChild('delModal') public delModal;
  gridColumnApi1: any;
  gridApi1: any;
  gridApi2: any;
  gridColumnApi2: any;
  holdParams: any;
  rowData1: any;
  rowData2: any;
  saveMessage: any;
  editType: string;
  enableEditForecaset: boolean = true;
  holdPCB: any;
  delMessage: any = false;
  commodities: any;
  cropYearList: any;
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.getBreakevenSummary();
  }
  onGridReady1(params) {
    this.gridApi1 = params.api;
    this.gridColumnApi1 = params.columnApi;
    


  }
  onGridReady2(params) {
    this.gridApi2 = params.api;
    this.gridColumnApi2 = params.columnApi;

  }

  ngOnInit() {

  }


  constructor(private http: Http, private user: UserRoleService, private broadCaster: Broadcaster, private spinnerService: Ng4LoadingSpinnerService) {
    this.currentUser = this.user.getUser();
    this.getCommodities();
    this.getCropyearList();
    this.gridOptions = <GridOptions>{
      onGridReady: () => {

      }
    };
    this.broadCaster.on<any>('Save PCB Event')
      .subscribe(res => {
        this.gridOptions.api.showLoadingOverlay()
        this.getBreakevenSummary();
        this.saveMessage = res;
        setTimeout(() => { this.saveMessage = false }, 4000);
      });
    this.columnDefs = [
      { headerName: "Farm Name", field: "farm_name" },
      {
        headerName: "Commodity", field: "commodity_name", onCellClicked: (params) => {
          this.detailedCommodity(params);
        },
        cellRenderer: (params) => `<a>${params.value}</a>`, cellStyle: (params) => {
          return { cursor: 'pointer', color: 'blue' }
        }
      },

      { headerName: "Crop Year", field: "crop_year" },
      { headerName: "Area", field: "AreaHectares" },
      { headerName: "Total Production", field: "TotalProduction" },
      { headerName: "Marketable Production", field: "MarketableProduction" },
      { headerName: "Total Growing Cost Per Hectare", field: "TotalGrowingCostsPerHectare" },
      { headerName: "Total Growing Cost Per Crop", field: "TotalGrowingCostsPerCrop" },
      { headerName: "Growing Breakeven Per Unit", field: "GrowingBreakevenPerUnit" },
      { headerName: "Total Assets Employed To Grow Crop/ha", field: "TotalAssetsCostPerHectare" },
      { headerName: "Total Assets Employed Cost Per Crop", field: "TotalAssetsCostPerCrop" },
      { headerName: "Assets Employed Breakeven Per Unit", field: "AssetsEmployedBreakevenPerUnit" },
      { headerName: "Target ROI", field: "TargetROI" },

    ];

    this.columnDefs1 = [
      {
        headerName: "Actions", valueGetter: (params) => 'Delete', cellRenderer: (params) => `<a>${params.value}</a>`
        , cellStyle: (params) => {
          return { cursor: 'pointer', color: 'blue' }
        },
        onCellClicked: (params) => {
          this.deleteCommodity(params);
        },
      },
      {
        headerName: "Farm Name", field: "farmName", editable: true, onCellValueChanged: (params) => {
          this.onCellValueChanged(params);
        },
        cellEditor: 'agTextCellEditor',
       
      },
      {
        headerName: "Field Name", field: "fieldName", editable: true, onCellValueChanged: (params) => {
          this.onCellValueChanged(params);
        },
        cellEditor: 'agTextCellEditor',
        
      },
      {
        headerName: "Commodity", field: "commodityName", editable: true, onCellValueChanged: (params) => {
          this.onCellValueChanged(params);
        },  cellEditor: 'agRichSelectCellEditor',
        cellEditorParams: {
          cellHeight: 50,
          values:["Wheat", "Malt Barley", "Feed Barley", "Canola", "Sorghum", "Chickpeas", "Peas", "Faba Beans", "Lentils", "Oats", "Lupins", "Cotton", "Cotton Seed", "Adzuki Beans", "Broad Beans", "CANOLA - Non-GM Canola", "CANOLA QUALITY JUNCEA", "Fenugreek", "Maize", "Mung Beans", "RAPESEED", "SOYBEAN ", "SUNFLOWER", "Vetch"]
        }
      },
      {
        headerName: "Crop Year", field: "cropYear", editable: true, onCellValueChanged: (params) => {
          this.onCellValueChanged(params);
        },
        cellEditor: 'agRichSelectCellEditor',
        cellEditorParams: {
          cellHeight: 50,
          values: ["2013-2014", "2014-2015", "2015-2016", "2016-2017", "2017-2018", "2018-2019", "2019-2020", "2020-2021", "2021-2022", "2022-2023"]
        }
      },

      {
        headerName: "Area(ha)", field: "areaHectares", editable: true, onCellValueChanged: (params) => {
          this.onCellValueChanged(params);
        },
        cellEditor: "agTextCellEditor"
      },
      {
        headerName: "Yield/ha", field: "yieldHectare", editable: true, onCellValueChanged: (params) => {
          this.onCellValueChanged(params);
        },
        cellEditor: "agTextCellEditor"
      },
      {
        headerName: "Area(acres)", field: "areaAcres", editable: true, onCellValueChanged: (params) => {
          this.onCellValueChanged(params);
        },
        cellEditor: "agTextCellEditor"
      },
      {
        headerName: "Yield/acres", field: "yieldAcres", editable: true, onCellValueChanged: (params) => {
          this.onCellValueChanged(params);
        },
        cellEditor: "agTextCellEditor"
      },
      {
        headerName: "Sharefarmer%", field: "sharefarmer", editable: true, onCellValueChanged: (params) => {
          this.onCellValueChanged(params);
        },
        cellEditor: "agTextCellEditor"
      },
      {
        headerName: "Target ROI", field: "targetRoi", editable: true, onCellValueChanged: (params) => {
          this.onCellValueChanged(params);
        },
        cellEditor: "agTextCellEditor"
      },
      {
        headerName: "Costs to Grow Crop/ha", field: "growingcostsPerHectare", editable: true, onCellValueChanged: (params) => {
          this.onCellValueChanged(params);
        },
        cellEditor: "agTextCellEditor"
      },
      {
        headerName: "Total Production", field: "totalProduction", editable: true, onCellValueChanged: (params) => {
          this.onCellValueChanged(params);
        },
        cellEditor: "agTextCellEditor"
      },
      {
        headerName: "Marketable Production", field: "marketableProduction", editable: true, onCellValueChanged: (params) => {
          this.onCellValueChanged(params);
        },
        cellEditor: "agTextCellEditor"
      }
    ];
    this.editType = "fullRow";

    this.columnDefs2 = [
      { headerName: "Commodity", field: "commodityName" },
      { headerName: "Crop Year", field: "crop_year" },
      { headerName: "Contract Number", field: "contract_number" },
      { headerName: "Sold Quantity(units)", field: "sold_quantity" }
    ];


    this.getRowNodeId = function (data) {
      return data.id;
    };
    this.broadCaster.on<any>('Save Farm Event')
      .subscribe(res => {
        this.gridOptions.api.showLoadingOverlay()
        this.getBreakevenSummary();
        this.saveMessage = res;
        setTimeout(() => { this.saveMessage = false }, 4000);
      });

  }
  closePCBModal() {
    this.dvModel.hide();
  }
  editStarted(params) {
    this.enableEditForecaset = false;
  }
  onCellValueChanged(params) {
    this.enableEditForecaset = false;
    let cellField = params.colDef.field;
    this.rowData1[params.node.rowIndex][cellField] = params.newValue;

  }
  getDetailedViewDetail(params) {
    let url = `${serviceUrl}AgvantageGetDetailedProductionCostsSB/AgvantageGetDetailedProductionRestPS/api/v1/GetDetailedProductionCost?user_id=${this.currentUser.USER.U_ID}&crop_year=${params.data.crop_year}&commodity_name=${params.data.commodity_name}`;
    //this.url = serviceUrl+"AgvantageGetDetailedProductionCostsSB/AgvantageGetDetailedProductionRestPS/api/v1/GetDetailedProductionCost?U_ID=" + uid;
   this.spinnerService.show();
    this.http.get(url).subscribe((data) => {
      this.detailedInfo = data.json() ? data.json() : {};
      console.log(this.detailedInfo);
      this.rowData1 = this.detailedInfo?this.detailedInfo.ProductionCosts:[];
      this.rowData2 = this.detailedInfo.ForwardSaleSummary?this.detailedInfo.ForwardSaleSummary.ForwardSale:[];
      this.gridApi1.sizeColumnsToFit();
      this.gridApi2.sizeColumnsToFit();
      this.spinnerService.hide();
      //Open modal

    },
      (err) => {
        this.detailedInfo = { ProductionCosts: [], ForwardSaleSummary: { ForwardSale: [] } };
      })
  }
  detailedCommodity(params) {
    //open the deatiled foreacst pop up
    this.holdParams = params;
    this.getDetailedViewDetail(params);
    this.dvModel.show();

  }

  getBreakevenSummary() {
    let url = `${serviceUrl}AgvantageGetSummaryBreakevenSB/AgvantageGetSummaryBreakevenRestPS/api/v1/GetSummaryBreakeven?user_id=${this.currentUser.USER.U_ID}`;
    this.http.get(url).subscribe((data) => {
      this.rowData = data.json() ? data.json().BreakevenResponse : [];
      this.gridApi.sizeColumnsToFit();
      this.spinnerService.hide();
    },
      (err) => {
        this.rowData = [];
        this.spinnerService.hide();

      })
  }

  @ViewChild('pcbGridEditModel') pcbGridEditModel;
  editPCBModal() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
  }
  methodFromParent(rowObj) {
    console.log(rowObj.data);
    this.pcbGridEditModel.show();
  }

  deleteCommodity(params) {
    this.holdPCB = params;
    this.delModal.show();
  }
  updateForecast() {
    console.log("edited row is -----------", this.rowData1);
    this.spinnerService.show();
    let updateObj = {
      ProductionCosts : this.rowData1
    }
    let url = `${serviceUrl}AgvantageUpdateDetailedProductionCostsSB/AgvantageUpdateDetailedProductionCosts/api/v1/UpdateProductionCosts`;
    this.http.post(url,updateObj).subscribe(data=>{
      this.saveMessage = data.json()?data.json().status: false;
      setTimeout(()=>{this.saveMessage = false;},6000);
      this.dvModel.hide();
      this.getBreakevenSummary();
      this.spinnerService.hide();
    },err=>{
      this.spinnerService.hide();
      alert("Some error in the server");
    })
  }
  closeDeleteDialog() {
    this.delModal.hide();
  }
  deletePCB() {
    this.spinnerService.show();
    let url = `${serviceUrl}AgvantageDeleteProductionCosts/DeleteProductionCosts/api/v1/DeleteProductionCosts`;
    let params = {
      "ProductionCosts": {
        "pcId": this.holdParams.pcId
      }
    };
    this.http.post(url, params).subscribe((data) => {
      console.log(data.json());
      this.spinnerService.hide();
      this.delMessage = data.json() ? data.json().status : false;
      setTimeout(() => { this.delMessage = false; }, 4000);
      this.delModal.hide();
      this.getDetailedViewDetail(this.holdParams);
    }, (err) => {
      this.delMessage = "Some error occured";
      this.spinnerService.hide();
      console.log(err.json());
    })
  }

  getCommodities() {
    this.url = serviceUrl + "AgVantageGetCommoditiesSB/getCommodities/api/v1/getCommodity?getCommodities=true";
    this.http.get(this.url).subscribe((data) => {
      this.commodities = data.json().CommodityCollection.Commodity.map((val) => val.C_NAME);
    })

  }
  getCropyearList() {
    let url = serviceUrl + 'AgvGetCropYear/GetCropYear/api/v1/getCropYear?getcropyer=true';
    this.http.get(url).subscribe((data) => {
      // console.log('crp year data:',data.json().CropYear);
      this.cropYearList = data.json().CropYear.map((val) => val.Crop_year);
      // this.selectedCommodity1=this.commodities[0].C_NAME;
      // console.log(this.commodities);
    })
  }

}




