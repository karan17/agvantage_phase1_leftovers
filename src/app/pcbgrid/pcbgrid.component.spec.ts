import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PcbgridComponent } from './pcbgrid.component';

describe('PcbgridComponent', () => {
  let component: PcbgridComponent;
  let fixture: ComponentFixture<PcbgridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PcbgridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PcbgridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
