import {
  Component, OnInit, OnDestroy, NgZone, ViewChild, ElementRef, Input, Output, EventEmitter, ViewContainerRef,
  ComponentFactoryResolver, ComponentFactory, ComponentRef
} from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import { UserRoleService } from '../user-role.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Broadcaster } from '../broadcaster';
import { GridOptions } from "ag-grid/main";
import { serviceUrl } from "../app.module";
import { EditDeleteRendererComponent } from '../edit-delete-renderer/edit-delete-renderer.component';

@Component({
  selector: 'app-contractors-on-farm',
  templateUrl: './contractors-on-farm.component.html',
  styleUrls: ['./contractors-on-farm.component.scss']
})
export class ContractorsOnFarmComponent implements OnInit {
  saleHistoryData: any;
  @ViewChild('addContractorsModal') public addContractorsModal;
  @ViewChild('addCommMoveRecModal') public addCommMoveRecModal;
  @ViewChild('addFarmAndFieldsModal') public addFarmAndFieldsModal;
  @ViewChild('selectSaleModal') public selectSaleModal;
  @ViewChild('salesAndPurchaseModal') public salesAndPurchaseModal;
  @ViewChild('salesAndPurchaseModalForm') public salesAndPurchaseModalForm;
  @ViewChild('addFarmAndFieldsForm') public addFarmAndFieldsForm;

  addContractors;
  machineryDetails;
  addCommMovmtRec;
  addFarmAndFields;

  headers: Headers;
  options: RequestOptions;
  submitBrkevenForcast;
  dateCommenced;
  contractorCompName;
  compAdd;
  CompContactDetails;
  phone;
  email;
  keyContact;
  operatorName;
  operatorPhone;
  operatorUHF;
  machReg;
  jobortask;
  fieldsLocation;
  hectarescompleted;
  datecompleted;
  notes;
  deliveryCompNm;
  driverName;
  machineryReg;
  carrierrating;
  comments;
  gridOptions: GridOptions;
  gridOptionsSales : GridOptions;
  gridApi;
  gridColumnApi;

  columnDefs;
  selectSaleColumnDefs;
  rowData;
  defaultColDef;
  columnDefsNTPDetails;
  getRowNodeId;

  currentUser;
  newfarm: boolean = false;
  salesPurchase;
  farmname; farmArea; nfrm;
  dte; stype; saleorigin; comm; buyertradingName; sellertradingName; sellerrepresentative; tolerancelist; buyersrepresentative;
  delvterms; businesscreditedTo; paytermsrefData; officecreditedto; commodityorigin; prodcr; bnkr1; refnumber; commoditydestination;
  ffs; farmregion; ffsinvoice; addfreight; vrty; delperiodfrom; delperiodto; grd; weightterms; cropyear; rulesarb; contractno;
  gradestd; contracttype; gtacontractno; qty; gt; contractprice; discount; transtobedoneBy; increments; areahec; minyuphec;
  maxyuphec; minquantity; qnttotunmax; briefdetails; inctradeanddispresrule; contractcondtn; status; compcontactdetails;
  transconfirmby; confirmrevenue; unitsdelvtobuyer; bankdetailstoseller; finaldelvdate; confrmRevenueexclGST; confrmrevenueinclGST;
  rctisent; invoiceno; invoicedate;
  rowSelection: string;
  holdContractNo: any;
  enableNext: boolean = true;
  storageNames: any;
  truckTypes: any;
  EntityName: any;
  NGR: any;
  gridOptionsTruck :GridOptions;
  gridApiTruck: any;
  gridColumnApiTruck: any;
  gridOptionsNTP :GridOptions;
  gridApiNTP: any;
  gridColumnApiNTP: any;

  tabs: { title: string; content: string; active: boolean; disabled: boolean; }[];
  businessCreditRefData: any;
  brokerOffices :any[] = [];
  buyerTradingNames: any;
  sellerTradingNames: any;
  sellerRepresentatives: any;
  constructor(private router: Router, private role: UserRoleService, private _ngZone: NgZone,
    private http: Http, public spinnerService: Ng4LoadingSpinnerService, private broadCaster: Broadcaster,
    private resolver: ComponentFactoryResolver) {
      this.gridOptionsTruck = <GridOptions>{
        onGridReadyTruck: () => {
           //this.setTruckGrid();
            
        }
      };

      this.gridOptionsNTP = <GridOptions>{
        onGridReadyNTP: () => {
           //this.setTruckGrid();
            
        }
      };
      
  this.tabs = [
    { title: 'Dynamic Title 1', content: 'Dynamic content 1', active: true, disabled: false },
    { title: 'Dynamic Title 2', content: 'Dynamic content 2', active: false, disabled: true }
  ];
    this.addContractors = {};
    this.machineryDetails = {};
    this.addCommMovmtRec = {};
    this.addFarmAndFields = {};
    this.salesPurchase = {};
    this.rowSelection = "single";

    this.currentUser = this.role.getUser();
    console.log("--- cunrrent user", this.currentUser);
    this.getFarmDetails();
    this.getFieldDetailsByUserId();
    this.getWarehouseTypes();
    this.getSalesType();
    this.getSalesOriginDest();
    //this.getSalesAndPurchaseRefDataOriginDest();
    this.getCommodities();
    this.getCropyearList();
    
    // this.getCommodityDestination();
    this.EntityName = this.currentUser.USER.U_TRADING_NAME;
    this.NGR = this.currentUser.USER.U_NGR;
    
    this.columnDefs = [

      { headerName: "Load", field: "load", width :120 },
      { headerName: "Add Contract No", field: "ContractNo",width :120  },
      { headerName: "Buyer", field: "Buyer"},
      { headerName: "Product Collected", field: "CommodityCollected", width :120  },
      { headerName: "Grade", field: "Grade",width :120  },
      { headerName: "Truck Type", field: "TruckType",width :120  },
      { headerName: "Estimated Weight", field: "EstimatedWeight" , width :120 },
      { headerName: "Delivery Company Name", field: "DeliveryCompanyName" , width :120 },
      { headerName: "Driver Name", field: "DriverName" , width :120 },
      { headerName: "Truck Reg No", field: "TruckRegNo" , width :120 }
    ];

    this.selectSaleColumnDefs = [

      { headerName: "Action", checkboxSelection: true, pinned: 'left' },
      { headerName: "Contract No", field: "ContractNo" },
      { headerName: "Commodity", field: "Commodity" },
      { headerName: "Grade", field: "ContractNo" },
      { headerName: "Crop Year", field: "CropYear"},
      { headerName: "Sold Quantity", field: "Quantity"},
      { headerName: "Buyer", field: "BuyerName"},
    ];
   
    this.columnDefsNTPDetails = [
      { headerName: "NTP Name",  field: "NTPName"},
      { headerName: "Nominated Sites", field: "NomSites" },
      { headerName: "Location Differential", field: "LocDiff" },
      { headerName: "Site Price", field: "SitePrice" }
    ]

    this.rowData = [];

    // this.context = { componentParent: this };
    // this.frameworkComponents = {
    //   commodityUpdateCellRendererComponent: CommodityUpdateCellRendererComponent
    // };



  }
  onGridReadySH(params) {
    this.getRowNodeId = function (data) {
      return data.id;
    };
  }
  onGridReadyTruck(params) {
    this.gridApiTruck = params.api;
    this.gridColumnApiTruck = params.columnApi;
    
    this.setTruckGrid();
   
  }

  onGridReadyNTP(params) {
    this.gridApiNTP = params.api;
    this.gridColumnApiNTP = params.columnApi;
    
    this.setNTPGrid();
   
  }


  callSellerRepresents(){
    this.getSellerRepresentative();
  }
  ffsInvoice;
  ngOnInit() {
    this.getTruckTypes();
    this.getBrokerOffice();
    this.getStatus();
    this.getBuyerTradingName();
    this.getSellerTradingName();
    if(this.currentUser.USER.UserInRole === "BUYER"){
      this.salesPurchase.FFS = 2.5;
    }
    this.ffsInvoice = [
      {id:1, name:"Seller"},
      {id:1, name:"Buyer"}
    ]
    this.salesPurchase.contractPrice = 0;
    // this.salesPurchase.farmRegion = this.currentUser.USER.U_A_REGION;

  }
  getBuyerTradingName(){
    let url = `${serviceUrl}GetRegisteredCompaniesSB/GetRegisteredCompaniesPS/api/v1/GetRegisteredCompanies?CompanyType=Buyers`;
    this.http.get(url).subscribe(data=>{
      this.buyerTradingNames = data.json()?data.json().CompanyTradingName : [];
    },err=>{
      this.buyerTradingNames = [];
    })
  }
  
  getSellerTradingName(){
    let url = `${serviceUrl}GetRegisteredCompaniesSB/GetRegisteredCompaniesPS/api/v1/GetRegisteredCompanies?CompanyType=Sellers`;
    this.http.get(url).subscribe(data=>{
      this.sellerTradingNames = data.json()?data.json().CompanyTradingName : [];
    },err=>{
      this.sellerTradingNames = [];
    })
  }
  getSellerRepresentative(){
    let url = `${serviceUrl}GetCompanyMembers/GetCompanyMembersPS/api/v1/GetCompanyMembers?CompanyName=${this.salesPurchase.sellerTradingName}`;
    this.http.get(url).subscribe(data=>{
      this.sellerRepresentatives = data.json()?data.json().Member : [];
    },err=>{
      this.sellerRepresentatives = [];
    })
  }
  setTruckGrid(){
    this.gridOptionsTruck.api.setRowData([]);
    //setTimeout(()=>{this.gridOptionsTruck.api.sizeColumnsToFit();});
    
  }

  setNTPGrid(){
    this.gridOptionsNTP.api.setRowData([]);
    //setTimeout(()=>{this.gridOptionsTruck.api.sizeColumnsToFit();});
    
  }

  ntpSiteListData;
  getNTPSiteList(){
    this.spinnerService.show();
    let url=`${serviceUrl}AgvantageGetSitesSB/AgvantageGetSitesRestPS/api/v1/GetSitesList?state_id=1&storage_type=`+this.salesPurchase.saleOrigin.Name;
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.ntpSiteListData = data.json() ? data.json().getSites : [];
    },
      (err) => {
        this.ntpSiteListData = [];
      });
  }
  // getLocDiff()
  ntpNominatedSiteListData;
  getNTPNominatedSiteList(){
    this.spinnerService.show();
    let url=`${serviceUrl}AgvantageGetNTPNominatedSitesSB/AgvantageGetNTPNominatedSitesRestPS/api/v1/GetNominatedSites?ntp_id=`+this.salesPurchase.ntpNameList.Site_ID;
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.ntpNominatedSiteListData = data.json() ? data.json().NominatedSite : [];
    },
      (err) => {
        this.ntpNominatedSiteListData = [];
      });
  }

  getLocDiff(){   
    
    this.spinnerService.show();
    let url = `${serviceUrl}GetLocationDifferential/GetLocationDifferentialPS/api/v1/GetLocationDifferential?NTPName=`+this.salesPurchase.ntpNomSiteList.NTP_Name+`&NominatedSite=`+this.salesPurchase.ntpNomSiteList.NominatedSite_Name;
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.salesPurchase.locDiff = data.json().Differential;
    })
  }

  statusList;
  getStatus(){
    this.statusList = [
      { key: 'Open', value: 'Open' },
      { key: 'Confirmed', value: 'Confirmed' },
      { key: 'Washed Out - No Confirmation Revenue', value: 'Washed Out - No Confirmation Revenue' },
      { key: 'Washed Out - Keep Confirmation Revenue', value: 'Washed Out - Keep Confirmation Revenue' }
    ];
  }



  gotoHome() {
    this._ngZone.run(() => this.router.navigate(['dashboard']));
  }

  openAddContractors() {
         
  this.tabs = [
    { title: 'Dynamic Title 1', content: 'Dynamic content 1', active: true, disabled: false },
    { title: 'Dynamic Title 2', content: 'Dynamic content 2', active: false, disabled: true }
  ];
   
     console.log(this.tabs);
    this.addContractorsModal.show();
    
  }

  closeAddContractors() {
    this.addContractorsModal.hide();
  }
  setFarmArea($event){
    this.addFarmAndFields.FarmArea = this.addFarmAndFields.FarmNameAuto.farm_area;
  }

  openAddCommMovementRecord() {
    this.enableNext = true;
    this.addCommMoveRecModal.show();
    this.getSaleHistory();
  }

  closeAddCommMovementRecord() {
    this.addCommMoveRecModal.hide();
  }

  openAddFieldsModal() {
    this.addFarmAndFieldsModal.show();
  }

  closeAddFieldsModal() {
    this.addFarmAndFieldsModal.hide();
  }

  openSelectSaleModal() {
    this.selectSaleModal.show();
  }

  closeSelectSaleModal() {
    this.selectSaleModal.hide();
  }

  openSalesAndPurchaseModal() {
    this.salesAndPurchaseModal.show();
  }

  closeSalesAndPurchaseModal() {
    this.salesAndPurchaseModal.hide();
  }
  onSelectionSale(param) {
    console.log("param", param);
    if (param.node.selected) {
      this.enableNext = false;
      this.holdContractNo = param.data.ContractNo;
      this.addCommMovmtRec.Buyer = param.data.Buyer;
      this.addCommMovmtRec.CommCollected = param.data.Commodity;
      this.addCommMovmtRec.Grade = param.data.Grade;
    }
  }

  setContractNo() {
    this.addCommMovmtRec.SelectContractNo = this.holdContractNo;
    this.selectSaleModal.hide();
  }
  fieldsArray = [{ farm_name: '', field_area: '' }];

  addFields(obj, index) {
    console.log(obj);
    delete obj.farm_name;
    let newObj = Object.assign({}, obj);
    if (this.fieldsArray.length < 11) {
      newObj.field_name = '';
      newObj.field_area = '';
      this.fieldsArray.push(newObj);
    }
    console.log('create new chemical fields...', this.fieldsArray);
  }

  removeFields(obj, index) {
    console.log('obj', obj, 'i:', index);
    // const idx: number = this.chemArray.indexOf(obj);
    this.fieldsArray.splice(index, 1);
  }
 
  load=0;truckLoadData=[];
  addTruckLoad() {
    
    this.load=this.load+1;
    let obj = {
      load: this.load,
      ContractNo: this.addCommMovmtRec.SelectContractNo,
      Buyer: this.addCommMovmtRec.Buyer, CommodityCollected: this.addCommMovmtRec.CommCollected,
      Grade: this.addCommMovmtRec.Grade, TruckType: this.addCommMovmtRec.TruckType, 
      EstimatedWeight: this.addCommMovmtRec.EstWeight, DeliveryCompanyName:this.addCommMovmtRec.DeliveryCompName,
      DriverName:this.addCommMovmtRec.DriverName, TruckRegNo:this.addCommMovmtRec.TruckRegNumber,
      Comments:this.addCommMovmtRec.Comments
    }
    this.gridApiTruck.updateRowData({ add: [obj] });
    
    this.truckLoadData.push(obj);
    this.addCommMovmtRec.DriverName = "";
    this.addCommMovmtRec.TruckRegNumber = "";
  }

  ntpLoadData =[];NTPName; NomSites;LocDiff;SitePrice;
  addNTPSites() {
    // this.load=this.load+1;
    // NTP Contract Price - Location Differential = Site Price.
    this.salesPurchase.ntpSitePrice = this.salesPurchase.contractPrice - this.salesPurchase.locDiff;
    console.log("--- this.salesPurchase.ntpSitePrice", this.salesPurchase.ntpSitePrice);
    let obj = {
      NTPName: this.salesPurchase.ntpNameList.Site_Name,
      NomSites: this.salesPurchase.ntpNomSiteList.NominatedSite_Name, 
      LocDiff: this.salesPurchase.locDiff, 
      SitePrice: this.salesPurchase.ntpSitePrice
    }
    this.gridApiTruck.updateRowData({ add: [obj] });
    
    this.ntpLoadData.push(obj);
  }

  selectNextTab(index) {
    this.tabs[index].active = true;
    this.tabs[index].disabled = false;
  }

  addcontractmessage;
  addContractDetails() {
    this.headers = new Headers({
      'Content-Type': 'application/json; charset=UTF-8',
      'Accept': 'application/json'
    });
    this.options = new RequestOptions({ headers: this.headers });
    this.spinnerService.show();
    let contractorDetails = {
      "ContractorName": this.addContractors.ContractorCompName,
      "Phone": this.addContractors.Phone,
      "UHF": this.addContractors.OperatorUHF,
      "CompanyName": this.addContractors.ContractorCompName,
      "CompanyAddress": this.addContractors.CompAdd,
      "CompanyContact": this.addContractors.CompContactDetails,
      "Job": this.addContractors.Jobortask,
      "FieldLocation": this.addContractors.FieldsLocation.field_name,
      "HectaresCompleted": this.addContractors.Hectarescompleted,
      "CommencementDate": this.addContractors.DateCommenced,
      "CompletionDate": this.addContractors.Datecompleted,
      "ContractorNotes": this.addContractors.Notes,
      "DeliveryCompany": this.machineryDetails.DeliveryCompNm,
      "DriverName": this.machineryDetails.DriverName,
      "MachineryRegNo": this.machineryDetails.MachineryReg,//max size 3
      "CarrierRating": this.addContractors.Carrierrating,//max size 3 
      "Comments": this.machineryDetails.comments,
      "FarmerId": this.currentUser.USER.U_ID
    }

    let url = serviceUrl + "ContractorOnFarmSB/ContractorsOnFarmPS/api/v1/AddContractorOnFarm";
    this.addContractorsModal.hide();
    this.http.post(url, contractorDetails, this.options).subscribe(
      (data) => {
        this.spinnerService.hide();
        console.log("Add contractor res: ", data);
        this.broadCaster.broadcast('Save Event ContractDetail', "Saved Contract Details Succesfully");
        this.addcontractmessage = "Saved Contract Details Succesfully";
      },
      (err) => {
        this.spinnerService.hide();
        console.log("Add contractor err: ", err);
      }
    );
  }
  addmovmentrecmessage;
  addCommMovmtRecord() {    
    
    this.spinnerService.show();
    this.headers = new Headers({
      'Content-Type': 'application/json; charset=UTF-8',
      'Accept': 'application/json'
    });
    this.options = new RequestOptions({ headers: this.headers });
    let addCommMovementRecordObj =
    {
      "CollectionDate": this.addCommMovmtRec.CollectionDate,
      "StorageType": this.addCommMovmtRec.StorageType,
      "StorageName": this.addCommMovmtRec.StorageName.STORAGE_NAME,
      "DistanceFromCity": this.addCommMovmtRec.StorageDistance,
      "DirectionFromCity": this.addCommMovmtRec.StorageDirection,
      "City": this.addCommMovmtRec.citytown,
      "State": this.addCommMovmtRec.state,
      "UserId": this.currentUser.USER.U_ID,
      "TruckLoads": {
        "TruckLoad": this.truckLoadData        
      }
    }
    // console.log("--- addCommMovRec params:", addCommMovementRecordObj);
    let url = `${serviceUrl}CommodityMovementSB/CommodityMovementPS/api/v1/AddCommodityMovement`;
    this.addContractorsModal.hide();
    this.http.post(url, addCommMovementRecordObj, this.options).subscribe(
      (data) => {
        this.spinnerService.hide();
        console.log("addCommMovRec res: ", data);
        this.broadCaster.broadcast('Save Event Commodity Movement', "Saved Commodity Movement Record Succesfully");
        this.addCommMoveRecModal.hide(); 
        this.addmovmentrecmessage = "Saved Commodity Movement Record Succesfully";
      },
      (err) => {
        this.spinnerService.hide();
        console.log("addCommMovRec err: ", err);
      }
    );
  }
  WarehouseTypes;
  getWarehouseTypes() {
    // console.log('this.role.getRole(): ', this.role.getRole());
    // if (this.role.getRole() === "superadmin") {
    //   this.WarehouseTypes = [
    //     { T_ID: 'OFW', T_NAME: 'On Farm' },
    //     { T_ID: 'EFW', T_NAME: 'Ex Farm' }
    //   ]
    // } else if (this.role.getRole() === "P") {
      this.WarehouseTypes = [
        { T_ID: 'OFW', T_NAME: 'On Farm' },
        { T_ID: 'PW', T_NAME: 'Public' }
      ]
    // }
  }

  getStorageNames(type) {
    let url;
    if(type == "OFW")
     url = `${serviceUrl}AgcInvgetStorage/AgvInvGetStoragePS/api/v1/getStorage?store_type=${type}&u_id=${this.currentUser.USER.U_ID}`;
    else
      url = `${serviceUrl}AgcInvgetStorage/AgvInvGetStoragePS/api/v1/getStorage?store_type=${type}&u_id=${this.currentUser.USER.U_ID}`;
    
      this.spinnerService.show();
      this.http.get(url).subscribe((data) => {
        this.spinnerService.hide();
      this.storageNames = data.json() ? data.json().store : [];
    }, (err) => {
      this.storageNames = [];
    })

  }
  getTruckTypes() {
    let url = `${serviceUrl}AgvantageGetTruckAccessSB/AgvantageGetTruckAccessRestPS/api/v1/GetTruckAccess`;
    this.http.get(url).subscribe((data) => {
      this.truckTypes = data.json() ? data.json().TruckType : [];
    }, (err) => {
      this.truckTypes = [];
    })
  }
  callSelectedWHType() {
    console.log('selected warehouse type: ', this.addCommMovmtRec.StorageType);
    this.getStorageNames(this.addCommMovmtRec.StorageType);

  }

  callSelectedStrgNameDetails(){
    
    this.addCommMovmtRec.StorageDistance = this.addCommMovmtRec.StorageName.STORAGE_LOCA_DISTANCE;
    this.addCommMovmtRec.StorageDirection = this.addCommMovmtRec.StorageName.STORAGE_LOC_DIRECTION;
    this.addCommMovmtRec.citytown = this.addCommMovmtRec.StorageName.U_A_CITY;
    this.addCommMovmtRec.state = this.addCommMovmtRec.StorageName.U_A_STATE;
    this.addCommMovmtRec.state = this.addCommMovmtRec.StorageName.U_A_STATE;
  }

  farmDetails;
  getFarmDetails() {
    let user_id = this.currentUser.USER.U_ID;
    let url = serviceUrl + "/AgvantageGetFarmDetailsSB/AgvnatgeGetFarmDetailsRestPS/api/v1/GetFarmDetails?user_id=" + user_id;//363
    this.spinnerService.show();
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.farmDetails = data.json() ? data.json().FarmDetails : [];
    },
      (err) => {
        this.farmDetails = [];
      });
  }

  farmFieldAndLocDetails;
  getFieldDetailsByUserId() {
    let user_id = this.currentUser.USER.U_ID;
    let url = serviceUrl + "/AgvantageGetFieldDetailsSB/AgvantageGetFieldDetailsRestPS/api/v1/GetFieldDetails?user_id=" + user_id;
    this.spinnerService.show();
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      console.log("res", data);
      this.farmFieldAndLocDetails = data.json() ? data.json().FieldDetails : [];
    },
      (err) => {
        this.farmFieldAndLocDetails = [];
      });
  }

  states;
  getStates() {
    this.spinnerService.show();
    let url = serviceUrl + "AgvantageGetStatesSB/AgvantageGetStatesRestPS/api/v1/getStates?country_id=1"
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.states = data.json()?data.json().getStates:[];
    })
  }

  farmingRegions;
  getFarmingRegions() {
  //   this.spinnerService.show();
  //   let url = serviceUrl + "AgvantageGetFarmingRegionsSB/AgvantageGetFarmingRegionsRestPS/api/v1/GetFarmingRegions?state_id=" + this.addDestinationData.State + "&pz_id=" + this.addDestinationData.MarketZone;
  //   this.http.get(url).subscribe((data) => {
  //     this.spinnerService.hide();
      this.farmingRegions = [{FarmingRegion_Name : this.salesPurchase.sellerRepresentative.FarmingRegionId}];
      // this.farmingRegions = data.json()?data.json().getFarmingRegions:[];
  //   })
  }

  // portZones;
  // getPortZones() {
  //   this.spinnerService.show();
  //   let url = serviceUrl + "AgvantageGetPortZonesSB/AgvantageGetPortZoneRestPS/api/v1/getPortZones?state_id=" + this.addDestinationData.State;
  //   this.http.get(url).subscribe((data) => {
  //     this.spinnerService.hide();
  //     this.portZones = data.json()?data.json().getPortZones:[];
  //   })
  // }

  Destination;
  getDestination(){
    let url = "http://129.154.71.56:9073/SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSaleOriginDestinations?Request=GetSaleOrigin";
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.Destination = data.json()?data.json().SiteOrigin:[];
    })
  }

  showNTPDetails = false;
  saleOriginChange(){
    this.getCommodityOrigin();
    this.getCommodityDestination();
    this.getFarmingRegions();
    console.log("saleOrigin selection...");
    if(this.salesPurchase.saleOrigin==="Nearest Terminal Port (NTP)" && this.salesPurchase.SaleType.Name==="Forward"){
      this.showNTPDetails = true;
    }
    // this.showCommDestination();
  }

  submitFarmsAndFields() {

    this.spinnerService.show();
    this.headers = new Headers({
      'Content-Type': 'application/json; charset=UTF-8',
      'Accept': 'application/json'
    });
    this.options = new RequestOptions({ headers: this.headers });
    console.log('addFarmAndFields', this.addFarmAndFields);

    let params = {};
    if (this.addFarmAndFields.farm_id == undefined) {
      params = {
        FarmDetails: {
          user_id: this.currentUser.USER.U_ID,
          farm_name: this.addFarmAndFields.FarmName || this.addFarmAndFields.FarmNameAuto.farm_name,
          farm_area: this.addFarmAndFields.FarmArea,
          FieldDetails: this.fieldsArray

        }

      };
    } else {
      params = {
        FarmDetails: {
          user_id: this.currentUser.USER.U_ID,
          farm_id: this.addFarmAndFields.farm_id,
          FieldDetails: this.fieldsArray

        }

      };
    }
    let url = serviceUrl + "AgvantageAddFarmDetailsSB/AgvantageAddFarmDetailsRestPS/api/v1/AddFarmDetails"

    this.http.post(url, params, this.options).subscribe((data) => {
      this.spinnerService.hide();
      console.log('resp', data.json());
      // this.submitAddffForcast = data.json().Outcome;
      this.addFarmAndFieldsForm.reset();
      // this.broadCaster.broadcast('Save Event', 'Saved Successfully.');

      // $(".modal-content").scrollTop(0);
      this.addFarmAndFieldsModal.hide();
      this.getFieldDetailsByUserId();
    })
  }

  salesTypeList;
  getSalesType() {
    let url = serviceUrl + "SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSalesType?Request=GetSalesType"
    console.log();
    this.spinnerService.show();
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      console.log("res", data);
      this.salesTypeList = data.json() ? data.json().SaleType : [];
    },
      (err) => {
        this.salesTypeList = [];
      });
  }

  salesOriginDestList;
  getSalesOriginDest() {
    let url = serviceUrl + "SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSaleOriginDestinations?Request=GetSaleOrigin"
    console.log();
    this.spinnerService.show();
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      console.log("res", data);
      this.salesOriginDestList = data.json() ? data.json().SiteOrigin : [];
    },
      (err) => {
        this.salesOriginDestList = [];
      });
  }

  salesPurchaseRefDataList;
  tolaranceRefDataList = [];
  tradeDisputeRuleRefDataList = [];
  transferredConfirmedByRefDataList = [];
  transferDoneByRefDataList = [];
  weightTermsRefData = [];
  rCTIRefDataList = [];
  paymentTermsRefData = [];
  gTAContractRefData = [];
  gradeStandardsRefData = [];
  gSTRefData = [];
  deliveryTermsRefData = [];
  arbitrationRuleRefData = [];
  contractTypeRefData = [];
  getSalesAndPurchaseRefDataOriginDest() {
    // http://129.154.71.56:9073/SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSalesAndPurchaseReferenceData?SaleType=1&SiteOrigin=4&Commodity=Cotton
    let url = serviceUrl + "SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSalesAndPurchaseReferenceData?SaleType=" + this.salesPurchase.SaleType.Name + "&SiteOrigin=" + this.salesPurchase.saleOrigin.Id + "&Commodity=" + this.salesPurchase.commodity
    // console.log();
    this.spinnerService.show();
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      console.log("res", data);
      this.salesPurchaseRefDataList = data.json() ? data.json() : [];
      this.businessCreditRefData = this.salesPurchaseRefDataList.BusinessCreditedRefData ? this.salesPurchaseRefDataList.BusinessCreditedRefData.RefData : [];
      this.tolaranceRefDataList = this.salesPurchaseRefDataList.ToleranceRefData ? this.salesPurchaseRefDataList.ToleranceRefData.RefData : [];
      this.tradeDisputeRuleRefDataList = this.salesPurchaseRefDataList.TradeDisputeRuleRefData ? this.salesPurchaseRefDataList.TradeDisputeRuleRefData.RefData : [];
      this.transferredConfirmedByRefDataList = this.salesPurchaseRefDataList.TransferredConfirmedByRefData ? this.salesPurchaseRefDataList.TransferredConfirmedByRefData.RefData : [];
      this.transferDoneByRefDataList = this.salesPurchaseRefDataList.TransferDoneByRefData ? this.salesPurchaseRefDataList.TransferDoneByRefData.RefData : [];
      this.weightTermsRefData = this.salesPurchaseRefDataList.WeightTermsRefData ? this.salesPurchaseRefDataList.WeightTermsRefData.RefData : [];
      this.rCTIRefDataList = this.salesPurchaseRefDataList.RCTIRefData ? this.salesPurchaseRefDataList.RCTIRefData.RefData : [];
      this.paymentTermsRefData = this.salesPurchaseRefDataList.PaymentTermsRefData ? this.salesPurchaseRefDataList.PaymentTermsRefData.RefData : [];
      this.gTAContractRefData = this.salesPurchaseRefDataList.GTAContractRefData ? this.salesPurchaseRefDataList.GTAContractRefData.RefData : [];
      this.gradeStandardsRefData = this.salesPurchaseRefDataList.GradeStandardsRefData ? this.salesPurchaseRefDataList.GradeStandardsRefData.RefData : [];
      this.gSTRefData = this.salesPurchaseRefDataList.GSTRefData ? this.salesPurchaseRefDataList.GSTRefData.RefData : [];
      this.deliveryTermsRefData = this.salesPurchaseRefDataList.DeliveryTermsRefData ? this.salesPurchaseRefDataList.DeliveryTermsRefData.RefData : [];
      this.arbitrationRuleRefData = this.salesPurchaseRefDataList.ArbitrationRuleRefData ? this.salesPurchaseRefDataList.ArbitrationRuleRefData.RefData : [];
      this.contractTypeRefData = this.salesPurchaseRefDataList.ContractTypeRefData ? this.salesPurchaseRefDataList.ContractTypeRefData.RefData : [];
    },
      (err) => {
        this.salesPurchaseRefDataList = [];
      });
  }

  commodityOriginList;
  getCommodityOrigin() {
    let user_id = this.currentUser.USER.U_A_REGION;
    // https://129.154.71.56:9074/AgvantageGetCommodityOriginSB/AgvantageGetCommodityOriginRestPS/api/v1/GetCommodityOrigin?seller_user_id=1
    // let url = `${serviceUrl}AgvantageGetSitesSB/AgvantageGetSitesRestPS/api/v1/GetSitesList?user_id=${this.currentUser.USER.U_ID}&storage_type=${this.salesPurchase.saleOrigin.Name}&state_id=${this.currentUser.USER.U_A_STATE}`;
    let url = `${serviceUrl}AgvantageGetCommodityOriginSB/AgvantageGetCommodityOriginRestPS/api/v1/GetCommodityOrigin?seller_user_id=${this.salesPurchase.sellerRepresentative.UserId}`;
    this.spinnerService.show();
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      console.log("res", data);
      this.commodityOriginList = data.json() ? data.json().Storage.Storage_Name : [];
    },
      (err) => {
        this.commodityOriginList = [];
      });
  }
  getBrokerOffice() {
    let url = serviceUrl + "AgvantageGetBrokerOfficeSB/AgvantageGetBrokerOfficeRestPS/api/v1/GetBrokerOffice?broker_name=Agvantage Commodities Pty Ltd";
    this.http.get(url).subscribe((data) => {
        this.brokerOffices = data.json() ? data.json().broker_offices.broker_office : [];
    }),
        (err) => {
            this.brokerOffices = [];
        }
}

showCommDestInput=false; showCommDestDDL=true; showTolerance = true;
showCommDestination(){  
  if(this.salesPurchase.saleOrigin.Name === "DelieveredDestination"){    
    this.showCommDestInput = true;
    this.showCommDestDDL = false;
  }else{    
    this.showCommDestInput=false; 
    this.showCommDestDDL=true;
  }

  if(this.salesPurchase.saleOrigin.Name==="NTP" && this.salesPurchase.SaleType.Name==="Forward"){
    this.showNTPDetails = true;
    this.showTolerance = false;
    this.getNTPSiteList();
  }else{
    this.showTolerance = true;
    this.showNTPDetails = true;
  }

}

  commodityDestination;
  getCommodityDestination() { 
        
    let url = `${serviceUrl}AgvantageGetSitesSB/AgvantageGetSitesRestPS/api/v1/GetSitesList?storage_type=${this.salesPurchase.saleOrigin.Name}&state_id=${this.salesPurchase.sellerRepresentative.StateId}`;
    this.spinnerService.show();
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();      
      this.commodityDestination = data.json() ? data.json().getSites : [];      
    },
      (err) => {
        this.commodityDestination = [];
      });
  }

  commodities;
  getCommodities() {
    this.spinnerService.show();
    let url = serviceUrl + "AgVantageGetCommoditiesSB/getCommodities/api/v1/getCommodity?getCommodities=true";
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.commodities = data.json().CommodityCollection.Commodity;
    })
  }


  callGrades() {
    this.getCommodityGrade(this.salesPurchase.commodity);
    this.getSalesAndPurchaseRefDataOriginDest();
  }

  grades;
  getCommodityGrade(id) {
    this.spinnerService.show();
    let url = serviceUrl + "AgVantageGetCommodityGrade/AgVantageGetGradePS/api/v1/getGrade?C_ID=" + id;
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.grades = data.json().gradeCollection.grade;
    })
  }

  cropYearList;
  getCropyearList() {
    this.spinnerService.show();
    let url = serviceUrl + 'AgvGetCropYear/GetCropYear/api/v1/getCropYear?getcropyer=true';
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.cropYearList = data.json().CropYear;
    })
  }
  getSaleHistory() {
    let url = `${serviceUrl}SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSalesAndPurchaseHistoryByUserId?U_ID=${this.currentUser.USER.U_ID}&U_Type=${this.currentUser.USER.U_TYPE}`;
    this.http.get(url).subscribe((data) => {
      this.saleHistoryData = data.json() ? data.json().SaleHistory : [];
    },
      (err) => {
        this.saleHistoryData = [];
      });
  }
  submitSalesAndPurchaseMsg;
  submitSalesAndPurchase() {
    let params = {
      "SaleDate": new Date(this.salesPurchase.date),
      "SaleType": this.salesPurchase.SaleType.Id,
      "SaleOrigin": this.salesPurchase.saleOrigin.Id,
      "Commodity": this.salesPurchase.commodity,
      "BuyerName": this.salesPurchase.buyerTradingName,
      "SellerName": this.salesPurchase.sellerTradingName,
      "SellerRepresentative": this.salesPurchase.sellerRepresentative.UserId,
      "BuyerRepresentative": this.salesPurchase.buyersRepresentative,
      "BusinessCreditedTo": this.salesPurchase.businessCreditedTo,
      "OfficeCreditedTo": this.salesPurchase.officeCreditedTo,
      "ProducerBrokerTrader": this.salesPurchase.producer,
      "ReferenceNo": this.salesPurchase.refNumber,
      "FFS": this.salesPurchase.FFS,
      "FFSInvoicedTo": this.salesPurchase.FFSInvoiced,
      "Variety": this.salesPurchase.variety,
      "Grade": this.salesPurchase.grade,
      "CropYear": this.salesPurchase.cropYear,
      "ContractNo": this.salesPurchase.contractNo,
      "ContractType": this.salesPurchase.contractType,
      "Quantity": this.salesPurchase.quantity,
      "ContractPrice": this.salesPurchase.contractPrice,
      "Discounts": this.salesPurchase.discounts,
      "Increments": this.salesPurchase.increment,
      "Area": this.salesPurchase.areahec,
      "MinYield": this.salesPurchase.minyuphec,
      "MaxYield": this.salesPurchase.maxyuphec,
      "MinQuantity": this.salesPurchase.minQuantity,
      "MaxQuantity": this.salesPurchase.maxQuantity,
      "Tolerance": this.salesPurchase.toleranceList,
      "DeliveryTerms": this.salesPurchase.delvTerms,
      "PaymentTerms": this.salesPurchase.payTermsRefData,
      "CommodityOrigin": this.salesPurchase.commodityOrigin,
      "CommodityDestination": this.salesPurchase.commodityDestination,
      "FarmingRegion": this.salesPurchase.farmRegion,
      "AdditionalFreight": this.salesPurchase.addFreight,
      "DeliveryPeriodFrom": new Date (this.salesPurchase.delPeriodFrom),
      "DeliveryPeriodTo": this.salesPurchase.delPeriodTo,
      "WeightTerms": this.salesPurchase.weightTerms,
      "ArbitrationRules": this.salesPurchase.rulesArb,
      "GradeStandards": this.salesPurchase.gradeStd,
      "GTAContractNo": this.salesPurchase.gtaContractNo,
      "GST": this.salesPurchase.gst,
      "BuyerSuppliedRCTI": this.salesPurchase.buyerRCTI,
      "SellerTaxInvoice": this.salesPurchase.sellerRCTI,
      "TransferDoneBy": this.salesPurchase.transtobedoneby,
      "VendorDeclarationReqd": this.salesPurchase.vendorDecl,
      "Mortgage": this.salesPurchase.mortgage,
      "Encumbrance": this.salesPurchase.encumb,
      "PlantBreeders": this.salesPurchase.plactBreedRight,
      "Royalties": "string",
      "SecurityInterest": this.salesPurchase.regUnregSecIntrst,
      "DisclosureComment": this.salesPurchase.briefDetails,
      "TradeDisputeResolutionRule": this.salesPurchase.incTradeAndDispResRule,
      "ContractConditions": this.salesPurchase.contractCondtn,
      "Status": this.salesPurchase.status,
      "OKToProcess": this.salesPurchase.okToProcess,
      "ConfirmationRevenue": this.salesPurchase.confirmRevenue,
      "BankDetailsToSeller": this.salesPurchase.bankDetailsToSeller,
      "ReceivedConfirmationFromSeller": this.salesPurchase.confirmationRecFrmSeller,
      "SellerRCTIReturnedToBuyer": this.salesPurchase.sellerRCTIReturnBuyer,
      "ContractReceivedFromBuyer": this.salesPurchase.contractRecFrmBuyer,
      "ContractSentToSeller": this.salesPurchase.contractSentToSeller,
      "ReceivedContractFromSeller": this.salesPurchase.recContractFrmSeller,
      "TransferConfirmedBy": this.salesPurchase.transConfirmBy,
      "UnitsDeliveredToBuyer": this.salesPurchase.unitsDelvToBuyer,
      "FinalDeliveryData": this.salesPurchase.finalDelvDate,
      "OKToInvoice": this.salesPurchase.okToInvoice,
      "ConfirmationRevenueInclGST": this.salesPurchase.confrmRevenueInclGST,
      "ConfirmationRevenueExclGST": this.salesPurchase.confrmRevenueExclGST,
      "InvoiceNo": this.salesPurchase.invoiceNo,
      "InvoiceDate": new Date(this.salesPurchase.invoiceDate),
      "InvoiceReceived": this.salesPurchase.invoiceReceivedAndClosed,
      "U_ID": this.currentUser.USER.U_ID
    }
    console.log("params", params);
    this.spinnerService.show();
    this.headers = new Headers({
      'Content-Type': 'application/json; charset=UTF-8',
      'Accept': 'application/json'
    });
    this.options = new RequestOptions({ headers: this.headers });

    let url = serviceUrl + "SalesAndPurchase/SalesAndPurchasePS/api/v1/SubmitSalesAndPurchaseData";

    this.http.post(url, params, this.options).subscribe((data) => {
      this.spinnerService.hide();
      this.submitSalesAndPurchaseMsg = data.json().Response;
      console.log('resp', data.json());
      this.getSaleHistory();
      // this.submitAddffForcast = data.json().Outcome;
      this.salesAndPurchaseModalForm.reset();
      // this.broadCaster.broadcast('Save Event', 'Saved Successfully.');
      // $(".modal-content").scrollTop(0);
      this.salesAndPurchaseModal.hide();
    },(err) => {
      this.spinnerService.hide();
       console.log("Error: submit sales purchase");
    })
  }
}
