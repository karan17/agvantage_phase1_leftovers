import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewStorageGridComponent } from './view-storage-grid.component';

describe('ViewStorageGridComponent', () => {
  let component: ViewStorageGridComponent;
  let fixture: ComponentFixture<ViewStorageGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewStorageGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewStorageGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
