import {
  Component, OnInit, NgZone, ViewChild, ElementRef, Input, Output, EventEmitter,
  ComponentFactoryResolver
} from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import { FormControl } from "@angular/forms";
import { UserRoleService } from '../user-role.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Broadcaster } from '../broadcaster';
import { serviceUrl } from "../app.module";
import { GridOptions } from "ag-grid/main";

declare var $: any;

declare var google;
declare var $: any;

@Component({
  selector: 'app-view-storage-grid',
  templateUrl: './view-storage-grid.component.html',
  styleUrls: ['./view-storage-grid.component.scss']
})

export class ViewStorageGridComponent implements OnInit {
  columnDefs2;
  columnDefs3;
  columnDefs4;
  url: string;
  headers: Headers;
  option: RequestOptions;
  summaryArr: any = [];
  commodities: any;
  WarehouseTypes: any;
  PublicList: any;
  OnfarmList: any;
  ExfarmList: any;
  inventory: any;
  targetprice: any;
  quantity: any;
  quality: any;
  qualityList: any;
  qualityValue: any;
  grade: any;
  details: any;
  autocomplete;
  location: any;
  loadList: any;
  fieldFrom: any;
  dateHarvested: any;
  fumigationORChemical: any;
  chemicalRatesApplied: any;
  dateApplied: any;
  loc;
  showExfarm: boolean = false;
  showOnfarm: boolean = false;
  showPublic: boolean = false;
  showChemRates: boolean = true;
  showDtApp: boolean = true;
  showCreateIcon: boolean = true;
  showRmvIcon: boolean = true;
  ihName;
  ihLocation;
  ofName;
  ofLocation;
  storageLocation;
  exfarm;
  onfarm;
  addStorage;
  storageName;
  StorageDistance;
  storageDirection
  storageLocationGPS;
  storageLocationDistance;
  storageLocationDirection;
  storageLocationNearestTown;
  storageLocationAddress;
  storageLocationCity;
  storageLocationState;
  storageLocationPostCode;
  storageLocationCountry;
  address; city; state; country; zipcode; neartown;
  public latitude: number;
  public longitude: number;
  public searchControl: FormControl;
  public zoom: number;
  map;
  service;
  infoWindow;
  sourceBox; destBox;
  cropYearList;
  componentContext: any;
  getRowNodeId;
  rowSelection: string;


  @ViewChild("search")
  public searchElementRef: ElementRef;

  @ViewChild("sourceSearch")
  public sourceSearchElementRef: ElementRef;

  @ViewChild("destiSearch")
  public destiSearchElementRef: ElementRef;

  @ViewChild("mapDiv")
  public mapDiv: ElementRef;

  @ViewChild("commMovmntRecordModel")
  public commMovmntRecordModel;
  // @ViewChild("gpslocation")
  // public gpsloc: ElementRef;

  // public searchControl: FormControl;
  @ViewChild('addInventoryForm') public addInventoryForm;
  @ViewChild('addcommodityDetails') public addcommodityDetails;
  @ViewChild('addMarketingDetails') public addMarketingDetails;
  @ViewChild('myModal') public myModal;
  @ViewChild('noBDModaSummary') public noBDModaSummary;
  @ViewChild('ForwardSaleHistory') public ForwardSaleHistory;
  @ViewChild('location')
  // public locationElementRef: ElementRef;

  public options = { types: ['address'], componentRestrictions: { country: 'AUS' } };
  bsValue1: Date = new Date();
  bsValue2: Date = new Date();
  // @ViewChild("alertContainer", { read: ViewContainerRef }) container;
  componentRef; inputSource; inputDest;
  srcsrch; destsrch;
  dist: any; duration: any;
  EXFARM_FLAG = ''; ONFARM_FLAG = ''; ADDSTORAGE_FLAG = '';
  farmDetails: any;
  truckDetails: any;
  gridOptions: GridOptions;
  gridOptionsInvComm: GridOptions;
  params;
  gridApi;
  gridApiInvComm;
  gridColumnApiInvComm;
  commMvmtReccolumnDef;
  columnDefInvCommodity;
  quantities: any;
  addRowDisable;
  addRow: any;
  constructor(private router: Router, private role: UserRoleService, private _ngZone: NgZone,
    private http: Http, public spinnerService: Ng4LoadingSpinnerService, private broadCaster: Broadcaster,
    private resolver: ComponentFactoryResolver) {
    this.inventory = {};
    this.exfarm = {};
    this.onfarm = {};
    this.addStorage = {};
    this.rowSelection = "single";
    this.gridOptions = <GridOptions>{
      onGridReady1: (params) => {
        this.params = params;
        this.gridApi = params.api;
        this.gridOptions.api.sizeColumnsToFit();
        // this.getStorageRecord();
      }
    };

    this.gridOptionsInvComm = <GridOptions>{
      onGridReadyInvComm: (params) => {
        // this.params=params;
        // this.gridApiInvComm = params.api;
        // // this.gridColumnApiInvComm = params.columnApi;
        // this.gridOptionsInvComm.api.sizeColumnsToFit();
        // this.setTruckGrid();
        // this.getStorageRecord();
      },

      defaultColDef() {
        // editable: true
      },
      onRowEditingStarted(event) {
        console.log('never called - not doing row editing');
      },

      onRowEditingStopped(event) {
        console.log('never called - not doing row editing');
      }
    };

    this.commMvmtReccolumnDef = [
      { headerName: "StorageType", field: "StorageType" },
      { headerName: "City", field: "City" },
      { headerName: "DistanceFromCity", field: "DistanceFromCity", headerClass: "text-center" },
      { headerName: "CollectionDate", field: "CollectionDate" },
      { headerName: "StorageName", field: "StorageName" },
      { headerName: "DirectionFromCity", field: "DirectionFromCity" },
      { headerName: "DriverName", field: "DriverName" },
      { headerName: "State", field: "State" }
    ];

    this.columnDefs2 = [
      { headerName: "Quantity", field: "Quantity" },
      { headerName: "Protein", field: "Protein", headerClass: "text-center" },
      { headerName: "Moisture", field: "Moisture" },
      { headerName: "Screenings", field: "Screenings" },
      { headerName: "TestWeight", field: "TestWeight" },
      { headerName: "ETC", field: "ETC" }
    ];

    this.columnDefs3 = [
      { headerName: "Action", checkboxSelection: true, pinned: 'left' },
      { headerName: "ContractNumber", field: "ContractNo", headerClass: "text-center" },
      { headerName: "Commodity", field: "Commodity" },
      { headerName: "Grade", field: "Grade" },
      { headerName: "CropYear", field: "CropYear" },
      { headerName: "SoldQuantity", field: "ContractQuantity" },
      { headerName: "Buyer", field: "Buyer" }
    ];

    this.columnDefs4 = [
      { headerName: "Action", field: "Action" },
      { headerName: "ContractNumber", field: "ContractNumber", headerClass: "text-center" },
      { headerName: "Commodity", field: "Commodity" },
      { headerName: "Grade", field: "Grade" },
      { headerName: "CropYear", field: "CropYear" },
      { headerName: "SoldQuantity", field: "SoldQuantity" },
      { headerName: "Buyer", field: "Buyer" }
    ];

    this.columnDefInvCommodity = [
      { headerName: "Quantity", field: "Quantity" },
      { headerName: "Protein", field: "Protein" },
      { headerName: "Moisture", field: "Moisture", headerClass: "text-center" },
      { headerName: "Screening", field: "Screening" },
      { headerName: "TestWeight", field: "TestWeight" },
      { headerName: "Etc", field: "Etc" }

    ];



    // this.gpsmap = document.getElementById('mapGPS');
  }

  onGridReadyForwardSell(params) {
    this.getRowNodeId = function (data) {
      return data.id;
    };
  }

  onGridReadyInvComm(params) {
    this.gridApiInvComm = params.api;
    this.gridColumnApiInvComm = params.columnApi;
    this.getRowNodeId = function (data) {
      return data.id;
    };
    this.setTruckGrid();

  }
  currentUser; gpsmap; gpsloctext;
  ngOnInit() {
    this.searchControl = new FormControl();
    this.currentUser = this.role.getUser();
    this.addRowDisable = true;
    console.log('this.currentUser', this.currentUser);
    this.getCommodities();
    this.getWarehouseTypes();
    this.getLoadList();
    this.getWareHouseList();
    this.getInvFieldFromDetails()
    this.getChemicalList();
    this.getCropyearList();
    this.getOnfarmWarehouseList();
    this.getPublicWarehouseList();
    this.getCommodityMovementRecordGrid();
    this.getQualityList();
    this.getSalesPurchaseHistoryByIDGrid();
    // this.getCommodityGrade();
    // this.loc = this.locationElementRef.nativeElement;
    // this.gpsloctext = this.gpsloc.nativeElement;
    this.loc = new google.maps.places.SearchBox(this.loc);
    console.log(this.loc);
    //set google maps defaults
    this.zoom = 4;
    //create search FormControl
    this.searchControl = new FormControl();
    this.infoWindow = new google.maps.InfoWindow;
    // this.setCurrentPosition('');
    // this.getGPSLocation();
    // google.maps.event.addDomListener(window, "load", this.getGPSLocation);
    // this.gpsmap = document.getElementById('mapGPS');
  }

  openCommMovementRecord() {
    this.commMovmntRecordModel.show();
  }

  closeCommMovementRecord() {
    this.commMovmntRecordModel.hide();
  }

  tabs = [
    { title: 'Dynamic Title 1', content: 'Dynamic content 1', active: true, disabled: false },
    { title: 'Dynamic Title 2', content: 'Dynamic content 2', active: false, disabled: true },
    { title: 'Dynamic Title 2', content: 'Dynamic content 3', active: false, disabled: true },
    { title: 'Dynamic Title 2', content: 'Dynamic content 4', active: false, disabled: true },
    { title: 'Dynamic Title 2', content: 'Dynamic content 5', active: false, disabled: true },
    { title: 'Dynamic Title 2', content: 'Dynamic content 6', active: false, disabled: true }
  ];

  selectNextTab(index) {
    this.tabs[index].active = true;
    this.tabs[index].disabled = false;
  }

  selectBackTab(index) {
    this.tabs[index].active = true;
    // this.tabs[index].disabled = false;        
  }

  closeFwdSaleModal() {
    this.ForwardSaleHistory.hide();
  }

  openFwdSaleModal() {
    this.ForwardSaleHistory.show();
  }

  onSelectionForwardSell(param) {
    if (param.node.selected) {
      console.log("row selection fwd sell", param.data);
      // this.enableNext = false;
      this.inventory.SoldQuantity = param.data.ContractQuantity;
      this.inventory.BuyerName = param.data.Buyer;
      this.inventory.Grade = param.data.Grade;
      this.inventory.ContractNo = param.data.ContractNo;
      this.inventory.Quantity = param.data.ContractQuantity;
      this.inventory.SystemRefNo = param.data.ReferenceNo;
    }
  }

  removeSelectedFwdSale() {
    this.inventory.BuyerName = "";
    this.inventory.Grade = "";
    this.inventory.ContractNo = "";
    this.inventory.Quantity = "";
    this.inventory.SystemRefNo = "";
  }

  /* Show current position */
  setCurrentPosition(flag) {

    console.log('--- flag', flag);

    this.initMap(this);

    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        console.log("setCurrentPosition ", position);
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
        if (flag == 'fromExfarm') {
          this.getGPSLocation(this, flag);
          this.EXFARM_FLAG = flag;
        }
        else if (flag == 'fromAddStorage') {
          this.getGPSLocation(this, flag);
          //  this.ADDSTORAGE_FLAG = flag
        }
        else if (flag == 'fromOnfarm') {
          this.getGPSLocation(this, flag);
          this.ONFARM_FLAG = flag;
        }
        else if (flag == 'fromInvAddStrgDtls') {
          this.getInvStrgDtlsGPSLocation(this, flag);

        }

        // this.initMap(this);
        // google.maps.event.addListener(window, 'load', this.initialize());
        // this.initializeAutocomplete();
      });
    }
  }

  place;
  getAddress(place: Object) {
    console.log("Address", place);
    this.place = place;
  }


  // getLocation(){
  //   console.log("get location");
  //   this.autocomplete = new google.maps.places.Autocomplete(document.getElementById('location'));
  //   this.autocomplete.addListener('place_changed');
  // }

  gotoHome() {
    this._ngZone.run(() => this.router.navigate(['dashboard']));
  }

  getCommodities() {
    this.url = serviceUrl + "AgVantageGetCommoditiesSB/getCommodities/api/v1/getCommodity?getCommodities=true";
    this.http.get(this.url).subscribe((data) => {
      this.commodities = data.json().CommodityCollection.Commodity;
      // this.selectedCommodity1=this.commodities[0].C_NAME;
      console.log(this.commodities);
    })
  }
  commMovRecordRowData;
  getCommodityMovementRecordGrid() {
    console.log("comm movmnt this.currentUser", this.currentUser);
    this.url = serviceUrl + "CommodityMovementSB/CommodityMovementPS/api/v1/GetCommodityMovementHistory?U_ID=261"//+this.currentUser.USER.U_ID;
    this.http.get(this.url).subscribe((data) => {
      console.log("comm mvmt rec: ", data);
      if (data.json() != null) {
        this.commMovRecordRowData = data.json().CommodityMovementHistory;
      } else {
        this.commMovRecordRowData = [];
        // this.gridOptions1.api.setRowData([]);           
      }
      // console.log("comm mvmt rec: ",this.commodities);
      // this.selectedCommodity1=this.commodities[0].C_NAME;
      // console.log(this.commodities);
    })
  }
  historyByBrokerIDRowData;
  getSalesPurchaseHistoryByIDGrid() {

    this.url = serviceUrl + "SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSalesAndPurchaseHistoryByUserId?U_ID=" + this.currentUser.USER.U_ID + "&U_Type=" + this.currentUser.USER.U_TYPE;
    this.http.get(this.url).subscribe((data) => {
      console.log("getSalesPurchaseHistoryByIDGrid: ", data);
      if (data.json() != null) {
        this.historyByBrokerIDRowData = data.json().SaleHistory;
      } else {
        this.historyByBrokerIDRowData = [];
        // this.gridOptions1.api.setRowData([]);           
      }
      // console.log("comm mvmt rec: ",this.commodities);
      // this.selectedCommodity1=this.commodities[0].C_NAME;
      // console.log(this.commodities);
    })
  }

  updateSummary(event) {
    this.summaryArr = event;
    console.log(this.summaryArr);
  }
  openSummary() {
    this.noBDModaSummary.show();
  }
  exportSummary() {
    console.log("export summary");
  }
  vcmRecord() {
    console.log("vcmRecord");
  }

  getWarehouseTypes() {
    console.log('this.role.getRole(): ', this.role.getRole());
    // if (this.role.getRole() === "superadmin") {
    //   this.WarehouseTypes = [
    //     { T_ID: '1', T_NAME: 'On Farm' },
    //     { T_ID: '2', T_NAME: 'Ex Farm' }
    //   ]
    // } else if (this.role.getRole() === "P") {
    this.WarehouseTypes = [
      { T_ID: '1', T_NAME: 'On Farm' },
      { T_ID: '2', T_NAME: 'Public' }
    ]
    // }
  }

  s_type; storageDetailsList;
  callStorageDetails() {
    console.log('call storage dtls', this.inventory.Storage_Type);
    if (this.inventory.Storage_Type === "On Farm") {
      this.s_type = "OFW"
    } else {
      this.s_type = "PW"
    }
    this.spinnerService.show();
    this.url = serviceUrl + "AgcInvgetStorage/AgvInvGetStoragePS/api/v1/getStorage?store_type=" + this.s_type + "&u_id=" + this.currentUser.USER.U_ID;
    this.http.get(this.url).subscribe((data) => {
      this.spinnerService.hide();
      console.log("strg detls: ", data);
      this.storageDetailsList = data.json().store;
      // console.log("comm mvmt rec: ",this.commodities);
      // this.selectedCommodity1=this.commodities[0].C_NAME;
      // console.log(this.commodities);
    })
  }
  storageFieldDetails;
  callStrgFieldDetails() {
    console.log('strgObj', this.inventory.StorageName);
    this.spinnerService.show();
    this.url = serviceUrl + "AgvInvGetStorageByIDSB/AgvInvGetStorageByIDPS/api/v1/getStoreByID?S_ID=" + this.inventory.STORAGE.STORAGE_ID;
    this.http.get(this.url).subscribe((data) => {
      this.spinnerService.hide();
      console.log("strg detls: ", data);
      // this.inventory=data.json(); 

      // console.log("--- selected storageFieldDetails : ",this.inventory);
      // this.inventory.STORAGE_NAME = data.json().STORAGE_NAME;
      this.inventory.STORAGE_NEAR_TOWN = data.json().STORAGE_NEAR_TOWN;
      this.inventory.STORAGE_LOCA_DISTANCE = data.json().STORAGE_LOCA_DISTANCE;
      this.inventory.STORAGE_LOC_DIRECTION = data.json().STORAGE_LOC_DIRECTION;
      this.inventory.U_A_STREET = data.json().U_A_STREET;
      this.inventory.U_A_STATE = data.json().U_A_STATE;
      this.inventory.U_A_COUNTRY = data.json().U_A_COUNTRY;
      this.inventory.U_A_POSTCODE = data.json().U_A_POSTCODE;
      this.inventory.STORAGE_LOC_GPS = data.json().STORAGE_LOC_GPS;

      // this.selectedCommodity1=this.commodities[0].C_NAME;
      // console.log(this.commodities);
    })
  }

  fieldFromList = [];
  getInvFieldFromDetails() {

    this.spinnerService.show();
    this.url = serviceUrl + "AgvantageGetFieldDetailsSB/AgvantageGetFieldDetailsRestPS/api/v1/GetFieldDetails?user_id=" + this.currentUser.USER.U_ID;
    this.http.get(this.url).subscribe((data) => {
      this.spinnerService.hide();
      console.log("--- Field from list: ", data);
      this.fieldFromList = data.json().FieldDetails;
      // console.log("--- selected storageFieldDetails : ",this.inventory);

      // console.log(this.commodities);
    })
  }

  // load=0;truckLoadData=[];
  // addCommGridRow() {
  //   this.load=this.load+1;
  //   let obj = {
  //     ContractNo: this.addCommMovmtRec.SelectContractNo,
  //     Buyer: this.addCommMovmtRec.Buyer, CommodityCollected: this.addCommMovmtRec.CommCollected,
  //     Grade: this.addCommMovmtRec.Grade, TruckType: this.addCommMovmtRec.TruckType, 
  //     EstimatedWeight: this.addCommMovmtRec.EstWeight, DeliveryCompanyName:this.addCommMovmtRec.DeliveryCompName,
  //     DriverName:this.addCommMovmtRec.DriverName, TruckRegNo:this.addCommMovmtRec.TruckRegNumber,
  //     Comments:this.addCommMovmtRec.Comments
  //   }
  //   this.gridApiTruck.updateRowData({ add: [obj] });

  //   this.truckLoadData.push(obj);
  // }

  getWareHouseList() {
    this.PublicList = [
      { PB_ID: '1', PB_NAME: 'public abcd' },
      { PB_ID: '2', PB_NAME: 'public syd' }
    ];

    this.OnfarmList = [
      { OF_ID: '1', OF_NAME: 'OnFarm syd' },
      { OF_ID: '2', OF_NAME: 'OnFarm asdf' }
    ];

    this.ExfarmList = [
      { IH_ID: '1', IH_NAME: 'exfarm asdf' },
      { IH_ID: '2', IH_NAME: 'exfarm qwee' }
    ];
  }

  getCropyearList() {
    let url = serviceUrl + 'AgvGetCropYear/GetCropYear/api/v1/getCropYear?getcropyer=true';
    this.http.get(url).subscribe((data) => {
      // console.log('crp year data:',data.json().CropYear);
      this.cropYearList = data.json().CropYear;
      // this.selectedCommodity1=this.commodities[0].C_NAME;
      // console.log(this.commodities);
    })
  }

  getQualityList() {
    // this.url = serviceUrl+"AgvInvengetQuality/AvgInvGetQualityPS/api/v1/getQuality?C_ID="+c_id;    
    // this.http.get(this.url).subscribe((data)=>{
    //   // console.log('data:',data);
    //   this.qualityList= data.json().Quality;

    //   // this.selectedCommodity1=this.commodities[0].C_NAME;
    //   // console.log(this.commodities);
    // })
    this.qualityList = [
      { q_id: '1', q_name: 'Est. Quantity & Avg. Quality' },
      { q_id: '2', q_name: 'Est. Quantity & Avg. Quality - Load by Load' }
    ];
  }

  chemicalList;
  getChemicalList() {
    this.url = serviceUrl + "AgvInvGetChemical/AgvInvGetChemPS/api/v1/getchemicals?getChemical=true";
    this.http.get(this.url).subscribe((data) => {
      console.log('chemical data:', data);
      this.chemicalList = data.json().Chemicals;
      // this.chemicalList.Chemicals = {"CHEM_ID" : 0,    "CHEM_NAME" : "None"};
      // this.selectedCommodity1=this.commodities[0].C_NAME;
      // console.log(this.commodities);
    })

  }

  callSelectedChemical(i) {
    if (i == 0) {
      this.chemArray.forEach((chem) => {
        if (chem.chemical_name == 'None') {
          this.showChemRates = false;
          this.showDtApp = false;
          this.showCreateIcon = false;
          // this.showRmvIcon = false;
        }
      });
    } else {
      this.chemArray.forEach((chem) => {
        if (chem.chemical_name == 'None') {
          this.showChemRates = false;
          this.showDtApp = false;
          // this.showCreateIcon = false;
          this.showRmvIcon = false;
        }
      });
    }



    this.showDtApp = false;
    this.showChemRates = false;
  }
  callSelectedType() {
    console.log('selected warehouse type: ', this.inventory.WarehouseType);
    if (this.inventory.WarehouseType == 1) {
      this.showExfarm = false;
      this.showPublic = false;
      this.showOnfarm = true;
    } else if (this.inventory.WarehouseType == 2) {
      if (this.role.getRole() === "superadmin") {
        this.showExfarm = true;
      }
      else {
        // if (this.role.getRole() === "p"){
        this.showPublic = true;
        console.log('--- show public : ', this.showPublic);
        // }
      }
      this.showOnfarm = false;
      console.log('--- show showOnfarm : ', this.showOnfarm);
    }
    // this.getWareHouseData(this.inventory.WarehouseType); 
  }

  callselectedQuality() {
    console.log('selected quality', this.inventory.Quality);
  }

  // qualityArray = [{disable:false}]; "quality_name" : "string",
  qualityArray = [{ quality_name: '', quality_value: '' }];
  hideAddButton: boolean = true;
  createNewQualityFields(obj, index) {

    let newObj = Object.assign({}, obj);
    // obj.disable = true;
    if (this.qualityArray.length < 11) {
      newObj.qualityName = '';
      newObj.qualityValue = '';
      this.qualityArray.push(newObj);
      console.log('create new quality fields...', this.qualityArray);
    }
  }

  removeNewQualityFields(obj, index) {
    console.log('obj', obj, 'i:', index);
    this.qualityArray.splice(index, 1);
  }
  // chemArray = [0];"chemical_name" : "string",



  chemArray = [{ chemical_name: '', chemical_value: '', date_applied: '' }];

  createNewChemFields() {
    let obj;
    // console.log(obj);
    let newChemObj = Object.assign({}, obj);
    if (this.chemArray.length < 11) {
      // newChemObj.chemical_name = undefined;
      // newChemObj.chemical_value = '';
      // newChemObj.date_applied = '';
      this.chemArray.push(newChemObj);
    }
    // this.chemArray.push(newChemObj);
    console.log('create new chemical fields...', this.chemArray);
  }

  removeNewChemFields(obj, index) {
    console.log('obj', obj, 'i:', index);
    // const idx: number = this.chemArray.indexOf(obj);
    this.chemArray.splice(index, 1);
  }

  // ngOnDestroy() {
  //   this.componentRef.destroy();
  // }


  load = 0; invCommGridRowData = [];


  setTruckGrid() {
    this.gridOptionsInvComm.api.setRowData([]);
    //setTimeout(()=>{this.gridOptionsTruck.api.sizeColumnsToFit();});

  }

  callGrades() {

    this.getCommodityGrade(this.inventory.SCC_C_ID);
  }
  addLoad($event) {
    if (this.inventory.QntytQlty === '2') {
      this.qualities.unshift({ Q_NAME: 'Load'},{Q_NAME : 'Quantity'});
    } else {
      this.qualities = this.qualities.map(data => {
        if (data.Q_NAME != 'Load') {
          return data;
        }
      })
    }
  }

  grades;
  getCommodityGrade(id) {
    this.url = serviceUrl + "AgVantageGetCommodityGrade/AgVantageGetGradePS/api/v1/getGrade?C_ID=" + id;
    this.spinnerService.show();
    this.http.get(this.url).subscribe((data) => {
      this.spinnerService.hide();
      // console.log('grade', data.json());
      this.grades = data.json().gradeCollection.grade;
    })
  }

  showMapPage;
  getDistance() {
    // this.setCurrentPosition(flag);
    this.inputSource = this.sourceSearchElementRef.nativeElement;
    this.inputDest = this.destiSearchElementRef.nativeElement;
    this.sourceBox = new google.maps.places.SearchBox(this.inputSource);
    this.destBox = new google.maps.places.SearchBox(this.inputDest);
    // this.initializeAutocomplete();
    // this.initMap();
  }

  getOnfarmWarehouseList() {
    console.log('inventory.Onfarm : ', this.inventory.Onfarm);
    let url = serviceUrl + 'AgcInvgetStorage/AgvInvGetStoragePS/api/v1/getStorage?store_type=OFW&u_id=' + this.currentUser.USER.U_ID;
    this.http.get(url).subscribe((data) => {
      console.log('storage data:', data);
      this.OnfarmList = data.json() ? data.json().store : [];
      // this.selectedCommodity1=this.commodities[0].C_NAME;
      // console.log(this.commodities);
    })
  }

  getLoadList() {
    console.log('inventory.Onfarm : ', this.inventory.Onfarm);
    let url = serviceUrl + 'AgvInvGetLoad/AgvInvGetLoadPS/api/v1/getLoad?input=true';
    this.http.get(url).subscribe((data) => {
      console.log('storage data:', data);
      this.loadList = data.json().Loadvalues;
      // this.selectedCommodity1=this.commodities[0].C_NAME;
      // console.log(this.commodities);
    })
  }


  getPublicWarehouseList() {
    console.log('inventory.Onfarm : ', this.inventory.Onfarm);
    let url = serviceUrl + 'AgcInvgetStorage/AgvInvGetStoragePS/api/v1/getStorage?store_type=PW&u_id=' + this.currentUser.USER.U_ID;
    this.http.get(url).subscribe((data) => {
      console.log('storage data:', data);
      this.PublicList = data.json() ? data.json().store : [];
      // this.selectedCommodity1=this.commodities[0].C_NAME;
      // console.log(this.commodities);
    })
  }

  @Output() save: EventEmitter<any> = new EventEmitter();
  submitInventory;
  changeUnsold() {
    this.inventory.unsold_quantity = this.inventory.unsold_quantity - this.inventory.sold_quantity;
  }

  
  addInventory() {
    console.log('Post Inventory json :', this.inventory);


    this.myModal.hide();

    //prepare quality for load params
    let loadQualityPayloadArr = [];

    let params: any = {
      "storage_id": this.inventory.STORAGE ? this.inventory.STORAGE.STORAGE_ID : '',
      "commodity_details": {
        "comm_id": this.inventory.SCC_C_ID,
        "grade_id": this.inventory.GradeCom,
        "quantity_quality_type": this.inventory.QntytQlty,
        "field_from": this.inventory.FieldFrom,
        "crop_year": this.inventory.Cropyear,
        "date_harvested": this.inventory.dateHarvested,

        
      },
      "marketing_details": {
        "sold_quantity": this.inventory.SoldQuantity,
        "total_unsold_quantity": this.inventory.TotalUnsoldQuantity,
        "target_price": this.inventory.TotalPrice,
        "delivery_date": this.inventory.DeliveryDate,
        "freight_cost": this.inventory.FreightCostFIL,
        "fumigation_cost": this.inventory.CarryFumigationCost,
        "chemical_attributes": {
          "chemical": this.chemArray
        }
      },
      "contract_details": {
        "sale_id": [
          this.inventory.ContractNo
        ]
      },
      "u_id": this.currentUser.USER.U_ID
    };

    if (this.inventory.QntytQlty == '2') {
      params.quality_by_load = {
        "Load": []
      }
      params.average_quality =  {
        "quantity": this.inventory.Quantity,
        "quality": []
      };
      let innerObj: any ;
      let loadAtributeAddition = 0;
      console.log(this.qualities);
      let start = false, end =  true;
      this.qualities.forEach(data => {

        if (data.Q_NAME == 'Load') {
          start= true;
          end = false;
          innerObj = {};
          innerObj.load_id = data.qualityVal;
          innerObj.quantity = data.quantity;
          innerObj.quality = [];
          params.quality_by_load.Load.push(innerObj);

        } 
        if (data.Q_NAME == 'Quantity') {
          innerObj.quantity = data.qualityVal;
        }
        else {
          innerObj.quality.push({ quality_name: data.Q_NAME, quality_value: data.qualityVal })
         
        }
        
        
      })
      console.log(innerObj);
    } else {
      this.qualities = this.qualities.map(data => {
        return {
          "quality_name": data.Q_NAME,
          "quality_value": data.qualityVal
        }
      });
      params.average_quality =  {
        "quantity": this.inventory.Quantity,
        "quality": this.qualities
      };
      params.quality_by_load = {
        "Load": []
      };
    }




    console.log('params', params);


    let URL = serviceUrl + 'AgvInvAddCommodity/AgvInvAddCommPS/api/v1/AddInv';
    //this.spinnerService.show();
    this.headers = new Headers({
      'Content-Type': 'application/json; charset=UTF-8',
      'Accept': 'application/json'
    });
    this.spinnerService.show();
    // this.option = new RequestOptions({ headers: this.headers });
    this.http.post(URL, params, new RequestOptions({ headers: this.headers })).subscribe((data) => {
      this.spinnerService.hide();
      console.log('--- add inv data.json() : ', data.json());
      this.submitInventory = data.json().outcome;
      this.addInventoryForm.reset();
      let res = { "msg": "Saved Successfully", "id": data.json().inventory_id };
      this.broadCaster.broadcast('Save Event', res);
      this.myModal.hide();

    },
      (err) => {
        console.log(err);
      })
  }

  openAddInventory() {
    this.addInventoryForm.reset();
    this.myModal.show();
    this.getOnfarmWarehouseList();
    this.getPublicWarehouseList();
  }
  submitStorage;
  @ViewChild('addStorageForm') public addStorageForm;
  @ViewChild('addInhouseForm') public addInhouseForm;
  @ViewChild('addOnfarmForm') public addOnfarmForm;

  @ViewChild('AddStorage') public AddStorage;
  @ViewChild('modalExfarm') public modalExfarm;
  @ViewChild('modalOnfarm') public modalOnfarm;

  setCity(event) {

    this._ngZone.run(() => this.addStorage.U_A_CITY = event)

  }

  setInvStgDtlsCity(event) {

    this._ngZone.run(() => this.addStorage.U_A_CITY = event)

  }

  openAddStorageModal() {
    this.getFarmDetails();
    this.getTrcukAccess();
    this.AddStorage.show();
  }
  openOnfarmModal() {
    this.modalOnfarm.show();
  }
  openExfarmModal() {
    this.modalExfarm.show();
  }

  closeAddStorageModal() {
    this.addStorageForm.reset();
    this.AddStorage.hide();
  }

  closeOnfarmModal() {
    this.addOnfarmForm.reset();
    this.modalOnfarm.hide();
  }

  closeExfarmModal() {
    this.addInhouseForm.reset();
    this.modalExfarm.hide();
  }



  addStorageInfo(context) {
    let params = {
      STORAGE_NAME: this.addStorage.StorageName,
      U_A_STREET: this.addStorage.U_A_STREET,
      U_A_POSTCODE: this.addStorage.U_A_POSTCODE,
      U_A_CITY: this.addStorage.U_A_CITY,
      U_A_STATE: this.addStorage.U_A_STATE,
      U_A_COUNTRY: this.addStorage.U_A_COUNTRY,
      STORAGE_LOCA_DISTANCE: this.addStorage.StorageDistance,
      STORAGE_LOC_DIRECTION: this.addStorage.StorageDirection,
      STORAGE_NEAR_TOWN: this.addStorage.NEAR_TOWN,
      STORAGE_TYPE: 'OFW',
      STORAGE_LOC_GPS: this.addStorage.GPS_LOCATION,
      FARMING_REGION: this.addStorage.FarmingRegion,
      TRUCK_TYPE: this.addStorage.TruckAccess,
      U_ID: this.currentUser.USER.U_ID
    }
    console.log('params', params);

    let URL = serviceUrl + 'AgvStoreRegistration/AgvStoreAddPS/api/v1/addStore';
    this.spinnerService.show();
    this.headers = new Headers({
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
      'Accept': 'application/json'
    });
    // this.option = new RequestOptions({ headers: this.headers });
    this.http.post(URL, $.param(params), new RequestOptions({ headers: this.headers })).subscribe((data) => {
      console.log('--- add store data.json() : ', data.json());
      if (data.json().errorCode) {
        // console.log('err');
        this.submitStorage = data.json().errorMessage;
      } else {
        // this.submitStorage = data.json().Outcome;
        this.broadCaster.broadcast('Save Storage Event', 'Saved Successfully');
        this.addStorageForm.reset();
        this.AddStorage.hide();

      }
      this.spinnerService.hide();
      $(".modal-content").scrollTop(0);
    })
  }

  dst;


  initMap(context) {




    var directionsService = new google.maps.DirectionsService;
    // let src = document.getElementById('source-input');
    // let dest = document.getElementById('destination-input');
    if (this.sourceBox != undefined) {
      var src = this.sourceBox.getPlaces();
      var dest = this.destBox.getPlaces();
      console.log('source ', src);
      console.log('Destination ', dest);

      // var bounds = new google.maps.LatLngBounds;
      // var markersArray = [];
      // this.initializeAutocomplete(input, map);
      var origin1 = src[0].formatted_address; //'Pune, Maharashtra, India'; //{lat: 55.93, lng: -3.118};
      // var origin2 = 'Greenwich, England';
      var destinationA = dest[0].formatted_address; //'Delhi, India';
      // var destinationB = {lat: 50.087, lng: 14.421};

      // var destinationIcon = 'https://chart.googleapis.com/chart?' +
      //     'chst=d_map_pin_letter&chld=D|FF0000|000000';
      // var originIcon = 'https://chart.googleapis.com/chart?' +
      //     'chst=d_map_pin_letter&chld=O|FFFF00|000000';

      // let currentPosition = new google.maps.LatLng(this.latitude, this.longitude);

      // var distcalcmap = new google.maps.Map(document.getElementById('distancemap'), {
      //   center: currentPosition,
      //   zoom: 13,
      //   mapTypeId: 'roadmap'
      // });

      // var directionsDisplay = new google.maps.DirectionsRenderer({map: distcalcmap});

      // var geocoder = new google.maps.Geocoder;

      var service = new google.maps.DistanceMatrixService;
      service.getDistanceMatrix({
        // origins: [origin1, origin2],
        origins: [origin1],
        // destinations: [destinationA, destinationB],
        destinations: [destinationA],
        travelMode: 'DRIVING',
        unitSystem: google.maps.UnitSystem.METRIC,
        avoidHighways: false,
        avoidTolls: false
      }, function (response, status) {
        if (status !== 'OK') {
          alert('Error was: ' + status);
        } else {
          var originList = response.originAddresses;
          var destinationList = response.destinationAddresses;
          var outputDiv = document.getElementById('output');
          outputDiv.innerHTML = '';
          // this.deleteMarkers(markersArray);
          // for (var i = 0; i < markersArray.length; i++) {
          //   markersArray[i].setMap(null);
          // }
          // markersArray = [];

          // var showGeocodedAddressOnMap = function(asDestination) {
          //   // var icon = asDestination ? destinationIcon : originIcon;
          //   return function(results, status) {
          //     if (status === 'OK') {
          //       this.distcalcmap.fitBounds(bounds.extend(results[0].geometry.location));
          //       markersArray.push(new google.maps.Marker({
          //         map: this.distcalcmap,
          //         position: results[0].geometry.location,
          //         icon: icon
          //       }));
          //     } else {
          //       alert('Geocode was not successful due to: ' + status);
          //     }
          //   };
          // };

          for (var i = 0; i < originList.length; i++) {
            var results = response.rows[i].elements;
            // geocoder.geocode({'address': originList[i]},
            //     // showGeocodedAddressOnMap(false)
            //   );
            for (var j = 0; j < results.length; j++) {
              // geocoder.geocode({'address': destinationList[j]},
              //     // showGeocodedAddressOnMap(true)
              //   );
              //     // var dist = document.getElementById('dist').innerHTML;
              // var dur = document.getElementById('duration').innerHTML;
              // dur = results[j].duration.text;
              // this.dist = results[j].distance.text;
              // this.duration = results[j].duration.text;
              outputDiv.innerHTML += originList[i] + ' to ' + destinationList[j] +
                '<br> <b> Distance: ' + results[j].distance.text + '</b> in <b>' +
                results[j].duration.text + '</b> <br>';

              var distance = results[j].distance.text.split(" ")[0];
              console.log('--- distance : ', distance);
              // (<HTMLInputElement>document.getElementById("strgDistnc")).value = results[j].distance.text;
              context.addStorage.StorageDistance = distance; //results[j].distance.text;

            }
          }
        }
      });
    }

    // Retrieve the start and end locations and create a DirectionsRequest using
    // WALKING directions.
    // directionsService.route({
    //   origin: origin1, //document.getElementById('start').value,
    //   destination: destinationA, //document.getElementById('end').value,
    //   travelMode: 'DRIVING'
    // }, function(response, status) {
    //   // Route the directions and pass the response to a function to create
    //   // markers for each step.
    //   if (status === 'OK') {
    //     // document.getElementById('warnings-panel').innerHTML =
    //     //     '<b>' + response.routes[0].warnings + '</b>';
    //     directionsDisplay.setDirections(response);
    //     // showSteps(response, markerArray, stepDisplay, map);
    //   } else {
    //     window.alert('Directions request failed due to ' + status);
    //   }
    // });
  }




  showGPSLocMap = true; GPS_LOCATION; gpslocation;


  getGPSLocation(context, flag) {

    this.showGPSLocMap = true;
    let currentPosition = new google.maps.LatLng(-24.994167, 134.866944);//lat,long
    var markers = [];
    // Clear out the old markers.
    markers.forEach(function (marker) {
      marker.setMap(null);
    });
    markers = [];
    var map;
    var originIcon = 'https://chart.googleapis.com/chart?' +
      'chst=d_map_pin_letter&chld=O|FFFF00|000000';
    console.log('---- this.ADDSTORAGE_FLAG', flag);

    if (flag == 'fromAddStorage') {
      map = new google.maps.Map(document.getElementById("addstorageMapGPS"), {
        center: currentPosition,
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });
      console.log('--- map', map);
    } else if (flag == 'fromOnfarm') {
      map = new google.maps.Map(document.getElementById("onfarmMapGPS"), {
        center: currentPosition,
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });
    } else if (flag == 'fromExfarm') {
      map = new google.maps.Map(document.getElementById("exfarmMapGPS"), {
        center: currentPosition,
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      });
    }

    // Create a marker for each place.
    markers.push(new google.maps.Marker({
      map: map,
      // icon: icon,
      position: currentPosition
    }));

    //   google.maps.event.addListener(markers, "click", function (e) {
    //     var infoWindow = new google.maps.InfoWindow();
    //     // infoWindow.setContent(markers.title);
    //     infoWindow.open(map, markers);
    // });

    //Add listener
    google.maps.event.addListener(map, "click", function (event) {
      markers.forEach(function (marker) {
        marker.setMap(null);
      });
      markers = [];
      var latitude = event.latLng.lat();
      var longitude = event.latLng.lng();
      console.log(latitude + ', ' + longitude);

      this.gpsloctext = latitude + ', ' + longitude;


      context.addStorage.GPS_LOCATION = this.gpsloctext;
      // (<HTMLInputElement>document.getElementById("gpsloctext")).value = this.gpsloctext;
      console.log("GPS_LOCATION :", this.gpsloctext);

      markers.push(new google.maps.Marker({ map: map, position: event.latLng }));
      // let radius = new google.maps.Circle({
      //   map: map,
      //   radius: 800,
      //   center: event.latLng,
      //   fillColor: '#777',
      //   fillOpacity: 0.1,
      //   strokeColor: '#AA0000',
      //   strokeOpacity: 0.8,
      //   strokeWeight: 2,
      //   draggable: true,    // Dragable
      //   editable: true      // Resizable
      // });

      // Center of map
      // map.panTo(new google.maps.LatLng(latitude, longitude));

    }); //end addListener

    // google.maps.event.addListener(markers, 'dragend', function(evt){
    //   infoWindow.setOptions({
    //       content: '<p>Marker dropped: Current Lat: ' + evt.latLng.lat().toFixed(3) + ' Current Lng: ' + evt.latLng.lng().toFixed(3) + '</p>'
    //   });
    //   infoWindow.open(map, markers);
    // });

    // google.maps.event.addListener(markers, 'drag', function(evt){
    //     console.log("marker is being dragged");
    // });  
  }

  getInvStrgDtlsGPSLocation(context, flag) {

    console.log("--- getInvStrgDtlsGPSLocationflag", flag);
    this.showGPSLocMap = true;
    let currentPosition = new google.maps.LatLng(-24.994167, 134.866944);//lat,long
    var markers = [];
    // Clear out the old markers.
    markers.forEach(function (marker) {
      marker.setMap(null);
    });
    markers = [];
    var map;
    var originIcon = 'https://chart.googleapis.com/chart?' +
      'chst=d_map_pin_letter&chld=O|FFFF00|000000';
    console.log('---- this.ADDSTORAGE_FLAG', flag);

    // if(flag  == 'fromAddStorage'){
    map = new google.maps.Map(document.getElementById("addInvStrgDtlsMapGPS"), {
      center: currentPosition,
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    // console.log('--- map', map);
    // }

    // Create a marker for each place.
    markers.push(new google.maps.Marker({
      map: map,
      // icon: icon,
      position: currentPosition
    }));

    //   google.maps.event.addListener(markers, "click", function (e) {
    //     var infoWindow = new google.maps.InfoWindow();
    //     // infoWindow.setContent(markers.title);
    //     infoWindow.open(map, markers);
    // });

    //Add listener
    google.maps.event.addListener(map, "click", function (event) {
      markers.forEach(function (marker) {
        marker.setMap(null);
      });
      markers = [];
      var latitude = event.latLng.lat();
      var longitude = event.latLng.lng();
      console.log(latitude + ', ' + longitude);

      this.gpsloctext = latitude + ', ' + longitude;


      context.inventory.GPS_LOCATION = this.gpsloctext;
      // (<HTMLInputElement>document.getElementById("gpsloctext")).value = this.gpsloctext;
      console.log("GPS_LOCATION :", this.gpsloctext);

      markers.push(new google.maps.Marker({ map: map, position: event.latLng }));
      // let radius = new google.maps.Circle({
      //   map: map,
      //   radius: 800,
      //   center: event.latLng,
      //   fillColor: '#777',
      //   fillOpacity: 0.1,
      //   strokeColor: '#AA0000',
      //   strokeOpacity: 0.8,
      //   strokeWeight: 2,
      //   draggable: true,    // Dragable
      //   editable: true      // Resizable
      // });

      // Center of map
      // map.panTo(new google.maps.LatLng(latitude, longitude));

    }); //end addListener

    // google.maps.event.addListener(markers, 'dragend', function(evt){
    //   infoWindow.setOptions({
    //       content: '<p>Marker dropped: Current Lat: ' + evt.latLng.lat().toFixed(3) + ' Current Lng: ' + evt.latLng.lng().toFixed(3) + '</p>'
    //   });
    //   infoWindow.open(map, markers);
    // });

    // google.maps.event.addListener(markers, 'drag', function(evt){
    //     console.log("marker is being dragged");
    // });  
  }


  deleteMarkers(markersArray) {
    for (var i = 0; i < markersArray.length; i++) {
      markersArray[i].setMap(null);
    }
    markersArray = [];
  }
  exportInfo() {
    var x = document.getElementById("snackbar");
    x.className = "show";
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
  }

  getFarmDetails() {
    this.spinnerService.show();

    this.url = serviceUrl + "AgvantageGetFarmingRegionsSB/AgvantageGetFarmingRegionsRestPS/api/v1/GetFarmingRegions";
    this.http.get(this.url).subscribe((data) => {
      this.spinnerService.hide();
      this.farmDetails = data.json() ? data.json().getFarmingRegions : [];
      console.log(this.farmDetails);
    },
      (err) => {
        this.farmDetails = [];
      });
  }
  getTrcukAccess() {
    this.spinnerService.show();
    this.url = serviceUrl + "/AgvantageGetTruckAccessSB/AgvantageGetTruckAccessRestPS/api/v1/GetTruckAccess";
    this.http.get(this.url).subscribe((data) => {
      this.spinnerService.hide();
      this.truckDetails = data.json() ? data.json().TruckType : [];
    },
      (err) => {
        this.truckDetails = [];
      });
  }
  qualities = [];
  wait: boolean = false;

  getQualities() {
    this.addRowDisable = false;
    let url = `${serviceUrl}AgvInvengetQuality/AvgInvGetQualityPS/api/v1/getQuality?C_ID=${this.inventory.SCC_C_ID}`;
    this.http.get(url).subscribe(data => {
      this.qualities = data.json() ? data.json().Quality : [];


      setTimeout(() => {
        this.wait = true;
      }, 2000);

    }, err => {
      this.quantities = [];
    })
  }
  getQualityForAdd() {
    
    let url = `${serviceUrl}AgvInvengetQuality/AvgInvGetQualityPS/api/v1/getQuality?C_ID=${this.inventory.SCC_C_ID}`;
    this.http.get(url).subscribe(data => {
      this.addRow = data.json() ? data.json().Quality : [];
      if (this.inventory.QntytQlty === '2') {
        this.addRow.unshift({ Q_NAME: 'Load'},{Q_NAME : 'Quantity'});
      }
      let newObj = Object.assign(this.addRow);
      this.qualities = this.qualities.concat(newObj);
    })
  }
  addCommGridRow(obj) {
    // this.load=this.load+1;
    this.getQualityForAdd();

  }
  indexTracker(index: number, value: any) {
    return index;
  }
}
